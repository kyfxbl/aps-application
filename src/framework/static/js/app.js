'use strict';

// Declare app level module which depends on filters, and services
var app = angular.module('app', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngStorage',
    'ui.router',
    'ui.bootstrap',
    'ui.load',
    'ui.jq',
    'ui.validate',
    'oc.lazyLoad',
    'pascalprecht.translate',
    'app.filters',
    'app.services',
    'app.directives',
    'app.controllers',
    'angularModalService',
    'angucomplete-alt'
])
    .run(
    ['$rootScope', '$state', '$stateParams',
        function ($rootScope, $state, $stateParams) {
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
            $rootScope.clientVeriable = window._client_variable;
        }
    ]
)
    .config(
    ['$stateProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide',
        function ($stateProvider, $urlRouterProvider, $controllerProvider, $compileProvider, $filterProvider, $provide) {

            // lazy controller, directive and service
            app.controller = $controllerProvider.register;
            app.directive = $compileProvider.directive;
            app.filter = $filterProvider.register;
            app.factory = $provide.factory;
            app.service = $provide.service;
            app.constant = $provide.constant;
            app.value = $provide.value;
            $urlRouterProvider
                .otherwise('/login/signin');


            $stateProvider
                .state('app', {
                    abstract: true,
                    url: '/app',
                    templateUrl: 'tpl/app.html'
                }).state('app.video_list', {
                    url: '/video_list',
                    templateUrl: '/cms_video/video/videoList.html',
                    resolve: {
                        deps: ['uiLoad','$ocLazyLoad',
                            function (uiLoad,$ocLazyLoad) {
                                return $ocLazyLoad.load('ngGrid').then(
                                    function(){
                                        return uiLoad.load(['/cms_video/video/js/videoList.js',
                                            'js/jquery/fileupload/tmpl.min.js',
                                            'js/jquery/fileupload/jquery.ui.widget.js',
                                            'js/jquery/fileupload/load-image.all.min.js',
                                            'js/jquery/fileupload/canvas-to-blob.min.js',
                                            'js/jquery/fileupload/jquery.iframe-transport.js',
                                            'js/jquery/fileupload/jquery.fileupload.js',
                                            'js/jquery/fileupload/jquery.fileupload-process.js',
                                            'js/jquery/fileupload/jquery.fileupload-image.js',
                                            'js/jquery/fileupload/jquery.fileupload-audio.js',
                                            'js/jquery/fileupload/jquery.fileupload-video.js',
                                            'js/jquery/fileupload/jquery.fileupload-validate.js',
                                            'js/jquery/fileupload/jquery.fileupload-ui.js',
                                            'js/jquery/fileupload/jquery.fileupload.css',
                                            'js/jquery/fileupload/jquery.fileupload-ui.css'
                                        ]);
                                    }
                                );
                            }]
                    }
                }).state('app.uploadvideo', {
                    url: '/uploadvideo',
                    templateUrl: '/cms_video/video/uploadVideo.html',
                    resolve: {
                        deps: ['uiLoad','$ocLazyLoad',
                            function( uiLoad ){
                                return uiLoad.load(['/cms_video/video/js/uploadVideo.js',
                                    'js/jquery/fileupload/tmpl.min.js',
                                    'js/jquery/fileupload/jquery.ui.widget.js',
                                    'js/jquery/fileupload/load-image.all.min.js',
                                    'js/jquery/fileupload/canvas-to-blob.min.js',
                                    'js/jquery/fileupload/jquery.iframe-transport.js',
                                    'js/jquery/fileupload/jquery.fileupload.js',
                                    'js/jquery/fileupload/jquery.fileupload-process.js',
                                    'js/jquery/fileupload/jquery.fileupload-image.js',
                                    'js/jquery/fileupload/jquery.fileupload-audio.js',
                                    'js/jquery/fileupload/jquery.fileupload-video.js',
                                    'js/jquery/fileupload/jquery.fileupload-validate.js',
                                    'js/jquery/fileupload/jquery.fileupload-ui.js',
                                    'js/jquery/fileupload/jquery.fileupload.css',
                                    'js/jquery/fileupload/jquery.fileupload-ui.css'
                                ])
                            }]
                    }
                }).state('app.poster_setting', {
                    url: '/poster_setting',
                    templateUrl: '/cms_video/topic/poster.html',
                    resolve: {
                        deps: ['uiLoad','$ocLazyLoad',
                            function (uiLoad,$ocLazyLoad) {
                                return $ocLazyLoad.load('ngGrid').then(
                                    function(){
                                    return uiLoad.load(['/cms_video/topic/js/indexSetting.js',
                                        'js/jquery/fileupload/tmpl.min.js',
                                        'js/jquery/fileupload/jquery.ui.widget.js',
                                        'js/jquery/fileupload/load-image.all.min.js',
                                        'js/jquery/fileupload/canvas-to-blob.min.js',
                                        'js/jquery/fileupload/jquery.iframe-transport.js',
                                        'js/jquery/fileupload/jquery.fileupload.js',
                                        'js/jquery/fileupload/jquery.fileupload-process.js',
                                        'js/jquery/fileupload/jquery.fileupload-image.js',
                                        'js/jquery/fileupload/jquery.fileupload-audio.js',
                                        'js/jquery/fileupload/jquery.fileupload-video.js',
                                        'js/jquery/fileupload/jquery.fileupload-validate.js',
                                        'js/jquery/fileupload/jquery.fileupload-ui.js',
                                        'js/jquery/fileupload/jquery.fileupload.css',
                                        'js/jquery/fileupload/jquery.fileupload-ui.css'
                                    ]);
                                }
                            );
                        }]
                    }
                }).state('app.topic_list', {
                    url: '/topic_list',
                    templateUrl: '/cms_video/topic/topicList.html',
                    resolve: {
                        deps: ['uiLoad','$ocLazyLoad',
                            function (uiLoad,$ocLazyLoad) {
                                return $ocLazyLoad.load('ngGrid').then(
                                    function(){
                                        return uiLoad.load(['/cms_video/topic/js/topicList.js',
                                            'js/jquery/fileupload/tmpl.min.js',
                                            'js/jquery/fileupload/jquery.ui.widget.js',
                                            'js/jquery/fileupload/load-image.all.min.js',
                                            'js/jquery/fileupload/canvas-to-blob.min.js',
                                            'js/jquery/fileupload/jquery.iframe-transport.js',
                                            'js/jquery/fileupload/jquery.fileupload.js',
                                            'js/jquery/fileupload/jquery.fileupload-process.js',
                                            'js/jquery/fileupload/jquery.fileupload-image.js',
                                            'js/jquery/fileupload/jquery.fileupload-audio.js',
                                            'js/jquery/fileupload/jquery.fileupload-video.js',
                                            'js/jquery/fileupload/jquery.fileupload-validate.js',
                                            'js/jquery/fileupload/jquery.fileupload-ui.js',
                                            'js/jquery/fileupload/jquery.fileupload.css',
                                            'js/jquery/fileupload/jquery.fileupload-ui.css'
                                        ]);
                                    }
                                );
                            }]
                    }
                }).state('app.deletetopic', {
                    url: '/deletetopic',
                    templateUrl: '/cms_video/topic/delete_topic.html',
                    resolve: {
                        deps: ['uiLoad','$ocLazyLoad',
                            function (uiLoad,$ocLazyLoad) {
                                return $ocLazyLoad.load('ngGrid').then(
                                    function(){
                                        return uiLoad.load(['/cms_video/topic/js/delete_topic.js',
                                            'js/jquery/fileupload/tmpl.min.js',
                                            'js/jquery/fileupload/jquery.ui.widget.js',
                                            'js/jquery/fileupload/load-image.all.min.js',
                                            'js/jquery/fileupload/canvas-to-blob.min.js',
                                            'js/jquery/fileupload/jquery.iframe-transport.js',
                                            'js/jquery/fileupload/jquery.fileupload.js',
                                            'js/jquery/fileupload/jquery.fileupload-process.js',
                                            'js/jquery/fileupload/jquery.fileupload-image.js',
                                            'js/jquery/fileupload/jquery.fileupload-audio.js',
                                            'js/jquery/fileupload/jquery.fileupload-video.js',
                                            'js/jquery/fileupload/jquery.fileupload-validate.js',
                                            'js/jquery/fileupload/jquery.fileupload-ui.js',
                                            'js/jquery/fileupload/jquery.fileupload.css',
                                            'js/jquery/fileupload/jquery.fileupload-ui.css'
                                        ]);
                                    }
                                );
                            }]
                    }
                }).state('app.commentDetail', {
                    url: '/commentDetail?id',
                    templateUrl: '/cms_video/topic/commentDetail.html',
                    resolve: {
                        deps: ['uiLoad','$ocLazyLoad',
                            function (uiLoad,$ocLazyLoad) {
                                return $ocLazyLoad.load('ngGrid').then(
                                    function(){
                                        return uiLoad.load(['/cms_video/topic/js/commentDetail.js',
                                            'js/jquery/fileupload/tmpl.min.js',
                                            'js/jquery/fileupload/jquery.ui.widget.js',
                                            'js/jquery/fileupload/load-image.all.min.js',
                                            'js/jquery/fileupload/canvas-to-blob.min.js',
                                            'js/jquery/fileupload/jquery.iframe-transport.js',
                                            'js/jquery/fileupload/jquery.fileupload.js',
                                            'js/jquery/fileupload/jquery.fileupload-process.js',
                                            'js/jquery/fileupload/jquery.fileupload-image.js',
                                            'js/jquery/fileupload/jquery.fileupload-audio.js',
                                            'js/jquery/fileupload/jquery.fileupload-video.js',
                                            'js/jquery/fileupload/jquery.fileupload-validate.js',
                                            'js/jquery/fileupload/jquery.fileupload-ui.js',
                                            'js/jquery/fileupload/jquery.fileupload.css',
                                            'js/jquery/fileupload/jquery.fileupload-ui.css'
                                        ]);
                                    }
                                );
                            }]
                    }
                }).state('app.replyDetail', {
                    url: '/replyDetail?id',
                    templateUrl: '/cms_video/topic/replyDetail.html',
                    resolve: {
                        deps: ['uiLoad','$ocLazyLoad',
                            function (uiLoad,$ocLazyLoad) {
                                return $ocLazyLoad.load('ngGrid').then(
                                    function(){
                                        return uiLoad.load(['/cms_video/topic/js/replyDetail.js',
                                            'js/jquery/fileupload/tmpl.min.js',
                                            'js/jquery/fileupload/jquery.ui.widget.js',
                                            'js/jquery/fileupload/load-image.all.min.js',
                                            'js/jquery/fileupload/canvas-to-blob.min.js',
                                            'js/jquery/fileupload/jquery.iframe-transport.js',
                                            'js/jquery/fileupload/jquery.fileupload.js',
                                            'js/jquery/fileupload/jquery.fileupload-process.js',
                                            'js/jquery/fileupload/jquery.fileupload-image.js',
                                            'js/jquery/fileupload/jquery.fileupload-audio.js',
                                            'js/jquery/fileupload/jquery.fileupload-video.js',
                                            'js/jquery/fileupload/jquery.fileupload-validate.js',
                                            'js/jquery/fileupload/jquery.fileupload-ui.js',
                                            'js/jquery/fileupload/jquery.fileupload.css',
                                            'js/jquery/fileupload/jquery.fileupload-ui.css'
                                        ]);
                                    }
                                );
                            }]
                    }
                }).state('app.tags_setting', {
                    url: '/tags_setting',
                    templateUrl: '/cms_video/topic/tags.html',
                    resolve: {
                        deps: ['uiLoad','$ocLazyLoad',
                            function (uiLoad,$ocLazyLoad) {
                                return $ocLazyLoad.load('ngGrid').then(
                                    function(){
                                        return uiLoad.load(['/cms_video/topic/js/tags.js',             
                                            'js/jquery/fileupload/tmpl.min.js',
                                            'js/jquery/fileupload/jquery.ui.widget.js',
                                            'js/jquery/fileupload/load-image.all.min.js',
                                            'js/jquery/fileupload/canvas-to-blob.min.js',
                                            'js/jquery/fileupload/jquery.iframe-transport.js',
                                            'js/jquery/fileupload/jquery.fileupload.js',
                                            'js/jquery/fileupload/jquery.fileupload-process.js',
                                            'js/jquery/fileupload/jquery.fileupload-image.js',
                                            'js/jquery/fileupload/jquery.fileupload-audio.js',
                                            'js/jquery/fileupload/jquery.fileupload-video.js',
                                            'js/jquery/fileupload/jquery.fileupload-validate.js',
                                            'js/jquery/fileupload/jquery.fileupload-ui.js',
                                            'js/jquery/fileupload/jquery.fileupload.css',
                                            'js/jquery/fileupload/jquery.fileupload-ui.css'
                                        ]);
                                    }
                                );
                            }]
                    }
                }).state('app.jiaocheng', {
                    url: '/jiaocheng',
                    templateUrl: '/cms_video/activity/jiaocheng.html',
                    resolve: {
                        deps: ['uiLoad','$ocLazyLoad',
                            function (uiLoad,$ocLazyLoad) {
                                return $ocLazyLoad.load('ngGrid').then(
                                    function(){
                                        return uiLoad.load(['/cms_video/activity/js/jiaocheng.js',
                                            'js/jquery/fileupload/tmpl.min.js',
                                            'js/jquery/fileupload/jquery.ui.widget.js',
                                            'js/jquery/fileupload/load-image.all.min.js',
                                            'js/jquery/fileupload/canvas-to-blob.min.js',
                                            'js/jquery/fileupload/jquery.iframe-transport.js',
                                            'js/jquery/fileupload/jquery.fileupload.js',
                                            'js/jquery/fileupload/jquery.fileupload-process.js',
                                            'js/jquery/fileupload/jquery.fileupload-image.js',
                                            'js/jquery/fileupload/jquery.fileupload-audio.js',
                                            'js/jquery/fileupload/jquery.fileupload-video.js',
                                            'js/jquery/fileupload/jquery.fileupload-validate.js',
                                            'js/jquery/fileupload/jquery.fileupload-ui.js',
                                            'js/jquery/fileupload/jquery.fileupload.css',
                                            'js/jquery/fileupload/jquery.fileupload-ui.css'
                                        ]);
                                    }
                                );
                            }]
                    }
                }).state('app.jcDetail', {
                    url: '/jcDetail?id',
                    templateUrl: '/cms_video/activity/jcDetail.html',
                    resolve: {
                        deps: ['uiLoad','$ocLazyLoad',
                            function (uiLoad,$ocLazyLoad) {
                                return $ocLazyLoad.load('ngGrid').then(
                                    function(){
                                        return uiLoad.load(['/cms_video/activity/js/jcDetail.js',
                                            'js/jquery/fileupload/tmpl.min.js',
                                            'js/jquery/fileupload/jquery.ui.widget.js',
                                            'js/jquery/fileupload/load-image.all.min.js',
                                            'js/jquery/fileupload/canvas-to-blob.min.js',
                                            'js/jquery/fileupload/jquery.iframe-transport.js',
                                            'js/jquery/fileupload/jquery.fileupload.js',
                                            'js/jquery/fileupload/jquery.fileupload-process.js',
                                            'js/jquery/fileupload/jquery.fileupload-image.js',
                                            'js/jquery/fileupload/jquery.fileupload-audio.js',
                                            'js/jquery/fileupload/jquery.fileupload-video.js',
                                            'js/jquery/fileupload/jquery.fileupload-validate.js',
                                            'js/jquery/fileupload/jquery.fileupload-ui.js',
                                            'js/jquery/fileupload/jquery.fileupload.css',
                                            'js/jquery/fileupload/jquery.fileupload-ui.css'
                                        ]);
                                    }
                                );
                            }]
                    }
                }).state('app.userMessage', {
                    url: '/push_message',
                    templateUrl: '/cms_video/setting/userMessage.html',
                    resolve: {
                        deps: ['uiLoad','$ocLazyLoad',
                            function (uiLoad,$ocLazyLoad) {
                                return $ocLazyLoad.load('ngGrid').then(
                                    function(){
                                        return uiLoad.load(['/cms_video/setting/js/userMessage.js',
                                            'js/jquery/fileupload/tmpl.min.js',
                                            'js/jquery/fileupload/jquery.ui.widget.js',
                                            'js/jquery/fileupload/load-image.all.min.js',
                                            'js/jquery/fileupload/canvas-to-blob.min.js',
                                            'js/jquery/fileupload/jquery.iframe-transport.js',
                                            'js/jquery/fileupload/jquery.fileupload.js',
                                            'js/jquery/fileupload/jquery.fileupload-process.js',
                                            'js/jquery/fileupload/jquery.fileupload-image.js',
                                            'js/jquery/fileupload/jquery.fileupload-audio.js',
                                            'js/jquery/fileupload/jquery.fileupload-video.js',
                                            'js/jquery/fileupload/jquery.fileupload-validate.js',
                                            'js/jquery/fileupload/jquery.fileupload-ui.js',
                                            'js/jquery/fileupload/jquery.fileupload.css',
                                            'js/jquery/fileupload/jquery.fileupload-ui.css'
                                        ]);
                                    }
                                );
                            }]
                    }
                }).state('app.get_feedback', {
                    url: '/get_feedback',
                    templateUrl: '/cms_video/setting/get_feedback.html',
                    resolve: {
                        deps: ['uiLoad','$ocLazyLoad',
                            function (uiLoad,$ocLazyLoad) {
                                return $ocLazyLoad.load('ngGrid').then(
                                    function(){
                                        return uiLoad.load(['/cms_video/setting/js/get_feedback.js',
                                            'js/jquery/fileupload/tmpl.min.js',
                                            'js/jquery/fileupload/jquery.ui.widget.js',
                                            'js/jquery/fileupload/load-image.all.min.js',
                                            'js/jquery/fileupload/canvas-to-blob.min.js',
                                            'js/jquery/fileupload/jquery.iframe-transport.js',
                                            'js/jquery/fileupload/jquery.fileupload.js',
                                            'js/jquery/fileupload/jquery.fileupload-process.js',
                                            'js/jquery/fileupload/jquery.fileupload-image.js',
                                            'js/jquery/fileupload/jquery.fileupload-audio.js',
                                            'js/jquery/fileupload/jquery.fileupload-video.js',
                                            'js/jquery/fileupload/jquery.fileupload-validate.js',
                                            'js/jquery/fileupload/jquery.fileupload-ui.js',
                                            'js/jquery/fileupload/jquery.fileupload.css',
                                            'js/jquery/fileupload/jquery.fileupload-ui.css'
                                        ]);
                                    }
                                );
                            }]
                    }
                }).state('app.add_author', {
                    url: '/add_author',
                    templateUrl: '/cms_video/setting/add_author.html',
                    resolve: {
                        deps: ['uiLoad','$ocLazyLoad',
                            function (uiLoad,$ocLazyLoad) {
                                return $ocLazyLoad.load('ngGrid').then(
                                    function(){
                                        return uiLoad.load(['/cms_video/setting/js/add_author.js',
                                            'js/jquery/fileupload/tmpl.min.js',
                                            'js/jquery/fileupload/jquery.ui.widget.js',
                                            'js/jquery/fileupload/load-image.all.min.js',
                                            'js/jquery/fileupload/canvas-to-blob.min.js',
                                            'js/jquery/fileupload/jquery.iframe-transport.js',
                                            'js/jquery/fileupload/jquery.fileupload.js',
                                            'js/jquery/fileupload/jquery.fileupload-process.js',
                                            'js/jquery/fileupload/jquery.fileupload-image.js',
                                            'js/jquery/fileupload/jquery.fileupload-audio.js',
                                            'js/jquery/fileupload/jquery.fileupload-video.js',
                                            'js/jquery/fileupload/jquery.fileupload-validate.js',
                                            'js/jquery/fileupload/jquery.fileupload-ui.js',
                                            'js/jquery/fileupload/jquery.fileupload.css',
                                            'js/jquery/fileupload/jquery.fileupload-ui.css'
                                        ]);
                                    }
                                );
                            }]
                    }
                }).state('app.user_infor', {
                    url: '/user_infor?account_id&nickname&type',
                    templateUrl: '/cms_video/setting/user_infor.html',
                    resolve: {
                        deps: ['uiLoad','$ocLazyLoad',
                            function (uiLoad,$ocLazyLoad) {
                                return $ocLazyLoad.load('ngGrid').then(
                                    function(){
                                        return uiLoad.load(['/cms_video/setting/js/user_infor.js',
                                            'js/jquery/fileupload/tmpl.min.js',
                                            'js/jquery/fileupload/jquery.ui.widget.js',
                                            'js/jquery/fileupload/load-image.all.min.js',
                                            'js/jquery/fileupload/canvas-to-blob.min.js',
                                            'js/jquery/fileupload/jquery.iframe-transport.js',
                                            'js/jquery/fileupload/jquery.fileupload.js',
                                            'js/jquery/fileupload/jquery.fileupload-process.js',
                                            'js/jquery/fileupload/jquery.fileupload-image.js',
                                            'js/jquery/fileupload/jquery.fileupload-audio.js',
                                            'js/jquery/fileupload/jquery.fileupload-video.js',
                                            'js/jquery/fileupload/jquery.fileupload-validate.js',
                                            'js/jquery/fileupload/jquery.fileupload-ui.js',
                                            'js/jquery/fileupload/jquery.fileupload.css',
                                            'js/jquery/fileupload/jquery.fileupload-ui.css'
                                        ]);
                                    }
                                );
                            }]
                    }
                }).state('app.auto_reply', {
                    url: '/auto_reply',
                    templateUrl: '/cms_video/wechat/auto_reply.html',
                    resolve: {
                        deps: ['uiLoad','$ocLazyLoad',
                            function (uiLoad,$ocLazyLoad) {
                                return $ocLazyLoad.load('ngGrid').then(
                                    function(){
                                        return uiLoad.load(['/cms_video/wechat/js/auto_reply.js',
                                            'js/jquery/fileupload/tmpl.min.js',
                                            'js/jquery/fileupload/jquery.ui.widget.js',
                                            'js/jquery/fileupload/load-image.all.min.js',
                                            'js/jquery/fileupload/canvas-to-blob.min.js',
                                            'js/jquery/fileupload/jquery.iframe-transport.js'
                                        ]);
                                    }
                                );
                            }]
                    }
                }).state('app.communities', {
                    url: '/communities',
                    templateUrl: '/cms_video/community/communities.html',
                    resolve: {
                        deps: ['uiLoad','$ocLazyLoad',
                            function (uiLoad,$ocLazyLoad) {
                                return $ocLazyLoad.load('ngGrid').then(
                                    function(){
                                        return uiLoad.load(['/cms_video/community/js/communities.js',
                                            'js/jquery/fileupload/tmpl.min.js',
                                            'js/jquery/fileupload/jquery.ui.widget.js',
                                            'js/jquery/fileupload/load-image.all.min.js',
                                            'js/jquery/fileupload/canvas-to-blob.min.js',
                                            'js/jquery/fileupload/jquery.iframe-transport.js',
                                            'js/jquery/fileupload/jquery.fileupload.js',
                                            'js/jquery/fileupload/jquery.fileupload-process.js',
                                            'js/jquery/fileupload/jquery.fileupload-image.js',
                                            'js/jquery/fileupload/jquery.fileupload-audio.js',
                                            'js/jquery/fileupload/jquery.fileupload-video.js',
                                            'js/jquery/fileupload/jquery.fileupload-validate.js',
                                            'js/jquery/fileupload/jquery.fileupload-ui.js',
                                            'js/jquery/fileupload/jquery.fileupload.css',
                                            'js/jquery/fileupload/jquery.fileupload-ui.css'
                                        ]);
                                    }
                                );
                            }]
                    }
                }).state('app.community_post', {
                    url: '/community_post?community_id',
                    templateUrl: '/cms_video/community/community_post.html',
                    resolve: {
                        deps: ['uiLoad','$ocLazyLoad',
                            function (uiLoad,$ocLazyLoad) {
                                return $ocLazyLoad.load('ngGrid').then(
                                    function(){
                                        return uiLoad.load(['/cms_video/community/js/community_post.js',
                                            'js/jquery/fileupload/tmpl.min.js',
                                            'js/jquery/fileupload/jquery.ui.widget.js',
                                            'js/jquery/fileupload/load-image.all.min.js',
                                            'js/jquery/fileupload/canvas-to-blob.min.js',
                                            'js/jquery/fileupload/jquery.iframe-transport.js',
                                            'js/jquery/fileupload/jquery.fileupload.js',
                                            'js/jquery/fileupload/jquery.fileupload-process.js',
                                            'js/jquery/fileupload/jquery.fileupload-image.js',
                                            'js/jquery/fileupload/jquery.fileupload-audio.js',
                                            'js/jquery/fileupload/jquery.fileupload-video.js',
                                            'js/jquery/fileupload/jquery.fileupload-validate.js',
                                            'js/jquery/fileupload/jquery.fileupload-ui.js',
                                            'js/jquery/fileupload/jquery.fileupload.css',
                                            'js/jquery/fileupload/jquery.fileupload-ui.css'
                                        ]);
                                    }
                                );
                            }]
                    }
                }).state('app.community_child', {
                    url: '/community_child?community_id',
                    templateUrl: '/cms_video/community/community_child.html',
                    resolve: {
                        deps: ['uiLoad','$ocLazyLoad',
                            function (uiLoad,$ocLazyLoad) {
                                return $ocLazyLoad.load('ngGrid').then(
                                    function(){
                                        return uiLoad.load(['/cms_video/community/js/community_child.js',
                                            'js/jquery/fileupload/tmpl.min.js',
                                            'js/jquery/fileupload/jquery.ui.widget.js',
                                            'js/jquery/fileupload/load-image.all.min.js',
                                            'js/jquery/fileupload/canvas-to-blob.min.js',
                                            'js/jquery/fileupload/jquery.iframe-transport.js',
                                            'js/jquery/fileupload/jquery.fileupload.js',
                                            'js/jquery/fileupload/jquery.fileupload-process.js',
                                            'js/jquery/fileupload/jquery.fileupload-image.js',
                                            'js/jquery/fileupload/jquery.fileupload-audio.js',
                                            'js/jquery/fileupload/jquery.fileupload-video.js',
                                            'js/jquery/fileupload/jquery.fileupload-validate.js',
                                            'js/jquery/fileupload/jquery.fileupload-ui.js',
                                            'js/jquery/fileupload/jquery.fileupload.css',
                                            'js/jquery/fileupload/jquery.fileupload-ui.css'
                                        ]);
                                    }
                                );
                            }]
                    }
                }).state('app.community_post_comments', {
                    url: '/community_post_comments?post_id',
                    templateUrl: '/cms_video/community/community_post_comments.html',
                    resolve: {
                        deps: ['uiLoad','$ocLazyLoad',
                            function (uiLoad,$ocLazyLoad) {
                                return $ocLazyLoad.load('ngGrid').then(
                                    function(){
                                        return uiLoad.load(['/cms_video/community/js/community_post_comments.js',
                                            'js/jquery/fileupload/tmpl.min.js',
                                            'js/jquery/fileupload/jquery.ui.widget.js',
                                            'js/jquery/fileupload/load-image.all.min.js',
                                            'js/jquery/fileupload/canvas-to-blob.min.js',
                                            'js/jquery/fileupload/jquery.iframe-transport.js',
                                            'js/jquery/fileupload/jquery.fileupload.js',
                                            'js/jquery/fileupload/jquery.fileupload-process.js',
                                            'js/jquery/fileupload/jquery.fileupload-image.js',
                                            'js/jquery/fileupload/jquery.fileupload-audio.js',
                                            'js/jquery/fileupload/jquery.fileupload-video.js',
                                            'js/jquery/fileupload/jquery.fileupload-validate.js',
                                            'js/jquery/fileupload/jquery.fileupload-ui.js',
                                            'js/jquery/fileupload/jquery.fileupload.css',
                                            'js/jquery/fileupload/jquery.fileupload-ui.css'
                                        ]);
                                    }
                                );
                            }]
                    }
                }).state('app.community_fans', {
                    url: '/community_fans?community_id&creator_id',
                    templateUrl: '/cms_video/community/community_fans.html',
                    resolve: {
                        deps: ['uiLoad','$ocLazyLoad',
                            function (uiLoad,$ocLazyLoad) {
                                return $ocLazyLoad.load('ngGrid').then(
                                    function(){
                                        return uiLoad.load(['/cms_video/community/js/community_fans.js',
                                            'js/jquery/fileupload/tmpl.min.js',
                                            'js/jquery/fileupload/jquery.ui.widget.js',
                                            'js/jquery/fileupload/load-image.all.min.js',
                                            'js/jquery/fileupload/canvas-to-blob.min.js',
                                            'js/jquery/fileupload/jquery.iframe-transport.js',
                                            'js/jquery/fileupload/jquery.fileupload.js',
                                            'js/jquery/fileupload/jquery.fileupload-process.js',
                                            'js/jquery/fileupload/jquery.fileupload-image.js',
                                            'js/jquery/fileupload/jquery.fileupload-audio.js',
                                            'js/jquery/fileupload/jquery.fileupload-video.js',
                                            'js/jquery/fileupload/jquery.fileupload-validate.js',
                                            'js/jquery/fileupload/jquery.fileupload-ui.js',
                                            'js/jquery/fileupload/jquery.fileupload.css',
                                            'js/jquery/fileupload/jquery.fileupload-ui.css'
                                        ]);
                                    }
                                );
                            }]
                    }
                }).state('app.goods_new', {
                    url: '/goods_new',
                    templateUrl: '/cms_video/goods/goods_new.html',
                    resolve: {
                        deps: ['uiLoad','$ocLazyLoad',
                            function (uiLoad,$ocLazyLoad) {
                                return $ocLazyLoad.load('ngGrid').then(
                                    function(){
                                        return uiLoad.load(['/cms_video/goods/js/goods_new.js',
                                            'js/jquery/fileupload/tmpl.min.js',
                                            'js/jquery/fileupload/jquery.ui.widget.js',
                                            'js/jquery/fileupload/load-image.all.min.js',
                                            'js/jquery/fileupload/canvas-to-blob.min.js',
                                            'js/jquery/fileupload/jquery.iframe-transport.js',
                                            'js/jquery/fileupload/jquery.fileupload.js',
                                            'js/jquery/fileupload/jquery.fileupload-process.js',
                                            'js/jquery/fileupload/jquery.fileupload-image.js',
                                            'js/jquery/fileupload/jquery.fileupload-audio.js',
                                            'js/jquery/fileupload/jquery.fileupload-video.js',
                                            'js/jquery/fileupload/jquery.fileupload-validate.js',
                                            'js/jquery/fileupload/jquery.fileupload-ui.js',
                                            'js/jquery/fileupload/jquery.fileupload.css',
                                            'js/jquery/fileupload/jquery.fileupload-ui.css'
                                        ]);
                                    }
                                );
                            }]
                    }
                }).state('app.goods_list', {
                    url: '/goods_list?cate',
                    templateUrl: '/cms_video/goods/goods_list.html',
                    resolve: {
                        deps: ['uiLoad','$ocLazyLoad',
                            function (uiLoad,$ocLazyLoad) {
                                return $ocLazyLoad.load('ngGrid').then(
                                    function(){
                                        return uiLoad.load(['/cms_video/goods/js/goods_list.js',
                                            'js/jquery/fileupload/tmpl.min.js',
                                            'js/jquery/fileupload/jquery.ui.widget.js',
                                            'js/jquery/fileupload/load-image.all.min.js',
                                            'js/jquery/fileupload/canvas-to-blob.min.js',
                                            'js/jquery/fileupload/jquery.iframe-transport.js',
                                            'js/jquery/fileupload/jquery.fileupload.js',
                                            'js/jquery/fileupload/jquery.fileupload-process.js',
                                            'js/jquery/fileupload/jquery.fileupload-image.js',
                                            'js/jquery/fileupload/jquery.fileupload-audio.js',
                                            'js/jquery/fileupload/jquery.fileupload-video.js',
                                            'js/jquery/fileupload/jquery.fileupload-validate.js',
                                            'js/jquery/fileupload/jquery.fileupload-ui.js',
                                            'js/jquery/fileupload/jquery.fileupload.css',
                                            'js/jquery/fileupload/jquery.fileupload-ui.css'
                                        ]);
                                    }
                                );
                            }]
                    }                
                }).state('app.goods_edit', {
                    url: '/goods_edit?goods_id&cate',
                    templateUrl: '/cms_video/goods/goods_edit.html',
                    resolve: {
                        deps: ['uiLoad','$ocLazyLoad',
                            function (uiLoad,$ocLazyLoad) {
                                return $ocLazyLoad.load('ngGrid').then(
                                    function(){
                                        return uiLoad.load(['/cms_video/goods/js/goods_edit.js',
                                            'js/jquery/fileupload/tmpl.min.js',
                                            'js/jquery/fileupload/jquery.ui.widget.js',
                                            'js/jquery/fileupload/load-image.all.min.js',
                                            'js/jquery/fileupload/canvas-to-blob.min.js',
                                            'js/jquery/fileupload/jquery.iframe-transport.js',
                                            'js/jquery/fileupload/jquery.fileupload.js',
                                            'js/jquery/fileupload/jquery.fileupload-process.js',
                                            'js/jquery/fileupload/jquery.fileupload-image.js',
                                            'js/jquery/fileupload/jquery.fileupload-audio.js',
                                            'js/jquery/fileupload/jquery.fileupload-video.js',
                                            'js/jquery/fileupload/jquery.fileupload-validate.js',
                                            'js/jquery/fileupload/jquery.fileupload-ui.js',
                                            'js/jquery/fileupload/jquery.fileupload.css',
                                            'js/jquery/fileupload/jquery.fileupload-ui.css'
                                        ]);
                                    }
                                );
                            }]
                    } 
                }).state('app.goods_search', {
                    url: '/goods_search?key&type&cate',
                    templateUrl: '/cms_video/goods/goods_search.html',
                    resolve: {
                        deps: ['uiLoad','$ocLazyLoad',
                            function (uiLoad,$ocLazyLoad) {
                                return $ocLazyLoad.load('ngGrid').then(
                                    function(){
                                        return uiLoad.load(['/cms_video/goods/js/goods_search.js',
                                            'js/jquery/fileupload/tmpl.min.js',
                                            'js/jquery/fileupload/jquery.ui.widget.js',
                                            'js/jquery/fileupload/load-image.all.min.js',
                                            'js/jquery/fileupload/canvas-to-blob.min.js',
                                            'js/jquery/fileupload/jquery.iframe-transport.js',
                                            'js/jquery/fileupload/jquery.fileupload.js',
                                            'js/jquery/fileupload/jquery.fileupload-process.js',
                                            'js/jquery/fileupload/jquery.fileupload-image.js',
                                            'js/jquery/fileupload/jquery.fileupload-audio.js',
                                            'js/jquery/fileupload/jquery.fileupload-video.js',
                                            'js/jquery/fileupload/jquery.fileupload-validate.js',
                                            'js/jquery/fileupload/jquery.fileupload-ui.js',
                                            'js/jquery/fileupload/jquery.fileupload.css',
                                            'js/jquery/fileupload/jquery.fileupload-ui.css'
                                        ]);
                                    }
                                );
                            }]
                    }
                }).state('app.web_list', {
                    url: '/goods_web',
                    templateUrl: '/cms_video/goods/goods_web.html',
                    resolve: {
                        deps: ['uiLoad','$ocLazyLoad',
                            function (uiLoad,$ocLazyLoad) {
                                return $ocLazyLoad.load('ngGrid').then(
                                    function(){
                                        return uiLoad.load(['/cms_video/goods/js/goods_web.js',
                                            'js/jquery/fileupload/tmpl.min.js',
                                            'js/jquery/fileupload/jquery.ui.widget.js',
                                            'js/jquery/fileupload/load-image.all.min.js',
                                            'js/jquery/fileupload/canvas-to-blob.min.js',
                                            'js/jquery/fileupload/jquery.iframe-transport.js',
                                            'js/jquery/fileupload/jquery.fileupload.js',
                                            'js/jquery/fileupload/jquery.fileupload-process.js',
                                            'js/jquery/fileupload/jquery.fileupload-image.js',
                                            'js/jquery/fileupload/jquery.fileupload-audio.js',
                                            'js/jquery/fileupload/jquery.fileupload-video.js',
                                            'js/jquery/fileupload/jquery.fileupload-validate.js',
                                            'js/jquery/fileupload/jquery.fileupload-ui.js',
                                            'js/jquery/fileupload/jquery.fileupload.css',
                                            'js/jquery/fileupload/jquery.fileupload-ui.css'
                                        ]);
                                    }
                                );
                            }]
                    }                                                   
                }).state('login', {
                    url: '/login',
                    template: '<div ui-view class="fade-in-right-big smooth"></div>'
                })
                .state('login.signin', {
                    url: '/signin',
                    templateUrl: '/cms_video/signin.html',
                    resolve: {
                        deps: ['uiLoad',
                            function (uiLoad) {
                                return uiLoad.load(['/cms_video/login.js']);
                            }]
                    }
                });
        }
    ]
)

// translate config
    .config(['$translateProvider', function ($translateProvider) {

        // Register a loader for the static files
        // So, the module will search missing translation tables under the specified urls.
        // Those urls are [prefix][langKey][suffix].
        $translateProvider.useStaticFilesLoader({
            prefix: 'l10n/',
            suffix: '.js'
        });

        // Tell the module what language to use by default
        $translateProvider.preferredLanguage('en');

        // Tell the module to store the language in the local storage
        $translateProvider.useLocalStorage();

    }])

/**
 * jQuery plugin config use ui-jq directive , config the js and css files that required
 * key: function name of the jQuery plugin
 * value: array of the css js file located
 */
    .constant('JQ_CONFIG', {
        easyPieChart: ['js/jquery/charts/easypiechart/jquery.easy-pie-chart.js'],
        sparkline: ['js/jquery/charts/sparkline/jquery.sparkline.min.js'],
        plot: ['js/jquery/charts/flot/jquery.flot.min.js',
            'js/jquery/charts/flot/jquery.flot.resize.js',
            'js/jquery/charts/flot/jquery.flot.tooltip.min.js',
            'js/jquery/charts/flot/jquery.flot.spline.js',
            'js/jquery/charts/flot/jquery.flot.orderBars.js',
            'js/jquery/charts/flot/jquery.flot.pie.min.js'],
        slimScroll: ['js/jquery/slimscroll/jquery.slimscroll.min.js'],
        sortable: ['js/jquery/sortable/jquery.sortable.js'],
        nestable: ['js/jquery/nestable/jquery.nestable.js',
            'js/jquery/nestable/nestable.css'],
        filestyle: ['js/jquery/file/bootstrap-filestyle.min.js'],
        slider: ['js/jquery/slider/bootstrap-slider.js',
            'js/jquery/slider/slider.css'],
        chosen: ['js/jquery/chosen/chosen.jquery.min.js',
            'js/jquery/chosen/chosen.css'],
        TouchSpin: ['js/jquery/spinner/jquery.bootstrap-touchspin.min.js',
            'js/jquery/spinner/jquery.bootstrap-touchspin.css'],
        wysiwyg: ['js/jquery/wysiwyg/bootstrap-wysiwyg.js',
            'js/jquery/wysiwyg/jquery.hotkeys.js'],
        dataTable: ['js/jquery/datatables/jquery.dataTables.min.js',
            'js/jquery/datatables/dataTables.bootstrap.js',
            'js/jquery/datatables/dataTables.bootstrap.css'],
        vectorMap: ['js/jquery/jvectormap/jquery-jvectormap.min.js',
            'js/jquery/jvectormap/jquery-jvectormap-world-mill-en.js',
            'js/jquery/jvectormap/jquery-jvectormap-us-aea-en.js',
            'js/jquery/jvectormap/jquery-jvectormap.css'],
        footable: ['js/jquery/footable/footable.all.min.js',
            'js/jquery/footable/footable.core.css'],
        daterangepicker: ['js/jquery/bootstrap-daterangepicker/daterangepicker.js',
            'js/jquery/bootstrap-daterangepicker/daterangepicker-bs3.css'],
        filepuload:['js/jquery/fileupload/tmpl.min.js',
            'js/jquery/fileupload/jquery.ui.widget.js',
            'js/jquery/fileupload/load-image.all.min.js',
            'js/jquery/fileupload/canvas-to-blob.min.js',
            'js/jquery/fileupload/jquery.iframe-transport.js',
            'js/jquery/fileupload/jquery.fileupload.js',
            'js/jquery/fileupload/jquery.fileupload-process.js',
            'js/jquery/fileupload/jquery.fileupload-image.js',
            'js/jquery/fileupload/jquery.fileupload-audio.js',
            'js/jquery/fileupload/jquery.fileupload-video.js',
            'js/jquery/fileupload/jquery.fileupload-validate.js',
            'js/jquery/fileupload/jquery.fileupload-ui.js',
            'js/jquery/fileupload/jquery.fileupload.css',
            'js/jquery/fileupload/jquery.fileupload-ui.css']
    }
)

// modules config
    .constant('MODULE_CONFIG', {
        select2: ['js/jquery/select2/select2.css',
            'js/jquery/select2/select2-bootstrap.css',
            'js/jquery/select2/select2.min.js',
            'js/modules/ui-select2.js']
    }
)

// oclazyload config
    .config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
        // We configure ocLazyLoad to use the lib script.js as the async loader
        $ocLazyLoadProvider.config({
            debug: false,
            events: true,
            modules: [
                {
                    name: 'ngGrid',
                    files: [
                        'js/modules/ng-grid/ng-grid.min.js',
                        'js/modules/ng-grid/ng-grid.css',
                        'js/modules/ng-grid/theme.css'
                    ]
                },
                {
                    name: 'toaster',
                    files: [
                        'js/modules/toaster/toaster.js',
                        'js/modules/toaster/toaster.css'
                    ]
                },
            ]
        })
    }]).config(["$httpProvider", function ($httpProvider) {
        $httpProvider.defaults.useXDomain = true;
        $httpProvider.defaults.withCredentials = true;
        $httpProvider.defaults.headers.common['X-Requested-With'] = "XMLHttpRequest";
        $httpProvider.interceptors.push('sessionChecker');
    }])
    .factory('sessionChecker', ["$location", function ($location) {
        var sessionChecker = {
            responseError: function (response) {
                if (response.status == 401) {
                    $location.path('/login/signin');
                    return;
                }
                return response;
            }
        };
        return sessionChecker;
    }]);


