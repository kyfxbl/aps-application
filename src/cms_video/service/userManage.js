var dbHelper            = require(FRAMEWORKPATH + "/utils/dbHelper");
var utils               = require(FRAMEWORKPATH + "/utils/utils");
var uuid                = require('node-uuid');
var _                   = require("underscore");
var logger = require(FRAMEWORKPATH + "/utils/logger").getLogger();
var redisService = require("./redisService");

exports.getUserInfo     = getUserInfo;
exports.addUser         = addUser;
exports.delUser         = delUser;
exports.editUser        = editUser;
exports.resetPassword   = resetPassword;
exports.deleteUser      = deleteUser;
exports.searchUser      = searchUser;         //搜索用户
exports.getPosts        = getPosts;           //获取用户的所有帖子
exports.getHomeWork     = getHomeWork;        //获取用户教的作业
exports.getTeacherData  = getTeacherData;    //获取老师的作品
exports.cancelUserBlack = cancelUserBlack;

function getUserInfo(req, res, next){
    var page = req.query["page"] || 1;
    var perPage = parseInt(req.query["perPage"]) || 20;
    var startIndex = (page - 1) * perPage;
    var results = {result: '',count:''};
    var sql1 = "select count(*) as count from accounts";
    var sql2 = "select a.id, a.username, a.password, a.birthday, a.coin, a.create_date, a.exp, a.gender, a.homepage_background, a.location, a.photo_url, a.profile, a.type, a.nickname, b.id 'black_id'  from accounts a left join comment_blacklist b on a.id = b.account_id limit :startIndex,:perPage";
    dbHelper.execSql(sql2, {startIndex: startIndex,perPage: perPage}, function(error, data1){
        if(error){
            console.log(error);
            return next(error);
        }
        _.each(data1, function(data){
            switch(data.type){
                case 1:
                    data.type = "美甲店主";
                    break;
                case 2:
                    data.type = "美甲师";
                    break;
                case 3:
                    data.type = "美甲从业者";
                    break;
                case 4:
                    data.type = "美甲消费者";
                    break;
                case 5:
                    data.type = "美甲老师";
                    break;
                case 6:
                    data.type = "其他";
                    break;
                default :
                    data.type = "";
            }
        });
        results.result = data1;
        dbHelper.execSql(sql1, {}, function(err, data2){
            if(err){
                console.log("err:" + err);
                return next(err);
            }
            results.count = data2;
            doResponse(req, res, results);
        });
    });
}

function addUser(req, res, next){
    var username = req.body.username;
    var nickname = req.body.nickname;
    var password = utils.md5(req.body.password);
    var type = req.body.type;
    var id = uuid.v1();
    var sql = "insert into accounts(id,username,password,nickname,type) values (:id,:username,:password,:nickname,:type)";
    var sql2 = "select * from accounts where username=:username";
    dbHelper.execSql(sql2, {username:username}, function(err, data){
        if(err){
            console.log(err);
            return next(err);
        }
        if(data.length == 0){
            dbHelper.execSql(sql, {id:id, username:username, password:password, nickname:nickname, type:type}, function(error,datas){
                if(error){
                    console.log(error);
                    return next(error);
                }
                doResponse(req, res, "ok");

                // 写入redis
                redisService.hmset(1, id, {
                    id: id,
                    username: username,
                    password: password,
                    nickname: nickname,
                    create_date: (new Date()).getTime(),
                    type: type,
                    profile: 'null',
                    exp: 0,
                    coin: 0,
                    homepage_background: 'null'
                }, function(err){
                    if(err){
                        logger.error(err);
                    }
                });

            });
        }else{
            doResponse(req, res, "该手机号已存在");
        }
    });

}

function editUser(req, res, next){
    var id       = req.body.id;
    var nickname = req.body.nickname;
    var type     = req.body.type;
    var exp      = req.body.exp;
    var coin     = req.body.coin;
    var sql      = "update accounts set nickname = :nickname,type = :type, exp = :exp, coin = :coin where id=:id";

    dbHelper.execSql(sql, {nickname: nickname, type: type, exp: exp, coin: coin, id:id}, function(error, data){
        if(error){
            console.log(error);
            return next(error);
        }
        doResponse(req, res, "ok");

        // 写入redis
        redisService.hmset(1, id, {
            id: id,
            nickname: nickname,
            type: type,
            exp: exp,
            coin: coin
        }, function(err){
            if(err){
                logger.error(err);
            }
        });
    });
}

function delUser(req, res, next){
    var id = req.body.id;
    var sql = "delete from accounts where id = :id";
    dbHelper.execSql(sql, {id: id}, function(error, data){
        if(error){
            console.log(error);
            return next(error);
        }
        doResponse(req, res, "ok");
    });
}

function resetPassword(req, res, next){
    var id = req.body.id;
    var password = utils.md5(req.body.password);
    var sql = "update accounts set password=:password where id=:id";
    dbHelper.execSql(sql, {password:password, id:id}, function(error, data){
        if(error){
            console.log(error);
            return next(error);
        }
        doResponse(req, res, "ok");
    });
}

// 小黑屋
function deleteUser(req, res, next){

    var account_id = req.body.id;
    var id = uuid.v1();

    var sql1 = "delete from comments where account_id = :account_id";
    var sql2 = "insert into comment_blacklist (id, account_id) values (:id, :account_id);";

    dbHelper.execSql(sql1, {account_id: account_id}, function (err){

        if(err){
            console.log(err);
            next(err);
            return;
        }

        dbHelper.execSql(sql2, {id: id, account_id: account_id}, function(err){

            if(err){
                console.log(err);
                next(err);
                return;
            }

            doResponse(req, res, "ok");
        });
    });
}

//取消小黑屋
function cancelUserBlack (req, res, next) {
    var black_id = req.body.black_id;
    var sql = 'delete from comment_blacklist where id=:black_id';
    dbHelper.execSql(sql, {black_id: black_id}, function (err) {
        if(err){
            console.log(err);
            next(err);
            return;
        }
        doResponse(req, res, "ok");
    });
}
 
//查找用户(昵称，用户名)
function searchUser (req, res, next) {

    var key         = req.query.key;
    var page        = req.query.page;
    var perPage     = req.query.perPage;
    var startIndex  = (page-1)*perPage;

    var result      = {
        pageData:'',
        count:0
    };

    async.parallel([getcount, getuserlist], function (err) {
        if(err){
            callback(err);
            return;
        }
        doResponse(req, res, result);
    });

    //查询符合要求的数量
    function getcount (callback) {
        var sql = "select count(*) as count from accounts where nickname like '%"+key+"%' or username like '%"+key+"%'";

        dbHelper.execSql(sql, {}, function (error, data) {
            if(error){
                console.log(error);
                return next(error);
            }
            result.count = data[0].count;
            callback(null);
        });
    }

    //获取分页数据
    function getuserlist (callback) {
        var sql = "select a.id, a.username, a.password, a.birthday, a.coin, a.create_date, a.exp, a.gender, a.homepage_background, a.location, a.photo_url, a.profile, a.type, a.nickname, b.id 'black_id' from accounts a left join comment_blacklist b on a.id = b.account_id where a.nickname like '%"+key+"%' or a.username like '%"+key+"%'    order by abs(length(nickname)-length(:key)) limit :startIndex,:perPage";

        dbHelper.execSql(sql, {key: key, startIndex: startIndex, perPage: parseInt(perPage)}, function (error, data) {
            if(error){
                console.log(error);
                return next(error);
            }
            _.each(data, function(data1){
            switch(data1.type){
                case 1:
                    data1.type = "美甲店主";
                    break;
                case 2:
                    data1.type = "美甲师";
                    break;
                case 3:
                    data1.type = "美甲从业者";
                    break;
                case 4:
                    data1.type = "美甲消费者";
                    break;
                case 5:
                    data1.type = "美甲老师";
                    break;
                case 6:
                    data1.type = "其他";
                    break;
                default :
                    data1.type = "";
            }
        });
            result.pageData = data;
            callback(null);
        });
    }
}

//根据用户id获取帖子
function getPosts (req, res, next) {
    var page       = req.query.page;
    var perPage    = req.query.perPage;
    var startIndex = (page-1)*perPage;
    var account_id = req.query.account_id;

    var result     = {
        pageData:'',
        count:0
    };

    async.parallel([getData, getCount], function (err) {
        if(err){
            callback(err);
            return;
        }
        doResponse(req, res, result);
    });

    function getData (callback) {
        var sql = "select a.id 'post_id',a.account_id 'account_id',a.create_date 'create_date',a.title 'title', "+
        "a.content 'content', b.name 'community_name'  from posts a left join communities b on a.community_id = b.id where account_id=:account_id and a.status <> 0 order by b.name limit :startIndex,:perPage";

        dbHelper.execSql(sql, {account_id: account_id, startIndex: startIndex, perPage: parseInt(perPage)},function (err, data) {
            if(err){
                console.log("err:" + err);
                return next(err);
            }
            result.pageData = data;
            callback(null);
        });
    }

    function getCount (callback) {
        var sql = "select count(*) as count from posts where account_id=:account_id and status <> 0";

        dbHelper.execSql(sql, {account_id: account_id},function (err, data) {
            if(err){
                console.log("err:" + err);
                return next(err);
            }
            result.count = data[0].count;
            callback(null);
        });
    }    
}

//获取交作业（带图片的评论）
function getHomeWork (req, res, next) {
    var page       = req.query.page;
    var perPage    = req.query.perPage;
    var startIndex = (page-1)*perPage;
    var account_id = req.query.account_id;

    var result     = {
        pageData:'',
        count:0
    };

    async.parallel([getData, getCount], function (err) {
        if(err){
            callback(err);
            return;
        }
        doResponse(req, res, result);
    });

    function getData (callback) {
        var sql = "select a.id 'comment_id', a.content 'content', a.content_pic 'content_pic', a.create_date 'create_date', a.score 'score',b.id 'topic_id', b.title 'topic_title'"+
        "from comments a left join topics b on a.topic_id=b.id where a.account_id=:account_id and a.content_pic is not Null and a.content_pic<>'' order by a.topic_id limit :startIndex,:perPage";

        dbHelper.execSql(sql, {account_id: account_id, startIndex: startIndex, perPage: parseInt(perPage)},function (err, data) {
            if(err){
                console.log("err:" + err);
                return next(err);
            }
            result.pageData = data;
            callback(null);
        });        
    }

    function getCount (callback) {
        var sql = "select count(*) as count from comments where account_id=:account_id";

        dbHelper.execSql(sql, {account_id: account_id},function (err, data) {
            if(err){
                console.log("err:" + err);
                return next(err);
            }
            result.count = data[0].count;
            callback(null);
        });   
    }    
}

//获取老师的作品
function getTeacherData (req, res, next) {
    var page       = req.query.page;
    var perPage    = req.query.perPage;
    var startIndex = (page-1)*perPage;
    var account_id = req.query.account_id;

    var result     = {
        pageData:'',
        count:0
    };

    async.parallel([getData, getCount], function (err) {
        if(err){
            callback(err);
            return;
        }
        doResponse(req, res, result);
    });

    function getData (callback) {
        var sql = "select id 'topic_id', title 'topic_title', create_date 'create_date', big_thumb_pic_url 'pic_url' from topics where account_id=:account_id order by create_date limit :startIndex,:perPage";

        dbHelper.execSql(sql, {account_id: account_id, startIndex: startIndex, perPage: parseInt(perPage)},function (err, data) {
            if(err){
                console.log("err:" + err);
                return next(err);
            }
            result.pageData = data;
            callback(null);
        });
    }

    function getCount (callback) {
        var sql = "select count(*) as count from topics where account_id=:account_id";

        dbHelper.execSql(sql, {account_id: account_id},function (err, data) {
            if(err){
                console.log("err:" + err);
                return next(err);
            }
            result.count = data[0].count;
            callback(null);
        });   
    }      
}