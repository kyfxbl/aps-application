//圈子内帖子服务
var logger            = require(FRAMEWORKPATH + "/utils/logger").getLogger();
var uuid              = require('node-uuid');
var url               = require('url');
var http              = require('http');
var sizeOf            = require('image-size');
var community_postDao = require("./community_postDao.js");

exports.postList      = postList;				//获取可以有子圈子的圈子列表
exports.postImg       = postImg;				//获取帖子图片	
exports.toggleTop     = toggleTop;				//置顶和取消置顶
exports.toggleHot     = toggleHot;				//推荐和取消推荐
exports.toggleBest    = toggleBest;				//推荐和取消推荐
exports.addPost       = addPost;				//新建帖子
exports.deletePostImg = deletePostImg;			//删除帖子中的图片
exports.addPostImg 	  = addPostImg;				//增加帖子中的图片
exports.editPost      = editPost;				//更新帖子
exports.deletePost    = deletePost;				//删除帖子
exports.clearPost     = clearPost; 				//清空帖子
exports.reportList    = reportList;				//举报的帖子
exports.searchList	  = searchList;				//搜索的帖子
	
// 获取帖子列表
function postList (req, res, next) {

	var community_id = req.query.community_id;
	var page         = req.query.page;
	var perPage      = req.query.perPage;

	community_postDao.getPostList(community_id, page, perPage, function (err,result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, result);
	});
}

//获取帖子图片
function postImg (req, res, next) {
	 
	var post_id = req.query.post_id;

	community_postDao.getPostImg(post_id, function (err,result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, result);
	});
}

//置顶和取消置顶
function toggleTop (req, res, next) {

	var post_id = req.body.post_id;
	var is_top  = req.body.is_top;
	
	community_postDao.postTopAction(post_id, is_top, function (err,result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, {message: "ok"});
	});
	// body...
}

//推荐和取消推荐
function toggleHot (req, res, next) {

	var post_id = req.body.post_id;
	var is_hot  = req.body.is_hot;

	community_postDao.postHotAction(post_id, is_hot, function (err,result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, {message: "ok"});
	});
	// body...
}

//加精和取消加精
function toggleBest (req, res, next) {

	var post_id = req.body.post_id;
	var is_best  = req.body.is_best;

	community_postDao.postBestAction(post_id, is_best, function (err,result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, {message: "ok"});
	});
	// body...
}

//新建帖子
function addPost (req, res, next) {
	
	var data = {
		id           : uuid.v1(),
		creator_id	 : req.body.creator_id, 
		community_id : req.body.community_id, 
		create_date  : (new Date()).getTime(), 
		title        : req.body.title, 
		content      : req.body.content, 
		is_hot       : req.body.is_hot, 
		is_top       : req.body.is_top,
		imgs 		 : req.body.imgs
	};
   

	//为图片添加id
	
	
	async.each(data.imgs, function(item, nextOne){
		http.get(url.parse(item.pic_url), function (response) {
            var chunks = [];
            response.on('data', function (chunk) {

                chunks.push(chunk);

            }).on('end', function() {

                var buffer = Buffer.concat(chunks);
                
                item.id = uuid.v1();
				item.serial_number = item.serial_number-1;
				item.width = sizeOf(buffer).width;
				item.height = sizeOf(buffer).height;
                nextOne(null);

            }).on("error", function(err){
                nextOne(err);
            });
        });

	}, function (err) {
		if(err){
			logger.error(err);
			return next(err);
		}
		community_postDao.addPosteData(data, function (err,result) {
			if(err){
				logger.error(err);
				next(err);
				return;
			}
			doResponse(req, res, result);
		});
	});
	
}

//删除帖子中的图片
function deletePostImg (req, res, next) {
	var pic_id = req.body.pic_id;
	community_postDao.deletePostPic(pic_id, function (err, result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, {message: "ok"});
	});
}

//增加加帖子中的图片
function addPostImg (req, res, next) {
	var data = {
		id            : uuid.v1(),
		post_id       : req.body.post_id,
		pic_url       : req.body.pic_url,
		serial_number : req.body.serial_number
	};
	community_postDao.addPostPic(data, function (err, result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, {message: "ok"});
	});
}

//编辑帖子
function editPost (req, res, next) {
	var data = {
		creator_id   : req.body.creator_id,
		post_id      : req.body.post_id, 
		title        : req.body.title, 
		content      : req.body.content, 
		is_hot       : req.body.is_hot, 
		is_top       : req.body.is_top,
		imgs 		 : req.body.imgs
	};
	for (var i = 0; i < data.imgs.length; i++) {
		data.imgs[i].serial_number = data.imgs[i].serial_number-1;
	};
	community_postDao.editPostData(data, function (err, result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, result);
	});
}

//删除帖子
function deletePost (req, res, next) {
	var post_id = req.body.post_id;
	community_postDao.deletePostData(post_id, function (err, result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, result);
	});
}

//清空帖子
function clearPost (req, res, next) {
	var community_id = req.body.community_id;
	community_postDao.clearPostData(community_id, function (err, result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, result);
	});
}

//获取举报的帖子
function reportList (req, res, next) {
	var community_id = req.query.community_id;
	community_postDao.getReportData(community_id, function (err, result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, result);
	})
}

//获取搜索的帖子
function searchList (req, res, next) {
	var community_id = req.query.community_id;
	var key          = req.query.key;

	community_postDao.getSearchData(community_id, key, function (err, result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, result);
	});
}