var sqlHelper             = require(FRAMEWORKPATH + "/db/sqlHelper");
var dbHelper              = require(FRAMEWORKPATH + "/utils/dbHelper");
var uuid                  = require('node-uuid');
var accountService        = require('./accountService');

exports.getCommentsList   = getCommentsList;		//获取评论列表
exports.searchCommentLIst = searchCommentLIst;		//搜索评论列表
exports.deletecommentData = deletecommentData;		//删除评论

//评论列表
function getCommentsList (post_id, page, perPage, callback) {
	var startIndex 	= (page-1)*perPage;
	var result 		= {
		pageData:'',
		count:0,
		community_id:'',
		post_title:''
	};

	async.parallel([getlistdata, getlisnum, getcommunityname], function (err) {
		if(err){
            callback(err);
            return;
        }
        callback(null,result);
	});

	//查询帖子列表
	function getlistdata (callback) {
		
		var sql = "select a.id 'comment_id',a.account_id 'account_id',b.nickname 'nickname',a.content 'content',a.create_date 'create_data',"+
		"a.at_account_id 'at_account',c.nickname 'at_nickname',a.reply_to 'reply_to',a.last_reply_to 'last_reply_to' from post_comments a left join "+
		"accounts b on a.account_id=b.id left join accounts c on a.at_account_id=c.id where a.post_id= :post_id limit :startIndex,:perPage";

		dbHelper.execSql(sql, {post_id: post_id, startIndex: startIndex, perPage: parseInt(perPage)}, function (err, data) {
			if(err){
				callback(err);
				return;
			}
			result.pageData = data;
			callback(null);
		});
	}

	//查询圈子内帖子数量
	function getlisnum (callback) {
		var sql = "select count(1) count from post_comments where post_id=:post_id";
		dbHelper.execSql(sql, {post_id: post_id}, function (err, data) {
			if(err){
				callback(err);
				return;
			}
			result.count = data[0].count;
			callback(null);
		});
	} 

	//查询圈子id和帖子标题，由post_id去获取community_id和title
	function getcommunityname (callback) {
		var sql = "select title 'title',community_id 'community_id' from posts where id=:post_id";
		dbHelper.execSql(sql, {post_id: post_id}, function (err, data) {
			if(err){
				callback(err);
				return;
			}
			result.post_title   = data[0].title;
			result.community_id = data[0].community_id;
			callback(null);
		});
	}
}

//搜索帖子（内容，作者，a@作者来查找）
function searchCommentLIst (post_id, key, callback) {
	var sql = "select a.id 'comment_id',a.account_id 'account_id',b.nickname 'nickname',a.content 'content',a.create_date 'create_data',"+
		"a.at_account_id 'at_account',c.nickname 'at_nickname',a.reply_to 'reply_to',a.last_reply_to 'last_reply_to' from post_comments a left join "+
		"accounts b on a.account_id=b.id left join accounts c on a.at_account_id=c.id where a.post_id= :post_id and (b.nickname like '%"+key+"%' or a.content like '%"+key+"%' or c.nickname like '%"+key+"%') "+
		"order by abs(length(b.nickname)-length(:key)),abs(length(a.content)-length(:key)),abs(length(c.nickname)-length(:key))";

	dbHelper.execSql(sql, {post_id: post_id, key: key}, function (err, data) {
		if(err){
				callback(err);
				return;
		}
		callback(null, data);			
	});
}

//删除评论
function deletecommentData (comment_id, callback) {
	
	var sql = "delete from post_comments where id= :comment_id or reply_to= :comment_id or last_reply_to= :comment_id";

	dbHelper.execSql(sql, {comment_id: comment_id}, function (err, data) {
		if(err){
				callback(err);
				return;
		}
		callback(null, data);	
	});
}