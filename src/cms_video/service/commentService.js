var dbHelper                     = require(FRAMEWORKPATH + "/utils/dbHelper");
var utils                        = require(FRAMEWORKPATH + "/utils/utils");
var redisTask = require("./redisTask");

exports.getCommentByTopic        = getCommentByTopic;
exports.getCommentCountByTopic   = getCommentCountByTopic;
exports.deleteComment            = deleteComment;
exports.getReplyByCommentId      = getReplyByCommentId;
exports.getReplyCountByCommentId = getReplyCountByCommentId;
exports.deleteReply              = deleteReply;

//获取评论
function getCommentByTopic(req, res, next){

    var topic_id = req.params["topic_id"];
    var page = req.query["page"] || 1;
    var perPage = parseInt(req.query["perPage"]) || 10;
    var startIndex = (page - 1) * perPage;

    var sql = "select a.id 'id',b.username 'username',b.nickname 'nickname',a.content_pic 'pic',a.content 'content'," +
        "a.create_date 'createDate',a.isHomework 'isHomework' " +
        "from comments a left join accounts b on a.account_id = b.id " +
        "where a.topic_id = :topic_id and a.reply_to is null and a.status <> 0 limit :startIndex,:perPage";
    dbHelper.execSql(sql, {topic_id: topic_id,startIndex: startIndex,perPage: perPage}, function(err, result){
        if(err){
            return next(err);
        }
        doResponse(req, res, result);
    });
}

//评论统计
function getCommentCountByTopic(req, res, next){

    var topic_id = req.params["topic_id"];

    var sql = "select count(1) 'count' from comments where topic_id = :topic_id and status <> 0";
    dbHelper.execSql(sql, {topic_id: topic_id}, function(err, result){
        if(err){
            return next(err);
        }
        doResponse(req, res, result[0]);
    });
}

//删除评论
function deleteComment(req, res, next){
    var topic_id = req.body.topic_id;
    var id = req.params.commentId;

    var sql = "update comments set status = 0 where id = :id";

    dbHelper.execSql(sql, {id: id}, function(err){
        if(err){
            return next(err);
        }
        doResponse(req, res, {message: "ok"});

        redisTask.delCommentsByCommentId(topic_id, id, function(error){
            if(error){
                return logger.error(error);
            }
            return;
        });
    });
}

//查询回复
function getReplyByCommentId(req, res, next){
    var commentId = req.params["commentId"];
    var page = req.query["page"] || 1;
    var perPage = parseInt(req.query["perPage"]) || 10;
    var startIndex = (page - 1) * perPage;

    var sql = "select a.id 'id',b.username 'username',b.nickname 'nickname',a.content_pic 'pic',a.content 'content'," +
        "a.create_date 'createDate',a.isHomework 'isHomework',c.nickname 'atNickname' " +
        "from comments a left join accounts b on a.account_id = b.id " +
        "left join accounts c on a.at_account_id = c.id " +
        "where a.reply_to = :commentId limit :startIndex,:perPage";
    dbHelper.execSql(sql, {commentId: commentId,startIndex: startIndex,perPage: perPage}, function(err, result){
        if(err){
            return next(err);
        }
        doResponse(req, res, result);
    });
}

//回复统计
function getReplyCountByCommentId(req, res, next){
    var commentId = req.params["commentId"];
    var sql = "select count(1) count from comments where reply_to = :commentId";

    dbHelper.execSql(sql, {commentId: commentId}, function(err, result){
        if(err){
            return next(err);
        }
        doResponse(req, res, result[0]);
    });
}

//删除回复
function deleteReply(req, res, next){
    var replyId = req.params.replyId;

    var sql = "delete from comments where id = :replyId";

    dbHelper.execSql(sql, {replyId: replyId}, function(err){
        if(err){
            return next(err);
        }
        doResponse(req, res, {message: "ok"});
    });
}