var dbHelper = require(FRAMEWORKPATH + "/utils/dbHelper");
var uuid = require('node-uuid');
var async = require('async');

exports.queryAllAutoReply = queryAllAutoReply;
exports.addAutoReply = addAutoReply;
exports.delAutoReply = delAutoReply;
exports.updateAutoReply = updateAutoReply;

function queryAllAutoReply(req, res, next){

    var page = req.query["page"] || 1;
    var perPage = parseInt(req.query["perPage"]) || 20;
    var startIndex = (page - 1) * perPage;
    var sql = "select * from weixin_messages limit :startIndex,:perPage";
    var count_sql = "select count(1) 'count' from weixin_messages";
    dbHelper.execSql(sql, {startIndex: startIndex, perPage: perPage}, function(err, datas){

        if(err){
            next(err);
            return;
        }
        dbHelper.execSql(count_sql, {}, function(error, reuslts){
            if(error){
                return next(error);
            }

            doResponse(req, res, {datas: datas, count: reuslts[0].count});
        });

    });
}

function addAutoReply(req, res, next){

    var options = {
        id: uuid.v1(),
        keyword: req.body.keyword,
        content_url: req.body.content_url,
        content: req.body.content
    };
    var sql = "insert into weixin_messages (id,keyword,type,content_url,content) values (:id,:keyword,'text',:content_url,:content)";
    var check_sql = "select * from weixin_messages where keyword=:keyword";
    dbHelper.execSql(check_sql, {keyword: req.body.keyword}, function(err1, datas){
        if(err1){
            next(err1);
            return;
        }

        if(datas && datas.length > 0){
            next({code: 1, message: "这个关键词已存在。"});
            return
        }

        dbHelper.execSql(sql, options, function(err2){
            if(err2){
                next(err2);
                return;
            }

            doResponse(req, res, {messages: "success"});
        });
    });


}

function delAutoReply(req, res, next){
    var id = req.body.id;
    var sql = "delete from weixin_messages where id=:id";
    dbHelper.execSql(sql, {id: id}, function(err){
        if(err){
            next(err);
            return;
        }
        doResponse(req, res, {message: "success"});
    });
}

function updateAutoReply(req, res, next){
    var id = req.body.id;
    var keyword = req.body.keyword;
    var content = req.body.content;
    var content_url = req.body.content_url;
    var sql = "update weixin_messages set keyword=:keyword,content=:content,content_url=:content_url where id=:id";
    dbHelper.execSql(sql,{keyword: keyword, content: content, content_url: content_url, id: id}, function(err){
        if(err){
            next(err);
            return;
        }

        doResponse(req, res, {message: "success"});
    });
}