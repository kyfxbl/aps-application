var sqlHelper              = require(FRAMEWORKPATH + "/db/sqlHelper");
var dbHelper               = require(FRAMEWORKPATH + "/utils/dbHelper");
var uuid                   = require('node-uuid');
var accountService         = require('./accountService');

exports.getCommunitiesList = getCommunitiesList;		//获取圈子列表
exports.addCommunity       = addCommunity; 				//添加圈子
exports.deleteCommunity    = deleteCommunity;			//删除圈子
exports.updateCommunity    = updateCommunity;			//更新圈子信息
exports.getAttrList        = getAttrList;				//获取圈子类别列表
exports.getParentList      = getParentList;				//获取可以有子圈子的圈子列表
exports.getreportList      = getreportList;				//获取被举报的帖子列表
exports.getsearchList	   = getsearchList;				//获取搜索圈子列表
exports.getallfans		   = getallfans;				//获取圈子所有用户
exports.getsearchfans	   = getsearchfans;				//在当前圈子内搜索到的用户
exports.deletefans         = deletefans;				//解除圈子与用户的联系
exports.deleteallfans      = deleteallfans;				//解除圈子和所有用户的联系
exports.getchildData	   = getchildData;				//查询父级圈子下的所有子圈子
exports.deletechildData	   = deletechildData;				//从父级圈子中删除子圈子
exports.clearchildData	   = clearchildData;				//清除子圈子

// 圈子列表（可定制分页和分页数量）
function getCommunitiesList (page,perPage,callback) {

	var startIndex 	= (page-1)*perPage;
	var result 		= {
		pageData:'',
		count:0
	};

	async.parallel([getCommunitiesList, getCommunitiesNum], function (err) {
		if(err){
            callback(err);
            return;
        }
        callback(null,result);
	});

	//查询圈子列表
	function getCommunitiesList(callback){
		var sql = "select a.id 'community_id', a.name 'name', a.creator_id 'creator_id', a.create_date 'create_date', a.pic_url 'pic_url', a.attr 'attr', "+
		"a.description 'description', a.parent_id 'parent_id', a.is_container 'is_container', a.is_official 'is_official', a.status 'status', b.nickname 'nickname' "+
		"from communities a left join accounts b on a.creator_id=b.id where a.status <> 2 limit :startIndex,:perPage";

		dbHelper.execSql(sql, {startIndex: startIndex, perPage: parseInt(perPage)}, function (err, data) {
			if(err){
				callback(err);
				return;
			}
			result.pageData = data;
			callback(null);
		});
	}
	//查询圈子数量
	function getCommunitiesNum (callback) {
		var sql = "select count(1) count from communities where status <> 2";
		dbHelper.execSql(sql, {}, function (err, data) {
			if(err){
				callback(err);
				return;
			}
			result.count = data[0].count;
			callback(null);
		});
	}     
}

//新增圈子
function addCommunity (data,callback) {  
	
	var result = {
		communities:'',
		accounts:''
	};

	async.parallel([addcommunities, addaccounts], function (err) {
		if(err){
            callback(err);
            return;
        }
        callback(null,result);
	});

	//更新communities表 
	function addcommunities (callback) {
		var sql = "insert into communities (id, name, create_date, pic_url, attr, description, parent_id, is_container, is_official, creator_id, status) "+
		"VALUES (:id, :name, :create_date, :pic_url, :attr, :description, :parent_id, :is_container, :is_official, :creator_id, :status)";

		dbHelper.execSql(sql, {id: data.id, name: data.name, create_date: data.create_date, pic_url: data.pic_url, attr: data.attr, description: data.description, parent_id: data.parent_id, is_container: data.is_container, is_official: data.is_official, creator_id: data.creator_id, status: data.status}, function (err, data){
			if(err){
				callback(err);
				return;
			}
			result.communities = 'ok';
			callback(null);
		});
	}

	//更新communitites_has_accounts表
	function addaccounts (callback) {
		var sql = "insert into communities_has_accounts (id, community_id, account_id, create_date) values (:id, :community_id, :account_id, :create_date)";

		dbHelper.execSql(sql, {id: uuid.v1(), community_id: data.id, account_id: data.creator_id, create_date: data.create_date}, function (err, data) {
			if(err){
				callback(err);
				return;
			}
			result.accounts = 'ok';
			callback(null);
		});
	}
}

//删除圈子
function deleteCommunity (id,callback) {
	var flag = true;
	var data = {
		type: '', 		//0可以删除，1有子圈子，2有帖子 
		message: ''
	};

	async.series([getDataList, getPostList, deleteData], function (err, result) {
		if(err){
            callback(err);
            return;
        }
        callback(null,data);
	});
	
	//检查下面是否有子圈子
	function getDataList (callback) {

		var sql = "select name 'name', id 'community_id' from communities where parent_id = :parent_id and status <> 2";
		dbHelper.execSql(sql, {parent_id: id}, function (err, result) {
			if(err){
				callback(err);
				return;
			}
			if(result.length == 0){
				flag = true;
				callback(null);
				return;
			}
			data.type    = 1;
			data.message = result;
			flag         = false;
			callback(null);
		});
	}

	//检查下面是否有帖子
	function getPostList (callback) {
		if(flag){
			var sql = "select id 'post_id' from posts where community_id= :id and status <> 0";
			dbHelper.execSql(sql, {id: id}, function (err, result) {
				if(err){
					callback(err);
					return;
				}
				if(result.length == 0){
					flag = true;
					callback(null);
					return;
				}
				data.type    = 2;
				data.message = result;
				flag         = false;
				callback(null);
				return;
			});
			return;
		}
		callback(null);
	}

	//删除圈子
	function deleteData (callback) {
		if(flag){
			var sql = "update communities set status=2 where id= :id"
			dbHelper.execSql(sql, {id: id}, function (err, result) {
				if(err){
					callback(err);
					return;
				}
				data.type    = 0;
				data.message = 'communitiesok';
				callback(null);
				return;
			});
			return;
		}
		callback(null);
	}
}

//更新圈子信息
function updateCommunity (id, data, callback) {

	var result = {
		infor:'',
		parent:'',
		author:'',
		judge:'',		//true新作者在圈子内，false新作者不在圈子内
		post:''			//判断子圈子下面是否有帖子，有帖子true，无帖子false，有帖子，不允许改为父级圈子
	};

	async.series([judgepost, updatedata, clearparentid, judge, changecreator], function (err, data) {
		if(err){
            callback(err);
            return;
        }
        callback(null,result);
	});

	//判断是否有子圈子
	function judgepost (callback) {
		var sql = "select id  from posts where community_id= :id";
		dbHelper.execSql(sql, {id: id}, function (err, post) {
			if(err){
				callback(err);
				return;
			}
			if(post.length == 0){
				result.post = false;
				callback(null);
				return;
			}
			data.is_container = 0;		//如果子圈子中有帖子，不允许将圈子改为父级	圈子
			result.post = true;
			callback(null);
			return;
		});
	}

	//更新communities表
	function updatedata (callback) {
		if(result.post){
			var sql = "update communities set name = :name, pic_url = :pic_url, attr = :attr, description = :description, is_container = :is_container,"+
			" creator_id = :creator_id, status= :status, is_official= :is_official where id = :id";

			dbHelper.execSql(sql, {name: data.name, pic_url: data.pic_url, attr: data.attr, description: data.description, is_container: data.is_container, creator_id: data.creator_id, status: data.status, is_official: data.is_official, id: id}, function (err, data) {
				if(err){
					callback(err);
					return;
				}
				result.infor = 'ok';
				callback(null);
			});
		}else{
			var sql = "update communities set name = :name, pic_url = :pic_url, attr = :attr, description = :description, parent_id = :parent_id, is_container = :is_container,"+
			" creator_id = :creator_id, status= :status, is_official= :is_official where id = :id";

			dbHelper.execSql(sql, {name: data.name, pic_url: data.pic_url, attr: data.attr, description: data.description, parent_id: data.parent_id, is_container: data.is_container, creator_id: data.creator_id, status: data.status, is_official: data.is_official, id: id}, function (err, data) {
				if(err){
					callback(err);
					return;
				}
				result.infor = 'ok';
				callback(null);
			});
		}
	}

	//将子圈子变为父级圈子
	function clearparentid (callback) {
		if(data.is_container == 1){
			result.parent = 'nochange';
			callback(null);
			return;
		}

		var sql = "update communities set parent_id = :val where parent_id = :id";
		dbHelper.execSql(sql, {val: null, id: id}, function (err, data) {
			if(err){
				callback(err);
				return;
			}
			result.parent = 'ok';
			callback(null);
		}); 
	}

	//判断新作者是否是圈子用户
	function judge (callback) {
		if(data.old_creator == data.creator_id){
			result.author = 'nochange';
			result.judge = true;
			callback(null);
			return;
		}

		var sql = "select id from communities_has_accounts where account_id= :account_id and community_id= :community_id"
		dbHelper.execSql(sql, {account_id: data.creator_id, community_id: id}, function (err, data) {
			if(err){
				callback(err);
				return;
			}
			if(data.length == 0){
				result.judge = false;
				callback(null);
				return
			}
			result.judge = true;
			callback(null);
		});
	}

	//更改圈子作者
	function changecreator (callback) {
		if(result.judge){
			result.author = 'nochange';
			callback(null);
			return;
		}
		var sql = "insert into communities_has_accounts (id, community_id, account_id, create_date) values (:id, :community_id, :account_id, :create_date)"
		dbHelper.execSql(sql, {id: uuid.v1(), community_id: id, account_id: data.creator_id, create_date: data.create_date}, function (err, data) {
			if(err){
				callback(err);
				return;
			}
			result.author = 'ok';
			callback(null);
		});
	}
}

//获取圈子类别信息
function getAttrList (callback) {
	var sql = "select distinct attr from communities";
	dbHelper.execSql(sql, {}, function (err, data) {
		if(err){
			callback(err);
			return;
		}
		data = _.pluck(data, 'attr');
		callback(null,data);
	})
}

//获取可以拥有子圈子的圈子
function getParentList (callback) {
	var sql = "select name 'name', id 'community_id' from communities where is_container = 1 and status <> 2";
	dbHelper.execSql(sql, {}, function (err, data) {
		if(err){
			callback(err);
			return;
		}
		callback(null,data);
	});
}

//查询被举报的圈子列表
function getreportList (callback) {
	var sql = "select a.id 'community_id', a.name 'name', a.creator_id 'creator_id', a.create_date 'create_date', a.pic_url 'pic_url', a.attr 'attr', "+
		"a.description 'description', a.parent_id 'parent_id', a.is_container 'is_container', a.is_official 'is_official', b.nickname 'nickname' "+
		"from communities a left join accounts b on a.creator_id=b.id where a.is_reported='1' and a.status <> 2";

	dbHelper.execSql(sql, {}, function (err, data) {
		if(err){
			callback(err);
			return;
		}
		callback(null,data);
	});

}

//搜索圈子
function getsearchList (key, callback) {
	var sql = "select a.id 'community_id', a.name 'name', a.creator_id 'creator_id', a.create_date 'create_date', a.pic_url 'pic_url', a.attr 'attr',"+
	"a.description 'description', a.parent_id 'parent_id', a.is_container 'is_container', a.is_official 'is_official', b.nickname 'nickname' from "+
	"communities a left join accounts b on a.creator_id=b.id where (a.name like '%"+key+"%' or a.attr like '%"+key+"%' or b.nickname like '%"+key+"%') and a.status <> 2"+
	" order by abs(length(a.name)-length(:key)),abs(length(b.nickname)-length(:key)),abs(length(a.attr)-length(:key))";

	dbHelper.execSql(sql, {key: key}, function (err, data) {
		if(err){
			callback(err);
			return;
		}
		callback(null,data);
	});
}

//获取圈子内的成员列表
function getallfans (page, perPage, community_id, callback) {
	var startIndex 	= (page-1)*perPage;
	var result 		= {
		pageData:'',
		count:0
	};

	async.parallel([getData, getCount], function (err) {
		if(err){
            callback(err);
            return;
        }
        callback(null,result);
	});

	function getData (callback) {
		var sql = 'select b.id, b.username, b.photo_url, b.type, b.nickname '+
		'from communities_has_accounts a left join accounts b on a.account_id = b.id where a.community_id = :community_id limit :startIndex,:perPage';

		dbHelper.execSql(sql, {community_id: community_id, startIndex: startIndex, perPage: parseInt(perPage)}, function (err, data) {
			if(err){
				callback(err);
				return;
			}
			_.each(data, function(data){
	            switch(data.type){
	                case 1:
	                    data.type = "美甲店主";
	                    break;
	                case 2:
	                    data.type = "美甲师";
	                    break;
	                case 3:
	                    data.type = "美甲从业者";
	                    break;
	                case 4:
	                    data.type = "美甲消费者";
	                    break;
	                case 5:
	                    data.type = "美甲老师";
	                    break;
	                case 6:
	                    data.type = "其他";
	                    break;
	                default :
	                    data.type = "未知身份";
	            }
	        });
			result.pageData = data;
			callback(null);
		});
	}

	function getCount (callback) {
		var sql = 'select count(1) count from communities_has_accounts a left join accounts b on a.account_id = b.id where a.community_id = :community_id';

		dbHelper.execSql(sql, {community_id: community_id}, function (err, data) {
			if(err){
				callback(err);
				return;
			}
			result.count = data[0].count;
			callback(null);
		});
	}
}

//搜索圈子内的成员列表
function getsearchfans (page, perPage, community_id, key, callback) {
	var startIndex 	= (page-1)*perPage;
	var result 		= {
		pageData:'',
		count:0
	};

	async.parallel([getData, getCount], function (err) {
		if(err){
            callback(err);
            return;
        }
        callback(null,result);
	});

	function getData (callback) {
		var sql = "select b.id, b.username, b.photo_url, b.type, b.nickname "+
		"from communities_has_accounts a left join accounts b on a.account_id = b.id where a.community_id = :community_id and b.nickname like '%"+key+"%' order by abs(length(b.nickname)-length(:key)) limit :startIndex,:perPage";

		dbHelper.execSql(sql, {community_id: community_id, startIndex: startIndex, perPage: parseInt(perPage), key: key}, function (err, data) {
			if(err){
				callback(err);
				return;
			}
			result.pageData = data;
			callback(null);
		});
	}

	function getCount (callback) {
		var sql = "select count(1) count from communities_has_accounts a left join accounts b on a.account_id = b.id where a.community_id = :community_id and b.nickname like '%"+key+"%'";

		dbHelper.execSql(sql, {community_id: community_id}, function (err, data) {
			if(err){
				callback(err);
				return;
			}
			result.count = data[0].count;
			callback(null);
		});
	}
}

//从圈子用户列表中删除一个用户
function deletefans (account_id, community_id, callback) {
	var sql = "delete from communities_has_accounts where account_id= :account_id and community_id= :community_id";
	var message;
	dbHelper.execSql(sql, {account_id: account_id, community_id: community_id}, function (err, data) {
		if(err){
				callback(err);
				return;
			}
		message = 'ok';
		callback(null, message);
	});
}

//删除圈子中除了作者的所有用户
function deleteallfans (account_id, community_id, callback) {
	var sql = "delete from communities_has_accounts where community_id= :community_id and account_id <> :account_id";
	var message;
	dbHelper.execSql(sql, {account_id: account_id, community_id: community_id}, function (err, data) {
		if(err){
			callback(err);
			return;
		}
		message = 'ok';
		callback(null, message);
	});
}

//查询父级圈子下的所有子圈子
function getchildData (community_id, page, perPage, callback) {
	var startIndex 	= (page-1)*perPage;
	var result 		= {
		pageData:'',
		count:0
	};
	async.parallel([getData, getCount], function (err) {
		if(err){
            callback(err);
            return;
        }
        callback(null,result);
	});

	function getData (callback) {
		var sql = "select a.id 'community_id', a.name 'name', a.creator_id 'creator_id', a.create_date 'create_date', a.pic_url 'pic_url', a.attr 'attr', "+
		"a.description 'description', a.parent_id 'parent_id', a.is_container 'is_container', a.is_official 'is_official', a.status 'status', b.nickname 'nickname' "+
		"from communities a left join accounts b on a.creator_id=b.id where a.parent_id= :community_id and a.status <> 2 limit :startIndex,:perPage";

		dbHelper.execSql(sql,{community_id: community_id, startIndex: startIndex, perPage: parseInt(perPage)},function (err, data) {
			if(err){
				callback(err);
				return;
			}
			result.pageData = data;
			callback(null);
		});
	}

	function getCount (callback) {
		var sql = "select count(1) count from communities where parent_id= :community_id";
		dbHelper.execSql(sql, {community_id: community_id}, function (err, data) {
			if(err){
				callback(err);
				return;
			}
			result.count = data[0].count;
			callback(null);
		});
	}
}

//从父级圈子中删除子圈子
function deletechildData (community_id, callback) {
	var sql = "update communities set parent_id=null where id= :community_id";

	dbHelper.execSql(sql, {community_id: community_id}, function (err, data) {
		if(err){
			callback(err);
			return;
		}
		callback(null, data);
	});
}

//清空子圈子
function clearchildData (parent_id, callback) {
	var sql = "update communities set parent_id= :val where parent_id= :parent_id";

	dbHelper.execSql(sql, {val: null, parent_id: parent_id}, function (err, data) {
		if(err){
			callback(err);
			return;
		}
		callback(null, data);
	});
}
