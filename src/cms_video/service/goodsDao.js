var dbHelper               = require(FRAMEWORKPATH + "/utils/dbHelper");
var uuid                   = require('node-uuid');

exports.getCategoryList     = getCategoryList;
exports.createNewCategory   = createNewCategory;
exports.editCategory        = editCategory;
exports.deleteCategory      = deleteCategory;
exports.getGoodsList        = getGoodsList;
exports.getSearchList       = getSearchList;
exports.createNewGoods      = createNewGoods;
exports.editGoods           = editGoods;
exports.deleteGoods         = deleteGoods;
exports.toggleGoods         = toggleGoods;
exports.getNotShowList      = getNotShowList;
exports.getGoods            = getGoods;
exports.toggleHot			= toggleHot;
exports.getHotGoods			= getHotGoods;
exports.getWebList			= getWebList;
exports.createNewWeb		= createNewWeb;
exports.editWeb				= editWeb;
exports.deleteWeb			= deleteWeb;
exports.toggleWeb			= toggleWeb;

// exports following function
function getCategoryList (callback) {

	var sql = "select id 'category_id', name 'category_name',pic_url from commodity_categories";

	dbHelper.execSql(sql,{},function (err, data) {

		if(err){
			callback(err);
			return;
		}

		callback(null, data);
	});
}

function createNewCategory (data, callback) {
	var sql = "insert into commodity_categories (id, name, create_date,pic_url) values (:id, :name, :create_date, :pic_url)";

	dbHelper.execSql(sql,data,function (err, data) {
		if(err){
			callback(err);
			return;
		}
		callback(null,data);
	});
}

function editCategory (data, callback) {
	var sql = "update commodity_categories set name= :name, pic_url= :pic_url where id= :id";

	dbHelper.execSql(sql,data,function (err, data) {
		if(err){
			callback(err);
			return;
		}
		callback(null,data);
	});
}

function deleteCategory (category_id, callback) {

	var flag = false;// if allow delete

	async.series([judgeGoods, deleteAction], function (err, result) {
		if(err){
            callback(err);
            return;
        }
        callback(null, flag);
	});

	// not allow to delete non empty category
	function judgeGoods (callback) {
		var sql = "select id from commodities where category_id= :category_id";
		dbHelper.execSql(sql,{category_id: category_id},function (err, result) {

			if(err){
				callback(err);
				return;
			}

			if(result.length == 0){
				flag = true;
			}
			callback(null);
		});
	}

	// do delete
	function deleteAction (callback) {

		if(!flag){
			callback(null);
			return;
		}

		var sql = "delete from commodity_categories where id= :id";

		dbHelper.execSql(sql,{id: category_id},function (err, result) {
			if(err){
				callback(err);
				return;
			}
			callback(null);
		});
	}
}

function getGoodsList (category_id, page, perPage, callback) {
	var startIndex 	= (page-1)*perPage;
	var result 		= {
		pageData:'',
		count:0
	};

	async.parallel([getCount, getData], function (err) {
		if(err){
            callback(err);
            return;
        }
        callback(null,result);
	});

	function getCount  (callback) {
		var sql = "select count(1) 'count' from commodities where category_id= :category_id"

		dbHelper.execSql(sql, {category_id: category_id}, function (err, data) {
			if(err){
				callback(err);
				return;
			}
			result.count = data[0].count;
			callback(null);
		});
	}

	function getData (callback) {
		var sql = "select * from commodities where category_id= :category_id limit :startIndex,:perPage"

		dbHelper.execSql(sql, {category_id: category_id, startIndex: startIndex, perPage: parseInt(perPage)}, function (err, data) {
			if(err){
				callback(err);
				return;
			}
			result.pageData = data;
			callback(null);
		});
	}
}

function getSearchList (key, page, perPage,type, callback) {
	var startIndex 	= (page-1)*perPage;
	var result 		= {
		pageData:'',
		count:0
	};

	async.parallel([getCount, getData], function (err) {
		if(err){
            callback(err);
            return;
        }
        callback(null,result);
	});

	function getCount  (callback) {
		if(type == 0){
			var sql = "select count(1) 'count' from commodities where name like "+"'%"+key+"%' or description like "+"'%"+key+"%'";
		}else{
			var sql = "select count(1) 'count' from commodities where (name like "+"'%"+key+"%' or description like "+"'%"+key+"%') and available=1";
		}
		

		dbHelper.execSql(sql, {key: key}, function (err, data) {
			if(err){
				callback(err);
				return;
			}
			result.count = data[0].count;
			callback(null);
		});
	}

	function getData (callback) {
		if(type == 0){
			var sql = "select a.id,a.category_id,a.name,a.price,a.vendor_type,a.url,a.pic_url,a.description,a.available,a.real_id,a.is_hot,b.name 'category_name' from commodities a left join commodity_categories b on a.category_id=b.id where a.name like "+"'%"+key+"%' or a.description like "+"'%"+key+"%'"+
			" order by abs(length(a.name)-length(:key)), abs(length(a.description)-length(:key)) limit :startIndex,:perPage";
		}else{
			var sql = "select a.id,a.category_id,a.name,a.price,a.vendor_type,a.url,a.pic_url,a.description,a.available,a.real_id,a.cost,a.is_hot,b.name 'category_name' from commodities a left join commodity_categories b on a.category_id=b.id where (a.name like "+"'%"+key+"%' or a.description like "+"'%"+key+"%') and available=1"+
			" order by abs(length(a.name)-length(:key)), abs(length(a.description)-length(:key)) limit :startIndex,:perPage";
		}
		

		dbHelper.execSql(sql, {key: key, startIndex: startIndex, perPage: parseInt(perPage)}, function (err, data) {
			if(err){
				callback(err);
				return;
			}
			result.pageData = data;
			callback(null);
		});
	}
}

function getGoods(id, callback) {
	var sql = "select * from commodities where id= :id";
	dbHelper.execSql(sql, {id: id}, function (err, data) {
		if(err){
			callback(err);
			return;
		}
		callback(null, data);
	});
}

function createNewGoods (data, callback) {
	var result = {
		hot:true,
		msg:''
	}

	async.series([judgeHot,addData],function (err) {
		if(err){
            callback(err);
            return;
        }
        callback(null,result);
	});

	function judgeHot (callback) {
		var sql = "select count(1) count from commodities where available=1 and is_hot=1";
		dbHelper.execSql(sql,{},function (err, data) {
			if(err){
				callback(err);
				return;
			}
			if( data[0].count <3){
				result.hot = true;
				callback(null);
				return;
			}
			result.hot = false;
			callback(null);
		});
	}

	function addData (callback) {
		if(result.hot == false && data.available==1 && data.is_hot == 1){	
			result.msg = false;
			callback(null);
			return;
		}

		var sql = "insert into commodities (id,category_id,name,price,vendor_type,url,pic_url,description,available,create_date,real_id,cost,is_hot)"+
		"values (:id,:category_id,:name,:price,:vendor_type,:url,:pic_url,:description,:available,:create_date,:real_id,:cost,:is_hot)";
		dbHelper.execSql(sql,data,function (err, data) {
			if(err){
				callback(err);
				return;
			}
			result.msg = true;
			callback(null);
		});
	}
}

function editGoods (data, callback) {
	var result = {
		hot:true,
		msg:''
	}

	async.series([judgeHot,editData],function (err) {
		if(err){
            callback(err);
            return;
        }
        callback(null,result);
	});

	function judgeHot (callback) {
		var sql = "select count(1) count from commodities where available=1 and is_hot=1";
		dbHelper.execSql(sql,{},function (err, data) {
			if(err){
				callback(err);
				return;
			}
			if( data[0].count <3){
				result.hot = true;
				callback(null);
				return;
			}
			result.hot = false;
			callback(null);
		});
	}

	function editData (callback) {
		if(result.hot == false && data.available==1 && data.is_hot == 1){	
			result.msg = false;
			callback(null);
			return;
		}

		var sql = "update commodities set category_id=:category_id, name=:name, price=:price, vendor_type=:vendor_type, url=:url, pic_url=:pic_url, description=:description, available=:available ,real_id=:real_id, cost= :cost,is_hot= :is_hot where id=:id";

		dbHelper.execSql(sql,data,function (err, data) {
			if(err){
				callback(err);
				return;
			}
			result.msg = true;
			callback(null);
		});
	}

}

function deleteGoods (goods_id, callback) {
	var sql = "delete from commodities where id= :id";
	var sql2 = "delete from topics_has_commodities where commodity_id= :id"

	dbHelper.execSql(sql,{id: goods_id},function (err, data) {
		if(err){
			callback(err);
			return;
		}
		dbHelper.execSql(sql2,{id: goods_id},function (err, data) {
			if(err){
				callback(err);
				return;
			}
			callback(null,data);
		});
	});
}

function toggleGoods (goods_id, available, callback) {

	var result = {
		hot:true,
		self:true,
		msg:''
	}

	async.series([self,judgeHot,updateData],function (err) {
		if(err){
            callback(err);
            return;
        }
        callback(null,result);
	});

	function self (callback) {
		var sql = "select is_hot from commodities where id=:id";
		dbHelper.execSql(sql,{id:goods_id},function (err, data) {
			if(err){
				callback(err);
				return;
			}
			if( data[0].is_hot == 1){
				result.self = false;
				callback(null);
				return;
			}
			result.self = true;
			callback(null);
		})
	}

	function judgeHot (callback) {
		var sql = "select count(1) count from commodities where available=1 and is_hot=1";
		dbHelper.execSql(sql,{},function (err, data) {
			if(err){
				callback(err);
				return;
			}
			if( data[0].count <3){
				result.hot = true;
				callback(null);
				return;
			}
			result.hot = false;
			callback(null);
		});
	}

	function updateData (callback) {
		if(result.hot == false && result.self==false && available == 1){
			result.msg = false;
			callback(null);
			return;
		}

		var sql = "update commodities set available=:available where id=:id";
		dbHelper.execSql(sql,{id:goods_id, available:available},function (err, data) {
			if(err){
				callback(err);
				return;
			}
			result.msg = true;
			callback(null);
		});
	}
}

function getNotShowList (page, perPage, callback) {
	var startIndex 	= (page-1)*perPage;
	var result 		= {
		pageData:'',
		count:0
	};

	async.parallel([getCount, getData], function (err) {
		if(err){
            callback(err);
            return;
        }
        callback(null,result);
	});

	function getCount  (callback) {
		var sql = "select count(1) 'count' from commodities where available=0";

		dbHelper.execSql(sql, {}, function (err, data) {
			if(err){
				callback(err);
				return;
			}
			result.count = data[0].count;
			callback(null);
		});
	}

	function getData (callback) {
		var sql = "select a.id,a.category_id,a.name,a.price,a.vendor_type,a.url,a.pic_url,a.description,a.available,a.real_id,a.cost,a.is_hot,b.name 'category_name' from commodities a left join commodity_categories b on a.category_id=b.id where a.available=0 order by b.name";

		dbHelper.execSql(sql, {startIndex: startIndex, perPage: parseInt(perPage)}, function (err, data) {
			if(err){
				callback(err);
				return;
			}
			result.pageData = data;
			callback(null);
		});
	}
}

function getHotGoods (callback) {
	var sql = "select a.id,a.category_id,a.name,a.price,a.vendor_type,a.url,a.pic_url,a.description,a.available,a.real_id,a.cost,a.is_hot,b.name 'category_name' from commodities a left join commodity_categories b on a.category_id=b.id where a.is_hot=1 and a.available=1";

	dbHelper.execSql(sql, {}, function (err, data) {
		if(err){
			callback(err);
			return;
		}
		callback(null,data);
	});
}

function toggleHot (goods_id, is_hot, callback) {
	var result = {
		hot:true,
		slef:'',
		msg:''
	}

	async.series([self,judgeHot,updateData],function (err) {
		if(err){
            callback(err);
            return;
        }
        callback(null,result);
	});
	
	function self (callback) {
		var sql = "select available from commodities where id=:id";
		dbHelper.execSql(sql,{id:goods_id},function (err, data) {
			if(err){
				callback(err);
				return;
			}
			if( data[0].available == 1){
				result.self = false;
				callback(null);
				return;
			}
			result.self = true;
			callback(null);
		});
	}

	function judgeHot (callback) {
		var sql = "select count(1) count from commodities where available=1 and is_hot=1";
		dbHelper.execSql(sql,{},function (err, data) {
			if(err){
				callback(err);
				return;
			}
			if( data[0].count <3){
				result.hot = true;
				callback(null);
				return;
			}
			result.hot = false;
			callback(null);
		});
	}

	function updateData (callback) {
		if(result.hot == false && result.self==false && is_hot == 1){
			result.msg = false;
			callback(null);
			return;
		}
		var sql = "update commodities set is_hot=:is_hot where id=:id";
		dbHelper.execSql(sql,{id:goods_id, is_hot:is_hot},function (err, data) {
			if(err){
				callback(err);
				return;
			}
			result.msg = true;
			callback(null);
		});
	}
}

function getWebList  (page, perPage, callback) {
	var startIndex 	= (page-1)*perPage;
	var result 		= {
		pageData:'',
		count:0
	};

	async.parallel([getCount, getData], function (err) {
		if(err){
            callback(err);
            return;
        }
        callback(null,result);
	});

	function getCount  (callback) {
		var sql = "select count(1) 'count' from mall_activities";

		dbHelper.execSql(sql, {}, function (err, data) {
			if(err){
				callback(err);
				return;
			}
			result.count = data[0].count;
			callback(null);
		});
	}

	function getData (callback) {
		var sql = "select * from mall_activities  limit :startIndex,:perPage";

		dbHelper.execSql(sql, {startIndex: startIndex, perPage: parseInt(perPage)}, function (err, data) {
			if(err){
				callback(err);
				return;
			}
			result.pageData = data;
			callback(null);
		});
	}
}

function createNewWeb (data, callback) {
	var sql = "insert into mall_activities (id, activity_url, available, create_date, height,name) values (:id, :activity_url, :available, :create_date, :height, :name)";

	dbHelper.execSql(sql,data,function (err, data) {
		if(err){
			callback(err);
			return;
		}
		callback(null,data);
	});
}

function editWeb (data, callback) {
	var sql = "update mall_activities set activity_url= :activity_url, available= :available, height= :height, name= :name where id= :id";
	dbHelper.execSql(sql,data,function (err, data) {
		if(err){
			callback(err);
			return;
		}
		callback(null,data);
	});
}

function deleteWeb (id, callback) {
	var sql = "delete from mall_activities where id= :id";

	dbHelper.execSql(sql,{id: id},function (err, data) {
		if(err){
			callback(err);
			return;
		}
		callback(null,data);
	});	
}

function toggleWeb (id, available, callback) {

	var sql = "update mall_activities set available=:available where id=:id";

	dbHelper.execSql(sql,{id:id, available:available},function (err, data) {
		if(err){
			callback(err);
			return;
		}
		callback(null,data);
	});
}