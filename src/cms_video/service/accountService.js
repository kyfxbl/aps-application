/**
 * Created with JetBrains WebStorm.
 * User: ganyue
 * Date: 15/4/15
 * Time: 上午10:12
 * To change this template use File | Settings | File Templates.
 */

var dbHelper = require(FRAMEWORKPATH + "/utils/dbHelper");
var redisService = require("./redisService");
var uuid = require('node-uuid');

//视频表操作
exports.getAccount = getAccount;
exports.getAccountByid = getAccountByid;

function getAccount(accountArray,callback){
    if(accountArray.length>255){
        callback("只支持查询255以下的会员");
    }
    var accountMap = {};
    var ids = accountArray.join("','");
    ids = "'"+ids+"'";
    var sql = "select * from accounts where id in("+ids+")";
    dbHelper.execSql(sql, {}, function (error, result) {
        if (error) {
            callback(error);
            return
        }
        if(result && result.length>0){
            _.each(result,function(one){
                accountMap[one.id] = one;
            })
            callback(null,accountMap);

        }else{
            callback(null,accountMap);
        }
    });
}

//根据单个id查询account信息
function getAccountByid(id, callback){

    redisService.hgetall(1, id, function(err, obj){
        if(err){
            return callback(err);
        }
        if(!obj){
            obj = {
                id: null,
                username: null,
                password: null,
                nickname: null,
                photo_url: null,
                type: null,
                gender: null,
                birthday: null
            };
        }
        if(obj.photo_url == "null"){
            obj.photo_url = null;
        }
        if(obj.type){
            obj.type = parseInt(obj.type);
        }
        if(obj.gender){
            obj.gender = parseInt(obj.gender);
        }
        if(obj.birthday){
            obj.birthday = parseInt(obj.birthday);
        }
        callback(null, [obj]);
    });

}

