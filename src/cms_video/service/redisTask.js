var dbHelper = require(FRAMEWORKPATH + "/utils/dbHelper");
var logger = require(FRAMEWORKPATH + "/utils/logger").getLogger();
var redisService = require("./redisService");
var async = require("async");

exports.delCommentsByCommentId = delCommentsByCommentId;


//刷新Redis中单个主题的评论
function reloadCommentByTopicId(topicId, callback){

    var sql = "select a.id 'id',a.account_id 'account_id',a.create_date 'createDate'," +
        "a.content 'content',a.content_pic 'contentPic',a.isHomework 'isHomework',a.at_account_id 'at_account_id'," +
        "a.status 'status',a.score 'score' " +
        "from comments a " +
        "where a.reply_to is Null and a.topic_id=:id order by a.create_date desc";

    dbHelper.execSql(sql, {id: topicId}, function (err, results) {
        if (err) {
            return callback(err);
        }

        async.eachSeries(results, function (item, next) {
            var at = {};
            var replys = [];
            var replysAt = [];

            getAccountByid(item["account_id"], function (err, account_result) {

                if (err) {
                    return next(err);
                }

                async.series([_queryAt, _queryReplys, _queryReplysAt], function (err) {

                    if (err) {
                        return next(err);
                    }

                    item.at = JSON.stringify(at);
                    item.replys = JSON.stringify(replysAt);
                    item.author = account_result[0]["nickname"];
                    item.authorPhoto = account_result[0]["photo_url"];
                    item.userid = account_result[0]["id"];

                    //delete item["account_id"];
                    delete item["at_account_id"];

                    redisService.zadd(2, topicId, parseInt(item.createDate), item.id, function(err){
                        if(err){
                            return next(err);
                        }
                        redisService.hmset(2, item.id, item, function(err){
                            if(err){
                                return next(err);
                            }
                            next(null);
                        });
                    });

                });

            });

            //根据comment查询@的账号信息
            function _queryAt(callback) {

                var atSql = "select t.id 'userId',t.nickname 'nickname' from accounts t where t.id = :id";

                dbHelper.execSql(atSql, {id: item["at_account_id"]}, function (err, results) {

                    if (err) {
                        return callback(err);
                    }

                    at = results[0] || {};
                    callback(null);
                });
            }

            //根据comment查询回复
            function _queryReplys(callback) {

                //被系统删除的评论不查询回复信息
                //if(item.status == 0 || item.status == '0'){
                //    return callback(null);
                //}

                var replysSql = "select t.id 'id',a.id 'userid',a.nickname 'author',t.create_date 'createDate',t.content 'content'," +
                    "t.content_pic 'contentPic',t.isHomework 'isHomework',t.at_account_id 'at_account_id',t.status 'status' " +
                    "from comments t left join accounts a on t.account_id = a.id " +
                    "where t.reply_to = :id order by t.create_date";

                dbHelper.execSql(replysSql, {id: item["id"]}, function (err, results) {

                    if (err) {
                        return callback(err);
                    }

                    replys = results;
                    callback(null);
                });
            }

            //根据_queryReplys的结果查询回复中@的账号信息
            function _queryReplysAt(callback) {

                var replysAtSql = "select t.id 'userId',t.nickname 'nickname' from accounts t where t.id = :id";

                async.eachSeries(replys, function (item, cb) {
                    var id = item["at_account_id"];
                    /*
                     if(!id){
                     return cb(null);
                     }
                     */
                    dbHelper.execSql(replysAtSql, {id: id}, function (err, result) {
                        if (err) {
                            return cb(err);
                        }
                        item.at = result[0];
                        delete item["at_account_id"];
                        replysAt.push(item);
                        cb(null);
                    });
                }, function (err) {
                    if (err) {
                        return callback(err);
                    }

                    return callback(null);
                });
            }

        }, function (err) {
            if (err) {
                return callback(err);
            }

            callback(null);
        });

    });
}

//删除Redis中的评论
function delCommentsByCommentId(topicId, CommentId, callback){




    async.series([_delCommentIdInSet, _delCommentIdInHashset, _reloadComment], function(err){
        if(err){
            return callback(err);
        }

        callback(null);
    });

    function _delCommentIdInSet(callback){
        redisService.zrem(2, topicId, CommentId, function(err){
            if(err){
                return callback(err);
            }

            callback(null);

        });
    }

    function _delCommentIdInHashset(callback){
        redisService.del(2, CommentId, function(err){
            if(err){
                return callback(err);
            }

            callback(null);
        });

    }

    function _reloadComment(callback) {

        reloadCommentByTopicId(topicId, function(err){
            if(err){
                return callback(err);
            }

            callback(null);
        });

    }

}

//根据单个id查询account信息
function getAccountByid(id, callback){

    var result = {
        id: null,
        username: "",
        password: null,
        nickname: null,
        photo_url: null,
        type: null,
        gender: null,
        birthday: null,
        exp: 0,
        profile: null,
        coin: 0,
        homepage_background: ""
    };

    redisService.hgetall(1, id, function(err, obj){
        var sql = "select * from accounts where id=:id";
        if(err){
            logger.error(err);
            dbHelper.execSql(sql, {id: id}, function(error, datas){
                if(error){
                    callback(error);
                    return;
                }
                if(datas.length == 0){

                    callback(null, [result]);
                    return;
                }

                callback(null, [datas[0]]);

            });
            return;
        }
        if(!obj){

            dbHelper.execSql(sql, {id: id}, function(error, datas){
                if(error){
                    callback(error);
                    return;
                }
                if(datas.length == 0){

                    callback(null, [result]);
                    return;
                }
                callback(null, [datas[0]]);

            });
        }else{
            if(obj.photo_url == "null"){
                obj.photo_url = null;
            }
            if(obj.type){
                obj.type = parseInt(obj.type);
            }
            if(obj.gender){
                obj.gender = parseInt(obj.gender);
            }
            if(obj.birthday){
                obj.birthday = parseInt(obj.birthday);
            }
            if(obj.homepage_background == "null"){
                obj.homepage_background =null;
            }
            if(obj.coin){
                obj.coin = parseInt(obj.coin);
            }
            if(obj.exp){
                obj.exp = parseInt(obj.exp);
            }
            if(obj.profile == "null"){
                obj.profile = null;
            }
            callback(null, [obj]);
        }

    });

}