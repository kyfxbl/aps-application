var dbHelper = require(FRAMEWORKPATH + "/utils/dbHelper");
var _ = require('underscore');
var async = require('async');
var uuid = require('node-uuid');

exports.getHistoryActivities = getHistoryActivities;
exports.addActivities = addActivities;
exports.deleteActivities = deleteActivities;
exports.getCandidatesCountByActivityId = getCandidatesCountByActivityId;
exports.getCandidatesByActivityId = getCandidatesByActivityId;
exports.deleteCandidateById = deleteCandidateById;
exports.updateActivities = updateActivities;
exports.deleteReportCandidate = deleteReportCandidate;
exports.getActivityTime = getActivityTime;
exports.editCandidateById = editCandidateById;
exports.postCandidate = postCandidate;
exports.queryAllTopics = queryAllTopics;
exports.queryQjcTopics = queryQjcTopics;

function getHistoryActivities(req, res, next){
    var page = req.query["page"] || 1;
    var perPage = 20;
    var startIndex = (page - 1) * perPage;
    var history_result = {history: [],count:''};

    var sql = "select * from activities order by id desc limit :startIndex,:perPage";
    var sqlCount = "select count(*) as count from activities";

    dbHelper.execSql(sql, {startIndex: startIndex,perPage: perPage}, function(err, results){
            if(err){
               return next(err);
            }
            history_result.history.push(results);
            dbHelper.execSql(sqlCount, {}, function(err, result){
                if(err){
                    return next(err);
                }
                history_result.count = result;
                doResponse(req, res, history_result);
            });
        });
}

function addActivities(req, res, next){
    var begin_time = req.body.begin_time;
    var upload_end_time = req.body.upload_end_time;
    var make_end_time = req.body.make_end_time;
    var sql = "insert into activities (upload_end_time,make_end_time,begin_time) values (:upload_end_time,:make_end_time,:begin_time)";
    dbHelper.execSql(sql, {upload_end_time: upload_end_time, make_end_time: make_end_time,begin_time: begin_time}, function(err){
        if(err){
            return next(err);
        }
        doResponse(req, res, "添加成功");
    });
}

function deleteActivities(req, res, next){
    var activityId = req.body.id;
    var sql = "delete from activities where id=:id";
    dbHelper.execSql(sql, {id: activityId}, function(err){
        if(err){
            return next(err);
        }
        doResponse(req, res, "删除成功");
    });

}

//根据活动id分页查询求教程
function getCandidatesByActivityId(req, res, next){
    var id = req.query.id;
    var page = req.query["page"] || 1;
    var perPage = parseInt(req.query["perPage"]) || 10;
    var startIndex = (page - 1) * perPage;
    var sql = "select a.id 'id',a.pic_url 'picUrl',b.username 'username',b.nickname 'nickname',a.create_date 'createDate'," +
        "a.vote_num 'voteNum',a.status 'status' " +
        "from candidates a join accounts b on a.account_id = b.id " +
        "where a.activity_id = :id and a.status <> 0 order by a.vote_num desc limit :startIndex,:perPage";
    dbHelper.execSql(sql, {id: id,startIndex: startIndex,perPage: perPage}, function(err, result){
        if(err){
            return next(err);
        }
        doResponse(req, res, result);
    });
}

//查询所有的主题
function queryAllTopics(req, res, next){
    var sql = "select * from topics";
    dbHelper.execSql(sql, {}, function(err, result){
        if(err){
            return next(err);
        }
        doResponse(req, res, result);
    });
}

//查询type为qjc的主题
function queryQjcTopics(req, res, next){
    var sql = "select a.id 'id',a.account_id 'account_id',a.title 'title'," +
        "a.create_date 'create_date',a.status 'status',a.thumb_pic_url 'thumb_pic_url' from " +
        "topics a join categories_has_topics b on a.id = b.topic_id " +
        "join topic_categories c on b.category_id = c.id " +
        "where c.type = 'qjc' order by a.create_date desc";
    dbHelper.execSql(sql, {}, function(err, result){
        if(err){
            return next(err);
        }
        doResponse(req, res, result);
    });
}

//根据活动id查询求教程数目
function getCandidatesCountByActivityId(req, res, next){
    var id = req.query.id;
    var sql = "select count(1) 'count' from candidates where activity_id = :id and status <> 0";
    dbHelper.execSql(sql, {id: id}, function(err, result){
        if(err){
            return next(err);
        }
        doResponse(req, res, result[0]);
    });
}

//根据id删除求教程
function deleteCandidateById(req, res, next){
    var id = req.params.id;
    var sql = "update candidates set status=0 where id=:id";
    dbHelper.execSql(sql, {id: id}, function(err) {
        if (err) {
            return next(err);
        }
        doResponse(req, res, {message: "ok"});
    });
}

function updateActivities(req, res, next){

    var begin_time = req.body.begin_time;
    var upload_end_time = req.body.upload_end_time;
    var make_end_time = req.body.make_end_time;
    var id = req.body.id;
    var topicId = req.body.topic_id;

    var sql = "update activities set upload_end_time = :upload_end_time, make_end_time = :make_end_time,topic_id = :topicId,begin_time = :begin_time where id=:id";
    dbHelper.execSql(sql, {upload_end_time: upload_end_time, make_end_time: make_end_time, topicId: topicId, begin_time: begin_time,id: id}, function(err){

        if(err){
            next(err);
            return;
        }

        var sql2 = "update topics set is_publish = 0 where id = :id";
        dbHelper.execSql(sql2, {id: topicId}, function(err){

            if(err){
                next(err);
                return;
            }

            doResponse(req, res, "修改成功");
        });
    });
}

//根据id取消被举报
function deleteReportCandidate(req, res, next){
    var id = req.params.id;
    var sql = "update candidates set status = 1 where id = :id";
    dbHelper.execSql(sql, {id: id}, function(err){
        if(err){
            return next(err);
        }
        doResponse(req, res, {message: "ok"});
    });
}

//根据id查询活动所处阶段
function getActivityTime(req, res, next){
    var id = req.query.id;
    var sql = "select t.begin_time 'begin_time',t.upload_end_time 'upload_end_time',t.make_end_time 'make_end_time' from activities t where t.id = :id";
    dbHelper.execSql(sql, {id: id}, function(err, results){
        if(err){
            return next(err);
        }
        var now = new Date().getTime();
        if(now < results[0].begin_time){
            var stage = 0;
            var stageName = "活动未开始";
        }else if(now > results[0].begin_time && now < results[0].upload_end_time){
            var stage = 1;
            var stageName = "上传和投票阶段";
        }else if(now > results[0].upload_end_time && now < results[0].make_end_time){
            var stage = 2;
            var stageName = "教程制作阶段";
        }else{
            var stage = 4;
            var stageName = "已结束";
        }
        doResponse(req, res, {stage: stage,stageName: stageName});
    });
}

//根据id修改投票数
function editCandidateById(req, res, next){
    var id = req.body.id;
    var voteNum = req.body.voteNum;
    var sql = "update candidates set vote_num = :voteNum where id = :id";
    dbHelper.execSql(sql, {id: id,voteNum: voteNum}, function(err){
        if(err){
            return next(err);
        }
        doResponse(req, res, {message: "ok"});
    });
}



//增加求教程
function postCandidate(req, res, next){

    var sql = "insert into candidates(id,activity_id,account_id,pic_url,create_date,vote_num) " +
        " values(:id,:activity_id,:account_id,:pic_url,:create_date,:vote_num)";
    var model = {
        id: uuid.v1(),
        activity_id: req.params.activity_id,
        account_id: req.body.account.id,
        pic_url: req.body.pic_url,
        vote_num: req.body.voteNum,
        create_date: new Date().getTime()
    };
    dbHelper.execSql(sql, model, function(err){
        if(err){
            next(err);
        }
        doResponse(req, res, {message: "ok"});
    });
}