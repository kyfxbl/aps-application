var fs = require('fs');
var logger = require(FRAMEWORKPATH + "/utils/logger").getLogger();
var dbHelper = require(FRAMEWORKPATH + "/utils/dbHelper");
var topicDao = require("./topicDao");
var request = require("request");
var _ = require("underscore");
var path = require("path");
var async = require("async");

//查询首页数据
//查询所有系列
//新增系列
//删除系列
//修改系列
//置顶主题
//取消主题置顶
//所有主题列表
//查询制定主题系列的主题列表
exports.index           = index;
exports.addPoster       = addPoster;
exports.deletePoster    = deletePoster;
exports.editPoster      = editPoster;
exports.addTopicCate    = addTopicCate;
exports.updateTopicCate = updateTopicCate;
exports.deleteTopicCate = deleteTopicCate;
exports.topic2Top       = topic2Top;
exports.cancelTopic2Top = cancelTopic2Top;
exports.createIndex     = createIndex;
exports.saveHtml        = saveHtml;
exports.updateRedis     = updateRedis;
//var host = "http://115.29.197.83";
var host = "http://115.29.197.83";



function index(req, res, next){
    var indexObject = {
        posters:[
        ],
        channels:[
        ]
    };
    async.series([indexDate,listTopTopicCate],function(error){
        if(error){
            logger.error(error);
            next(error);
        }else{
            doResponse(req, res, indexObject);
        }
    });
    function indexDate(nextStep){
        topicDao.getPosters(function(error,posters){
            if(error){
                nextStep(error);
            }else{
                indexObject.posters = posters;
                nextStep(null);
            }
        });
    }

    function listTopTopicCate(nextStep){
        topicDao.listTopTopicCate(function(error,list){
            if(error){
                logger.error(error);
                nextStep(error);
            }else{
                indexObject.channels = list;
                nextStep(null);
            }
        });
    }
}
//新增主题系列
function addTopicCate(req, res, next){
    var topicCate = {
        name:req.body.name,
        serial_number:req.body.serial_number,
        pic_url: req.body.pic_url,
        type: req.body.serial_type == "activity" ? "activity" : "",
        home_menu_icon: req.body.home_menu_icon,
        need_show:req.body.need_show
    };
    topicDao.addTopicCate(topicCate,function(error,data){
        if(error){
            next(error);
        }else{
            doResponse(req, res, data);
        }
    });
}

//修改主题系列
function updateTopicCate(req, res, next){
    var topicCate = {
        id: req.body.id,
        name: req.body.name,
        serial_number: req.body.serial_number,
        pic_url: req.body.pic_url,
        home_menu_icon: req.body.home_menu_icon,
        need_show:req.body.need_show
    };
    topicDao.updateTopicCate(topicCate,function(error,data){
        if(error){
            next(error);
        }else{
            doResponse(req, res, data);
        }
    });
}

//删除主题系列
function deleteTopicCate(req, res, next){
    var topicCateId = req.param("id");

    topicDao.deleteTopicCate(topicCateId,function(error){
        if(error){
            logger.error(error);
            next(error);
        }else{
            doResponse(req, res, {
            });
        }
    });
}

//置顶主题
function topic2Top(req, res, next){
    var topic_id = req.param("topic_id");
    var category_id = req.param("category_id");
    topicDao.updateTopicCatePromote(topic_id,category_id,1,function(error){
        if(error){
            logger.error(error);
            next(error);
        }else{
            doResponse(req, res, {
            });
        }
    });
}

//取消置顶主题
function cancelTopic2Top(req, res, next){
    var topic_id = req.param("topic_id");
    var category_id = req.param("category_id");
    topicDao.updateTopicCatePromote(topic_id,category_id,0,function(error){
        if(error){
            logger.error(error);
            next(error);
        }else{
            doResponse(req, res, {
            });
        }
    });
}

//添加轮播图
function addPoster(req, res, next){
    var poster = {
        pic_url:req.body.pic_url,
        topic_id:req.body.topic_id,
        serial_number:req.body.serial_number,
        type: req.body.posterType,
        activity_url: req.body.activity_url,
        create_date:(new Date()).getTime(),
        active:1
    };
    topicDao.addPoster(poster,function(error){
        if(error){
            logger.error(error);
            next(error);
        }else{
            doResponse(req, res, {});
        }
    });
}

//编辑轮播图
function editPoster(req, res, next){
    var poster = {
        id:req.body.id,
        pic_url:req.body.pic_url,
        serial_number:req.body.serial_number
    };
    topicDao.editPoster(poster,function(error){
        if(error){
            logger.error(error);
            next(error);
        }else{
            doResponse(req, res, {msg:'ok'});
        }
    });
}

function deletePoster(req, res, next){
    var posterId = req.param("posterId");

    topicDao.deletePoster(posterId,function(error){
        if(error){
            logger.error(error);
            next(error);
        }else{
            doResponse(req, res, {
            });
        }
    });
}

function searchTopic(req, res, next){

}

function createIndex(req, res, next){

    var posterUrl = host+'/vapi2/nailstar/posters';
    var latestUrl = host+'/vapi2/nailstar/latestTopic';
    var hotestUrl = host+'/vapi2/nailstar/latestTopic';
    var categoriesUrl = host+'/vapi2/nailstar/categories';
    var posters = [];
    var lastestTopics = [];
    var hotestTopics = [];
    var categories = [];

    var options1 = {
        method: "GET",
        uri: posterUrl,
        json: true
    };
    var options2 = {
        method: "GET",
        uri: categoriesUrl,
        json: true
    };
    var options3 = {
        method: "GET",
        uri: latestUrl,
        json: true
    };
    var options4 = {
        method: "GET",
        uri: hotestUrl,
        json: true
    };

    async.parallel([_getPosters, _getLatestTopics, _getHostestTopics, _getCategories], function(err, data){
        if(err){
            next(err);
            return;
        }
        res.render("index", {posters: posters,lastestTopics:lastestTopics,hotestTopics:hotestTopics, categories:categories});
    });

    function _getPosters(callback){
        request(options1, function(err, response, body){
            if(err){
                console.log(err);
                callback(err);
                return;
            }
            else if(body.code != 0){
                callback(body.error);
                return;
            }
            posters = body.result.posters;
            callback();
        });
    }
    function _getLatestTopics(callback){
        request(options3, function(err, response, body){
            if(err){
                console.log(err);
                callback(err);
                return;
            }
            else if(body.code != 0){
                callback(body.error);
                return;
            }
            _.each(body.result.topics, function(topic){
                topic.time = ChangeDateFormat(topic.time);
            });
            lastestTopics = body.result.topics;
            callback();
        });
    }
    function _getHostestTopics(callback){
        request(options4, function(err, response, body){
            if(err){
                console.log(err);
                callback(err);
                return;
            }
            else if(body.code != 0){
                callback(body.error);
                return;
            }
            _.each(body.result.topics, function(topic){
                topic.time = ChangeDateFormat(topic.time);
            });
            hotestTopics = body.result.topics;
            callback();
        });
    }
    function _getCategories(callback){
        request(options2, function(err, response, body){
            if(err){
                console.log(err);
                callback(err);
                return;
            }
            else if(body.code != 0){
                callback(body.error);
                return;
            }
            categories = body.result.categories;
            callback();
        });
    }

}
function saveHtml(req, res, next){
    var url = global["_g_clusterConfig"].baseurl+"/index/createIndex";
    var options = {
        method: "GET",
        uri: url
    };

    request(options, function(err, response,body){

        if(err){
            next(err);
            return;
        }

        fs.writeFile(path.join(__dirname, 'index.html'), body, function(err){

            if(err){
                return next(err);
            }
            doResponse(req, res, {messages: "ok"});

            //ossClient.putHTMLStaticByPath(ossClient.BUCKETMAP.static_bucket, "index.html", path.join(__dirname, 'index.html'), function(error){
            //
            //    if(error){
            //        next(error);
            //        return;
            //    }
            //
            //    jsdom.env({
            //        url: "http://s.yilos.com",
            //        scripts: ["./3rd-lib/jquery.js"],
            //        done: function (errors, window) {
            //
            //            if(errors){
            //                console.log(errors);
            //                return next(errors);
            //            }
            //
            //            var $ = window.$;
            //
            //            if(!$){
            //                console.log("$ is undefined");
            //                return next({errorMsg: "$ is undefined"});
            //            }
            //
            //            async.series([appendCss,appendZepto,appendOther], function(e){
            //                if(e){
            //                    console.log(e);
            //                    return next(e);
            //                }
            //                $("body").html().replace(/\t/g,"").replace(/[\n\r]/g,"");
            //                var finalHtml = "<!DOCTYPE html><html lang='zh-CN'><head>" + $("head").html() + "</head><body>" + $("body").html() + "</body></html>";
            //                fs.writeFile(path.join(__dirname, 'index.html'), finalHtml, function(erro){
            //                    if(erro){
            //                        return next(erro);
            //                    }
            //
            //                    ossClient.putHTMLStaticByPath(ossClient.BUCKETMAP.static_bucket, "index.html", path.join(__dirname, 'index.html'), function(er){
            //                        if(er){
            //                            console.log(er);
            //                            return next(er);
            //                        }else{
            //                            console.log("UPLOAD SUCCESS!");
            //                            doResponse(req, res, {message: "saveHtml ok"});
            //                        }
            //                    });
            //                });
            //            });
            //            function appendCss(callback){
            //                request({method:"GET",uri:$("head link:eq(0)").attr("href")},function(er, res, content){
            //                    if(er){
            //                        console.log(er);
            //                        callback(er);
            //                        return;
            //                    }
            //                    $("head link:eq(0)").remove();
            //                    var compress = content.replace(/\t/g,"").replace(/[\n\r]/g,"").replace(/\ +/g,"");
            //                    $("head").append("<style type='text/css'>" +  compress + "</style>");
            //                    request({method:"GET",uri:$("head link:eq(0)").attr("href")},function(er, res, content2){
            //                        if(er){
            //                            console.log(er);
            //                            callback(er);
            //                            return;
            //                        }
            //                        $("head link:eq(0)").remove();
            //                        $("head").append("<style type='text/css'>" +  content2 + "</style>");
            //                        callback(null);
            //                    });
            //
            //                });
            //            }
            //            function appendZepto(callback){
            //                var zeptoUrl = $("script:first").attr("src");
            //                request({method:"GET", uri: zeptoUrl}, function(er, response, content){
            //                    if(er){
            //                        console.log(er);
            //                        callback(er);
            //                    }
            //                    $("script:first").remove();
            //                    $("title").after("<script>" + content + "</script>");
            //                    callback(null);
            //                });
            //            }
            //            function appendOther(callback){
            //                var slideUrl = $("script:eq(1)").attr("src");
            //                var indexUrl = $("script:eq(2)").attr("src");
            //                request({method:"GET", uri: slideUrl}, function(er, response, content){
            //                    if(er){
            //                        console.log(er);
            //                        callback(er);
            //                    }
            //                    $("script:eq(1)").remove();
            //                    $("script:first").after("<script>" + content + "</script>");
            //                    callback(null);
            //                });
            //
            //            }
            //
            //        }
            //    });
            //
            //
            //});
        });
    });
}

function ChangeDateFormat (time) {
    var date = new Date(time);
    var today = new Date();
    var result = today - date;
    if(result/1000<=60){
        return "刚刚";
    }
    else if(result/1000>60 && result/1000<=3600){
        return Math.floor((result/1000)/60)+ "分钟前";
    }
    else if(result/1000>3600 && result/1000<=86400){
        return Math.floor((result/1000)/3600)+ "小时前";
    }
    else if(result/1000>86400 && result/1000<=86400*30){
        return Math.floor((result/1000)/86400)+ "天前";
    }
    else{
        return (date.getMonth() + 1) + "月" + date.getDate() + "日";
    }

}

//首页数据静态化
function updateRedis(req, res, next){
    require('./redisTask').work(function(err){
        if(err){
            return next(err);
        }
        doResponse(req, res, {message: "ok"});
    });
}