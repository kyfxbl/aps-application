var dbHelper = require(FRAMEWORKPATH + "/utils/dbHelper");
var uuid = require('node-uuid');
var JPush = require('jpush-sdk');
var async = require("async");
var _ = require("underscore");

exports.getMessage     = getMessage;
exports.getMessgeCount = getMessgeCount;
exports.deleteMessage  = deleteMessage;
exports.addMessage     = addMessage;
exports.getLatestTopic = getLatestTopic;
exports.pushPost       = pushPost;
exports.pushSingleMessage  = pushSingleMessage;

//查询系统消息
function getMessage(req, res, next){
    var page = req.query["page"] || 1;
    var perPage = parseInt(req.query["perPage"]) || 10;
    var startIndex = (page - 1) * perPage;
    var sql = "select s.id 'id',s.title 'title',s.content 'content',s.publish_date 'publish_date',t.title 'topic_name', p.title 'post_name'" +
        " from system_messages s left join topics t  on s.topic_id=t.id left join posts p on s.post_id=p.id  limit :startIndex,:perPage";
    dbHelper.execSql(sql, {startIndex: startIndex,perPage: perPage}, function(err, result){
        if(err){
            return next(err);
        }
        doResponse(req, res, result);
    });
}

//查询系统消息数量
function getMessgeCount(req, res, next){
    var sql = "select count(1) 'count' from system_messages";
    dbHelper.execSql(sql, {}, function(err, result){
        if(err){
            return next(err);
        }
        doResponse(req, res, result[0]);
    });
}

//根据id删除消息
function deleteMessage(req, res, next){
    var id = req.params.id;
    var sql = "delete from system_messages where id = :id";
    dbHelper.execSql(sql, {id: id}, function(err){
        if(err){
            return next(err);
        }
        doResponse(req, res, {message: "ok"});
    });
}

//增加系统消息
function addMessage(req, res, next){
    var data = req.body;
    var client = JPush.buildClient('0762d9941dc6cd6e6963d70c', 'c60a5b43d831cad387d3120a', null, true);
    data.id = uuid.v1();
    data.publish_date = new Date().getTime();
    var sql = "insert into system_messages(id,title,content,publish_date,topic_id) " +
        "values(:id,:title,:content,:publish_date,:topicId)";
    dbHelper.execSql(sql, data, function(err){
        if(err){
            return next(err);
        }

        doResponse(req, res, {message: "ok"});

        client.push().setPlatform('ios')
            .setAudience(JPush.ALL)
            .setNotification(data.content, JPush.ios(data.content, 'happy.caf', 1, true, {'topicId': data.topicId}))
            .setOptions(null, null, null, true, null)
            .send(function(error, response){
                if(error){
                    console.log(error.message);
                    return;
                }
            });
    });
}

//查询最近的主题
function getLatestTopic(req, res, next){
    var sql = "select id,title from topics order by create_date desc limit 0,10";
    dbHelper.execSql(sql, {}, function(err, results){
        if(err) return next(err);
        doResponse(req, res, {topics: results});
    });
}

//推送帖子
function pushPost (req, res, next) {
    var content      = req.body.content;
    var postId       = req.body.post_id;
    var title        = req.body.title;
    var id           = uuid.v1();
    var publish_date = new Date().getTime();

    var sql = "insert into system_messages(id,title,content,publish_date,post_id) " +
        "values(:id,:title,:content,:publish_date,:post_id)";

    var client = JPush.buildClient('0762d9941dc6cd6e6963d70c', 'c60a5b43d831cad387d3120a', null, true);

    dbHelper.execSql(sql, {id: id, title: title, content: content, publish_date: publish_date, post_id: postId}, function(err){
        if(err){
            return next(err);
        }

        doResponse(req, res, {message: 'ok'});

        client.push().setPlatform('ios')
        .setAudience(JPush.ALL)
        .setNotification(content, JPush.ios(content, 'happy.caf', 1, true, {'postId': postId}))
        .setOptions(null, null, null, true, null)
        .send(function(error, response){
            if(error){
                console.log(error.message);
                return;
            }
        });
    }); 
}

// 推送点对点消息
function pushSingleMessage(req, res, next){

    doResponse(req, res, {message: "ok"});

    var content = req.body.content || "empty content";
    var targets = req.body.targets || "";

    var phones = targets.split(",");

    var sql = "select id from accounts where username = :username";

    var uids = [];

    async.each(phones, function(phone, next){

        dbHelper.execSql(sql, {username: phone}, function(err, results){

            _.each(results, function(result){
                uids.push(result.id.replace(/-/g, "_"));
            });

            next(null);
        });

    }, function(err){

        if(err){
            console.log(err);
        }

        var client = JPush.buildClient('0762d9941dc6cd6e6963d70c', 'c60a5b43d831cad387d3120a', null, true);

        client.push().setPlatform('ios')
            .setAudience(JPush.alias(uids))
            .setNotification(content, JPush.ios(content, 'happy.caf', 1, true, {}))
            .setOptions(null, null, null, true, null)
            .send(function(error, response){

                if(error){
                    console.log(error.message);
                }
            });
    });
}
