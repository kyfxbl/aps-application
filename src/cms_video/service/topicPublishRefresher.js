var async = require('async');
var _ = require("underscore");
var dbHelper = require(FRAMEWORKPATH + "/utils/dbHelper");

// 10秒钟刷新一次
var TIME_INTERVAL_REFRESH = 1000 * 10;

var GATHER_SQL = "select t.id from topics t inner join activities a on t.id = a.topic_id where a.begin_time < :now and a.make_end_time < :now and t.is_publish = 0";
var REFRESH_SQL = "update topics set is_publish = 1, create_date = :now where id = :id";

exports.start = start;

function start(){
    setInterval(doRefresh, TIME_INTERVAL_REFRESH);
}

function doRefresh(){

    var wait_for_refresh = [];

    var now = new Date().getTime();

    dbHelper.execSql(GATHER_SQL, {now: now}, function(err, results){

        if(err){
            console.log(err);
            return;
        }

        _.each(results, function(item){
            wait_for_refresh.push(item.id);
        });

        async.each(wait_for_refresh, function(id, callback){

            dbHelper.execSql(REFRESH_SQL, {now: now, id: id}, function(err){
                callback(err);
            });

        }, function(err){

            if(err){
                console.log(err);
            }
        });
    });
}

