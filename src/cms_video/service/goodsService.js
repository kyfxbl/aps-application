var logger = require(FRAMEWORKPATH + "/utils/logger").getLogger();
var uuid = require('node-uuid');
var goodsDao = require("./goodsDao.js");

exports.getcategoryList     = getcategoryList;
exports.createnewCategory   = createnewCategory;
exports.editcategory        = editcategory;
exports.deletecategory      = deletecategory;
exports.getgoodsList        = getgoodsList;
exports.getsearchList       = getsearchList;
exports.createnewGoods      = createnewGoods;
exports.editgoods           = editgoods;
exports.deletegoods         = deletegoods;
exports.togglegoods         = togglegoods;
exports.getnotshowList      = getnotshowList;
exports.getgoods            = getgoods;
exports.togglehot			= togglehot;
exports.gethotGoods			= gethotGoods;
exports.getWebList			= getwebList;
exports.createNewWeb		= createnewWeb;
exports.editWeb				= editweb;
exports.deleteWeb			= deleteweb;
exports.toggleWeb			= toggleweb;

function getcategoryList(req, res, next) {
	
	goodsDao.getCategoryList(function (err,result) {
		if(err){
			logger.error(err);
			next(err);// next(err) send error response to client
			return;
		}
		doResponse(req, res, result);// doResponse send response to client
	});
}

function createnewCategory (req, res, next) {

	var data = {
		id:  uuid.v1(),
		name: req.body.name,
		create_date: (new Date()).getTime(),
		pic_url: req.body.pic_url
	};

	goodsDao.createNewCategory(data, function (err,result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, {message:'ok'});
	});
}

function editcategory (req, res, next) {
	
	var data = {
		id:  req.body.id,
		name: req.body.name,
		pic_url: req.body.pic_url
	};

	goodsDao.editCategory(data, function (err,result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, {message:'ok'});
	});
}

function deletecategory (req, res, next) {

	var category_id = req.body.category_id;

	goodsDao.deleteCategory(category_id, function (err,result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, {message: result});
	});
}

function getgoodsList (req, res, next) {
	
	var category_id = req.query.category_id;
	var page        = req.query.page;
	var perPage     = req.query.perPage;

	goodsDao.getGoodsList(category_id,page,perPage, function (err,result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res,result);
	});
}

function getsearchList (req, res, next) {

	var key     = req.query.key;
	var page    = req.query.page;
	var perPage = req.query.perPage;
	var type    = req.query.type;  		// 0 for all products, and 1 for available products
	
	goodsDao.getSearchList(key,page,perPage,type, function (err,result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res,result);
	});
}

function createnewGoods (req, res, next) {
	
	var data = {
		id: uuid.v1(),
		category_id: req.body.category_id,
		name: req.body.name,
		price: req.body.price,
		vendor_type: 0,
		url: req.body.url,
		pic_url: req.body.pic_url,
		description: req.body.description,
		available: req.body.available,
		create_date: (new Date()).getTime(),
		real_id: req.body.real_id,
		cost: req.body.cost,
		is_hot: req.body.is_hot
	};

	goodsDao.createNewGoods(data, function (err,result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, result);
	});
}

function editgoods (req, res, next) {
	
	var data = {
		id: req.body.id,
		category_id: req.body.category_id,
		name: req.body.name,
		price: req.body.price,
		vendor_type: null,
		url: req.body.url,
		pic_url: req.body.pic_url,
		description: req.body.description,
		available: req.body.available,
		real_id: req.body.real_id,
		cost: req.body.cost,
		is_hot: req.body.is_hot
	};

	goodsDao.editGoods(data, function (err,result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, result);
	});
}

function deletegoods (req, res, next) {
	
	var goods_id = req.body.id;

	goodsDao.deleteGoods(goods_id, function (err,result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, {message:'ok'});
	});
}

function togglegoods (req, res, next) {
	
	var goods_id  = req.body.id;
	var available = req.body.available;

	goodsDao.toggleGoods(goods_id, available, function (err,result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, result);
	});
}

function togglehot (req, res, next) {
	var goods_id  = req.body.id;
	var is_hot = req.body.is_hot;

	goodsDao.toggleHot(goods_id, is_hot, function (err,result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, result);
	});
}

function getgoods (req, res, next) {
	var id = req.query.id;

	goodsDao.getGoods(id, function (err,result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, result);
	});
}

function getnotshowList (req, res, next) {
	
	var page    = req.query.page;
	var perPage = req.query.perPage;

	goodsDao.getNotShowList(page,perPage, function (err,result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, result);
	});
}

function gethotGoods (req, res, next) {
	
	goodsDao.getHotGoods(function (err,result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, result);
	});
}

function getwebList  (req, res, next) {

	var page        = req.query.page;
	var perPage     = req.query.perPage;

	goodsDao.getWebList(page,perPage,function (err,result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, result);
	});
}

function createnewWeb (req, res, next) {

	var data = {
		id:uuid.v1(),
		activity_url: req.body.activity_url,
		available: req.body.available,
		create_date:(new Date()).getTime(),
		height:req.body.height,
		name:req.body.name
	};

	goodsDao.createNewWeb(data,function (err,result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, {msg:'ok'});
	});
}

function editweb (req, res, next) {
	var data = {
		id:req.body.id,
		activity_url: req.body.activity_url,
		available: req.body.available,
		height:req.body.height,
		name:req.body.name
	};

	goodsDao.editWeb(data,function (err,result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, {msg:'ok'});
	});
}

function deleteweb (req, res, next) {

	var id = req.body.id;

	goodsDao.deleteWeb(id,function (err,result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, {msg:'ok'});
	});
}

function toggleweb (req, res, next) {

	var id        = req.body.id;
	var available = req.body.available;
	
	goodsDao.toggleWeb(id,available,function (err,result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, {msg:'ok'});
	});
}