var fs = require('fs');
var crypto = require('crypto');

var logger = require(FRAMEWORKPATH + "/utils/logger").getLogger();

var materialDao = require("./materialDao");
exports.uploadVideo = uploadVideo;
exports.uploadPicture = uploadPicture;
exports.listVideo = listVideo;
exports.deleteVideo = deleteVideo;


function listVideo(req, res, next){
    var result = {
        pageData:[],
        total:0
    }
    async.series([queryPageData,queryTotal],function(error){
        if(error){
            logger.error(error);
            next(error);
        }else{
            doResponse(req, res, result);
        }
    });

    function queryPageData(nextStep){
        var whereSql,
            whereVar={};
        var pageSize = parseInt(req.query["s"]);
        if(_.isNaN(pageSize)){
            pageSize = 0;
        }
        var pageNo = parseInt(req.query["p"]) ;
        if(_.isNaN(pageNo)){
            pageNo = 0;
        }
        whereSql = " 1=1 order by upload_date desc LIMIT "+pageSize*(pageNo-1)+","+pageSize+";";

        materialDao.listVideo(whereSql,whereVar,function(error,videoList){
            if(error){
                logger.error(error);
                nextStep(error);
            }else{
                result.pageData = videoList;
                nextStep(null,videoList);
            }
        });
    }

    function queryTotal(nextStep){
        var whereSql,
            whereVar={};
        whereSql = " 1=1 ";

        materialDao.quertVideoTotal(whereSql,whereVar,function(error,total){
            if(error){
                logger.error(error);
                nextStep(error);
            }else{
                result.total = total;
                nextStep(null,total);
            }
        });
    }

}

function uploadVideo(req, res, next){
    req.form.on('progress', function (bytesReceived, bytesExpected) {
    });
    req.form.on('end', function () {
        var filePath = req.files.files[0].path;
        videofile ={
            name: req.body.name,
            size: req.files.files[0].size,
            oss_url:"",
            cc_url:"",
            upload_date:(new Date()).getTime()
        };

        async.series([upload2Oss,add2Db,deleteTempFile],function(error){
            if(error){
                logger.error(error);
                next(error);
            }else{
                doResponse(req, res, {
                    files:[videofile]
                });
            }
        })
        function upload2Oss(nextStep){
                ossClient.putVideoObjectToOss(filePath,function(error,ossObject){
                    if(error){
                        nextStep(error);
                        return;
                    }else{
                        videofile.oss_url = ossObject.oss_url;
                        nextStep(null);

                    }
                });
        }
        function add2Db(nextStep){
            materialDao.addVideo(videofile,function(error){
                if(error){
                    nextStep(error);
                }else{
                    nextStep(null, {
                        files:[videofile]
                    });
                }
            });
        }
        function deleteTempFile(nextStep){
            fs.unlink(filePath, function (error) {
                if (error) {
                    logger.error("删除文件：" + filePath + "失败");
                    logger.error(error);
                    nextStep(error);
                    return;
                }
                nextStep(null);
            });
        }
    });
}


function uploadPicture(req, res, next){
    req.form.on('progress', function (bytesReceived, bytesExpected) {
    });
    req.form.on('end', function () {
        var filePath = req.files.files[0].path;
        picfile ={
            size: req.files.files[0].size,
            url:"",
            upload_date:(new Date()).getTime()
        };

        async.series([upload2Oss,add2Db,deleteTempFile],function(error){
            if(error){
                logger.error(error);
                next(error);
            }else{
                doResponse(req, res, {
                    files:[picfile]
                });
            }
        })


        function upload2Oss(nextStep){
            ossClient.putPictureObjectToOss(filePath,function(error,ossObject){
                if(error){
                    nextStep(error);
                    return;
                }else{
                    picfile.url = ossObject.oss_url;
                    nextStep(null);
                }
            });
        }
        function add2Db(nextStep){
            materialDao.addPicture(picfile,function(error){
                if(error){
                    nextStep(error);
                }else{
                    nextStep(null, {
                        files:[picfile]
                    });
                }
            });
        }
        function deleteTempFile(nextStep){
            fs.unlink(filePath, function (error) {
                if (error) {
                    logger.error("删除文件：" + filePath + "失败");
                    logger.error(error);
                    nextStep(error);
                    return;
                }
                nextStep(null);
            });
        }
    });
}


function deleteVideo(req, res, next){
    var videoId = req.param("id");

    materialDao.deleteVideo(videoId,function(error){
        if(error){
            logger.error(error);
            next(error);
        }else{
            doResponse(req, res, {
            });
        }
    });

}

function newVideoTopic(req, res, next){
    //构造视频对象
    //构造视频标签
}