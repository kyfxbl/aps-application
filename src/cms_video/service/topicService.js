var fs                    = require('fs');
var md5                   = require('MD5');
var crypto                = require('crypto');
var uuid                  = require('node-uuid');
var sqlHelper             = require(FRAMEWORKPATH + "/db/sqlHelper");
var dbHelper              = require(FRAMEWORKPATH + "/utils/dbHelper");

var logger                = require(FRAMEWORKPATH + "/utils/logger").getLogger();
var topicDao              = require("./topicDao");

exports.listTopic         = listTopic;
exports.queryTags         = queryTags;
exports.countTags         = countTags;
exports.addTopic          = addTopic;
exports.updateTopic       = updateTopic;
exports.deleteTopic       = deleteTopic;
exports.getAuthorAccounts = getAuthorAccounts;
exports.addTopicTag       = addTopicTag;
exports.deleteTopicTag    = deleteTopicTag;
exports.getTopic          = getTopic;

exports.getAllTopic       = getAllTopic;
exports.deleteTopicTrue   = deleteTopicTrue;
exports.getSearchTopic    = getSearchTopic;
exports.togglepublish     = togglepublish;
exports.getnopublish      = getnopublish;

function listTopic(req, res, next){
    var result = {
        pageData:[],
        total:0
    };
    var whereSql="",
        whereVar={};

    var tmp_result = {};
    async.series([queryPageData,queryTotal,queryAccount,queryLikeCount,queryCollectCount],function(error){
        if(error){
            logger.error(error);
            next(error);
        }else{
            result.pageData = tmp_result.pageData;
            doResponse(req, res, result);
        }
    });

    function queryPageData(nextStep){


        var querys = req.query;

        _.each(querys,function(value,key){
            if(key.indexOf("_search_")!=-1){
                var field = key.substr(8,key.length);
                if(field == "category_id"){
                    whereSql = whereSql+" t.id in (select topic_id from categories_has_topics where category_id=:category_id) and ";
                }else{
                    whereSql = whereSql+" "+field+"=:"+field+" and ";
                }
                whereVar[key.substr(8)] = value;
            }
        });

        var pageSize = parseInt(req.query["s"]);
        if(_.isNaN(pageSize)){
            pageSize = 10;
        }
        var pageNo = parseInt(req.query["p"]) ;
        if(_.isNaN(pageNo)){
            pageNo = 1;
        }
        whereSql = whereSql+" 1=1 order by t.create_date  ";
        topicDao.listTopic(whereSql+" LIMIT "+pageSize*(pageNo-1)+","+pageSize+";",whereVar,function(error,videoList){
            if(error){
                logger.error(error);
                nextStep(error);
            }else{
                tmp_result.pageData = videoList;
                nextStep(null,videoList);
            }
        });
    }

    function queryTotal(nextStep){
        topicDao.quertTopicTotal(whereSql,whereVar,function(error,total){
            if(error){
                logger.error(error);
                nextStep(error);
            }else{
                result.total = total;
                nextStep(null,total);
            }
        });
    }

    function queryAccount(nextStep){
        async.eachSeries(tmp_result.pageData, function(item, next){
            getAccountByid(item.account_id, function(err, account_result){
                if(err){
                    return next(err);
                }
                _.map(tmp_result.pageData, function(tmp_item){
                    if(tmp_item.id == item.id){
                        tmp_item.account = account_result[0];
                    }
                });
                next();
            });
        }, function(err){
            if(err){
                return nextStep(err);
            }
            nextStep();
        });
    }

    function queryLikeCount(nextStep){
        async.each(tmp_result.pageData, function(item, next){
            var sql = "select count(1) 'count' from topic_actions where action_type = '2' and topic_id = :id";
            dbHelper.execSql(sql, {id: item.id}, function(err, count_result){
                if(err){
                    return next(err);
                }
                _.map(tmp_result.pageData, function(tmp_item){
                    if(tmp_item.id == item.id){
                        tmp_item.like = count_result[0].count;
                    }
                });
                next();
            });
        }, function(err){
            if(err){
                return nextStep(err);
            }
            nextStep();
        });
    }

    function queryCollectCount(nextStep){
        async.each(tmp_result.pageData, function(item, next){
            var sql = "select count(1) 'count' from topic_actions where action_type = '3' and topic_id = :id";
            dbHelper.execSql(sql, {id: item.id}, function(err, count_result){
                if(err){
                    return next(err);
                }
                _.map(tmp_result.pageData, function(tmp_item){
                    if(tmp_item.id == item.id){
                        tmp_item.collect = count_result[0].count;
                    }
                });
                next();
            });
        }, function(err){
            if(err){
                return nextStep(err);
            }
            nextStep();
        });
    }
}

//添加主题
function addTopic(req, res, next){
   //topics、topics_has_videos、topics_has_pictures、videos、pictures

   var topic = {
       account_id:req.body.account_id,
       thumb_pic_url:req.body.thumb_pic_url,
       big_thumb_pic_url:req.body.big_thumb_pic_url,
       title:req.body.title,
       status:0,
       type: req.body.type,
       create_date:(new Date()).getTime(),
       activity_url: req.body.activity_url
   };
   if(req.body.type === "activity"){
        topic.category_id = req.body.category_ids;
        topic.id = uuid.v1();
        var sql = "insert into topics (id,title,create_date,status,thumb_pic_url,big_thumb_pic_url,type,activity_url) values " +
                "(:id,:title,:create_date,:status,:thumb_pic_url,:big_thumb_pic_url,:type,:activity_url)";
        var sql2 = "insert into categories_has_topics (id,category_id,topic_id,promote,serial_number) values (:id,:category_id,:topic_id,0,0)";
        dbHelper.execSql(sql,{
            id: topic.id,
            title: topic.title,
            create_date: topic.create_date,
            status: topic.status,
            thumb_pic_url: topic.thumb_pic_url,
            big_thumb_pic_url: topic.big_thumb_pic_url,
            type: topic.type,
            activity_url: topic.activity_url
        }, function(err, datas){
            if(err){
                logger.error(err);
                return next(err);
            }
            dbHelper.execSql(sql2, {id:uuid.v1(), category_id:topic.category_id[0], topic_id:topic.id}, function(error, data){
                if(error){
                    logger.error(error);
                    return next(error);
                }
                doResponse(req, res, {});
            });
        });
   }else{
        var pictureIds = req.body.pic_ids;
        var articles = req.body.articles;
        var tag_ids = req.body.tag_ids;
        var video_id = req.body.video_id;
        var category_ids = req.body.category_ids;
        var serial_number = req.body.serialNumber;
        var goods = req.body.goods;
        var topicObject = {
            topic:topic,
            video_id:video_id,
            category_ids:category_ids,
            serial_number:serial_number,
            tag_ids:tag_ids,
            pictureAndArticles:[
            ],
            goods:[]
        };
        for(var i=0;i<pictureIds.length;i++){
            topicObject.pictureAndArticles.push( {
                    content:articles[i],
                    pictures_id:pictureIds[i],
                    serial_number:i
                }
            );
        }

        for(var i=0;i<goods.length;i++){
            topicObject.goods.push( {
                    commodity_id:goods[i].goods_id,
                    description:goods[i].description,
                    serial_number:goods[i].serial_number
                }
            );
        }

        topicDao.addTopic(topicObject,function(error){
            if(error){
                logger.error(error);
                return next(error);
            }else{
                doResponse(req, res, {});
            }
        });
   }

}
//更新主题
function updateTopic(req, res, next){
    var topic_id = req.params["topicId"];
    var topic = {
        id: topic_id,
        account_id:req.body.account_id,
        thumb_pic_url:req.body.thumb_pic_url,
        big_thumb_pic_url:req.body.big_thumb_pic_url,
        title:req.body.title,
        status:0
    };

    var pictureIds = req.body.pic_ids;
    var articles = req.body.articles;
    var tag_ids = req.body.tag_ids;
    var video_id = req.body.video_id;
    var category_ids = req.body.category_ids;
    var serial_number = req.body.serialNum;
    var goods = req.body.goods;

    var topicObject = {
        topic:topic,
        video_id:video_id,
        category_ids:category_ids,
        tag_ids:tag_ids,
        serial_number:serial_number,
        pictureAndArticles:[

        ],
        goods:[]
    };

    for(var i=0;i<pictureIds.length;i++){
         topicObject.pictureAndArticles.push( {
             content:articles[i],
             pictures_id:pictureIds[i],
             serial_number:i
         });
    }

    for(var i=0;i<goods.length;i++){
        topicObject.goods.push( {
                commodity_id:goods[i].goods_id,
                description:goods[i].description,
                serial_number:goods[i].serial_number
            }
        );
    }

    topicDao.updateTopic(topicObject,function(error){
       if(error){
           logger.error(error);
           next(error);
       }else{
           doResponse(req, res, {});
       }
    });
}

function deleteTopic(req, res, next){
    var topicId = req.param("topic_id");
    var cateId = req.query.cateId;

    topicDao.deleteTopic(topicId,cateId,function(error){
        if(error){
            logger.error(error);
            next(error);
        }else{
            doResponse(req, res, {
            });
        }
    });
}

function queryTags(req, res, next){
    var pageSize = req.query.pageSize;
    var page = req.query.page;
    if(pageSize && page){
        pageSize = parseInt(pageSize);
        page = parseInt(page);
        var startIndex = (page-1) * pageSize;
        topicDao.queryTagsLimit(startIndex, pageSize, function(error,list){
            if(error){
                logger.error(error);
                next(error);
            }else{
                doResponse(req, res, {
                    tags:list
                });
            }
        });
    }else{
        topicDao.queryTags(function(error,list){
            if(error){
                logger.error(error);
                next(error);
            }else{
                doResponse(req, res, {
                    tags:list
                });
            }
        });
    }
}

function countTags(req, res, next){
    var sql = "select count(1) 'count' from tags";
    dbHelper.execSql(sql, {}, function(err, result){
        if(err){
            return next(err);
        }
        doResponse(req, res, result[0]);
    });
}

//添加标签
function addTopicTag (req,res,next) {
	var topicTag ={
		name:req.body.name
	};
	topicDao.addTag(topicTag, function (error) {
		if (error) {
			next(error);
            return;
		}

        doResponse(req, res, topicTag);
	})
}

//删除标签
function deleteTopicTag (req,res,next) {
    var tagId = req.param("id");
    topicDao.deleteTag(tagId,function (error) {
       if(error){
            logger.error(error);
            next(error);
        }else{
            doResponse(req, res, {});
        }
    });
}

function getAuthorAccounts(req, res, next){
    topicDao.getAuthorAccounts(function(error,list){
        if(error){
            logger.error(error);
            next(error);
        }else{
            doResponse(req, res, {
                authors:list
            });
        }
    });
}

function getTopic(req, res, next){

    var topic_id = req.params["topicId"];

    //主sql，先获取topic相关信息
    var mainSql = "select t.id 'id',t.title 'title',t.account_id 'account_id',t.thumb_pic_url 'thumbPicUrl',t.big_thumb_pic_url 'bigThumbPicUrl' " +
        "from topics t where t.id = :topic_id";

    dbHelper.execSql(mainSql, {topic_id: topic_id}, function(err, result){

        if(err) return next(err);
        if(!result || !result[0] || !result[0].id){
            err = new AccountError(1, "视频不存在");
            return next(err);
        }

        getAccountByid(result[0]["account_id"], function(err, account_result){

            if(err){
                return next(err);
            }
            //检查数据的关联账号
            if(!account_result || !account_result[0]){
                return next(new AccountError(1, "数据错误，关联账号不存在"));
            }

            var topic_result = {
                id: result[0]["id"],
                title: result[0]["title"],
                authorId: account_result[0]["id"],
                author: account_result[0]["nickname"],
                authorPhoto: account_result[0]["photo_url"],
                createDate: result[0]["createDate"],
                thumbPicUrl: result[0]["thumbPicUrl"],
                bigThumbPicUrl: result[0]["bigThumbPicUrl"]
            };

            async.parallel([_queryTags, _queryVideos, _queryPictures, _queryArticles, _queryCategories,_getGoodsId], function(err, result){

                if(err) return next(err);

                topic_result.tags = result[0];
                topic_result.videos = result[1];
                topic_result.pictures = result[2];
                topic_result.articles = result[3];
                topic_result.category_ids = result[4];
                topic_result.goods = result[5];

                return doResponse(req, res, topic_result);
            });

            //查询标签数组信息
            function _queryTags(callback){

                var sql = "select b.id 'id' " +
                    "from topics_has_tags a join tags b on a.tag_id = b.id " +
                    "where a.topic_id = :topic_id";

                dbHelper.execSql(sql, {topic_id: result[0]["id"]}, function(err, results){

                    if(err) return callback(err);

                    var tags = [];
                    _.each(results, function(item){
                        tags.push(item);
                    });

                    return callback(null, tags);
                });

            }

            //查询视频数组信息
            function _queryVideos(callback){

                var sql = "select b.id 'videoId' " +
                    "from topics_has_videos a left join videos b on a.video_id = b.id " +
                    "where a.topic_id = :topic_id order by a.serial_number";

                dbHelper.execSql(sql, {topic_id: result[0]["id"]}, function(err, results){

                    if(err) return callback(err);

                    var videos = [];
                    _.each(results, function(item){
                        videos.push(item);
                    });

                    return callback(null, videos);
                });
            }

            //查询图片数组信息
            function _queryPictures(callback){

                var sql = "select b.url 'url',b.id 'id' " +
                    "from topics_has_pictures a join pictures b on a.picture_id = b.id " +
                    "where a.topic_id = :topic_id order by a.serial_number";

                dbHelper.execSql(sql, {topic_id: result[0]["id"]}, function(err, results){

                    if(err) return callback(err);

                    var pictures = [];
                    _.each(results, function(item){
                        pictures.push(item);
                    });

                    return callback(null, pictures);
                });
            }

            //查询文章数组信息
            function _queryArticles(callback){

                var sql = "select b.content 'content' " +
                    "from topics_has_articles a join articles b on a.article_id = b.id " +
                    "where a.topic_id = :topic_id order by a.serial_number";

                dbHelper.execSql(sql, {topic_id: result[0]["id"]}, function(err, results){

                    if(err) return callback(err);

                    var articles = [];
                    _.each(results, function(item){
                        articles.push(item["content"]);
                    });

                    return callback(null, articles);
                });
            }

            //查询分类数组信息
            function _queryCategories(callback){

                var sql = "select distinct category_id 'id' from categories_has_topics where topic_id = :topic_id";

                dbHelper.execSql(sql, {topic_id: result[0]["id"]}, function(err, results){

                    if(err) return callback(err);

                    var categories = [];
                    _.each(results, function(item){
                        categories.push(item);
                    });

                    return callback(null, categories);
                });
            }

            //根据topic_id 查询商品commodity_id
            function _getGoodsId (callback) {
               var sql = "select a.commodity_id 'goods_id', a.description, a.serial_number, b.pic_url, b.name from topics_has_commodities a left join commodities b on a.commodity_id=b.id where a.topic_id= :topic_id order by a.serial_number";

               dbHelper.execSql(sql, {topic_id: result[0]["id"]}, function(err, results){

                    if(err) return callback(err);

                    var goods = [];
                    _.each(results, function(item){
                        goods.push(item);
                    });

                    return callback(null, goods);
                });
            }
        });

    });
}

//获取所有的主题
//新增主题

//更新主题
//删除主题


//根据单个id查询account信息
function getAccountByid(id, callback){

    var sql = "select t.id 'id',t.username 'username',t.password 'password',t.nickname 'nickname'," +
        "t.photo_url 'photo_url',t.type 'type',t.gender 'gender',t.birthday 'birthday' " +
        "from accounts t where t.id = :id";

    dbHelper.execSql(sql, {id: id}, function(err, result){

        if(err){
            return callback(err);
        }
        callback(null, result);

    });
}

function AccountError(code, message){
    this.errorCode = code;
    this.errorMessage = message;
}
AccountError.prototype = new Error();


//获取所有topic列表（用于删除）
function getAllTopic (req, res, next) {
    var page         = req.query.page;
    var perPage      = req.query.perPage;
    topicDao.getDeleteTopicList(page, perPage, function (err,result) {
        if(err){
            logger.error(err);
            next(err);
            return;
        }
        doResponse(req, res, result);
    });
}

//获取搜索的topic列表
function getSearchTopic (req, res, next) {
    var page     = req.query.page;
    var perPage  = req.query.perPage;
    var key      = req.query.key;
    
    topicDao.getSearchTopicList(key, page, perPage, function (err,result) {
        if(err){
            logger.error(err);
            next(err);
            return;
        }
        doResponse(req, res, result);
    });
}

//彻底删除topic
function deleteTopicTrue (req, res, next) {
    var topic_id = req.body.topic_id;
    topicDao.deleteTopicData(topic_id, function (err,result) {
        if(err){
            logger.error(err);
            next(err);
            return;
        }
        doResponse(req, res, result);
    });
}

//临时下架主题
function togglepublish (req, res, next) {
    var topic_id = req.body.topic_id;
    var is_publish = req.body.is_publish;
    topicDao.togglePublish(topic_id, is_publish, function (err,result) {
        if(err){
            logger.error(err);
            next(err);
            return;
        }
        doResponse(req, res, {message:'ok'});
    });
}

//获取所有下架的主题
function getnopublish (req, res, next) {
    var page     = req.query.page;
    var perPage  = req.query.perPage;
    topicDao.getNoPushlish(page, perPage, function (err,result) {
        if(err){
            logger.error(err);
            next(err);
            return;
        }
        doResponse(req, res, result);
    });
}
