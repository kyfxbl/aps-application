var sqlHelper          = require(FRAMEWORKPATH + "/db/sqlHelper");
var dbHelper           = require(FRAMEWORKPATH + "/utils/dbHelper");
var uuid               = require('node-uuid');
var accountService     = require('./accountService');

exports.getPostList    = getPostList;		//获取帖子列表
exports.getPostImg     = getPostImg;		//获取帖子图片
exports.postTopAction  = postTopAction		//置顶和取消置顶
exports.postHotAction  = postHotAction		//推荐和取消推荐
exports.postBestAction = postBestAction		//加精和取消帖子
exports.addPosteData   = addPosteData		//新建帖子
exports.deletePostPic  = deletePostPic 		//删除帖子里面的图片
exports.addPostPic     = addPostPic			//添加帖子里的照片
exports.editPostData   = editPostData		//编辑帖子
exports.deletePostData = deletePostData		//删除帖子
exports.clearPostData  = clearPostData		//清空帖子
exports.getReportData  = getReportData		//举报的帖子
exports.getSearchData  = getSearchData		//搜索的帖子

//帖子列表（可定制分页和分页数量）
function getPostList (community_id, page,perPage,callback) {
	var startIndex 	= (page-1)*perPage;
	var result 		= {
		pageData:'',
		count:0,
		comminity_name:''
	};

	async.parallel([getlistdata, getlisnum, getcommunityname], function (err) {
		if(err){
            callback(err);
            return;
        }
        callback(null,result);
	});

	//查询帖子列表
	function getlistdata (callback) {
		var sql = "select a.id 'post_id',a.account_id 'account_id',a.create_date 'create_date',a.title 'title', "+
		"a.content 'content',a.is_reported 'is_reported',a.is_hot 'is_hot',a.is_top 'is_top', a.is_best 'is_best',b.nickname 'nickname' "+
		"from posts a left join accounts b on a.account_id=b.id where a.community_id=:community_id and a.status=1 limit :startIndex,:perPage";

		dbHelper.execSql(sql, {community_id: community_id, startIndex: startIndex, perPage: parseInt(perPage)}, function (err, data) {
			if(err){
				callback(err);
				return;
			}
			result.pageData = data;
			callback(null);
		});
	}

	//查询圈子内帖子数量
	function getlisnum (callback) {
		var sql = "select count(1) count from posts where community_id=:community_id and status=1";

		dbHelper.execSql(sql, {community_id: community_id}, function (err, data) {
			if(err){
				callback(err);
				return;
			}
			result.count = data[0].count;
			callback(null);
		});
	} 

	//查询圈子名称
	function getcommunityname (callback) {
		var sql = "select name 'name' from communities where id= :community_id";

		dbHelper.execSql(sql, {community_id: community_id}, function (err, data) {
			if(err){
				callback(err);
				return;
			}
			result.comminity_name = data[0].name;
			callback(null);
		});
	}
}

//查看帖子图片
function getPostImg (post_id, callback) {
	var sql = " select pic_url,serial_number,id 'pic_id' from post_pictures where post_id= :post_id order by serial_number";

	dbHelper.execSql(sql, {post_id: post_id}, function (err, data) {
		if(err){
			callback(err);
			return;
		}
		callback(null, data);
	});
}

//置顶和取消置顶帖子
function postTopAction (post_id, is_top, callback) {
	var sql = 'update posts set is_top= :is_top where id= :post_id';

	dbHelper.execSql(sql, {is_top: is_top, post_id: post_id}, function (err, data) {
		if(err){
			callback(err);
			return;
		}
		callback(null, data);
	});
}

//推荐和取消推荐帖子
function postHotAction (post_id, is_hot, callback) {
	var sql = 'update posts set is_hot= :is_hot where id= :post_id';

	dbHelper.execSql(sql, {is_hot: is_hot, post_id: post_id}, function (err, data) {
		if(err){
			callback(err);
			return;
		}
		callback(null, data);
	});
}

function postBestAction(post_id, is_best, callback) {
	var sql = 'update posts set is_best= :is_best where id= :post_id';

	dbHelper.execSql(sql, {is_best: is_best, post_id: post_id}, function (err, data) {
		if(err){
			callback(err);
			return;
		}
		callback(null, data);
	});
}

//删除帖子（更改字段替代删除）
function deletePostData (post_id, callback) {

	var sql = 'update posts set status=0 where id= :post_id';

	dbHelper.execSql(sql, {post_id: post_id}, function (err, data ) {
		if(err){
			callback(err);
			return;
		}
		callback(null,{message: 'ok'});
	})
}

//新建帖子
function addPosteData (data, callback) {

	var result = {
		post:'',
		imgs:''
	};

	async.parallel([addPost, addPic], function (err) {
		if(err){
            callback(err);
            return;
        }
        callback(null,result);
	});

	//存到posts表
	function addPost (callback) {
		var sql = 'insert into posts (id, community_id, create_date, title, content, is_hot, is_top, account_id,ready,latest_reply_time) '+
		'values (:id, :community_id, :create_date, :title, :content, :is_hot, :is_top, :account_id, :ready, :latest_reply_time)';

		dbHelper.execSql(sql, {id: data.id, community_id: data.community_id, create_date: data.create_date, title: data.title, content: data.content, is_hot: data.is_hot, is_top: data.is_top, account_id: data.creator_id,ready:1,latest_reply_time: data.create_date}, function (err, data) {
			if(err){
				callback(err);
				return;
			}
			result.post = 'ok';
			callback(null);
		});
	}

	//将pic_url存到post_picture中
	function addPic (callback) {

		var sql = 'insert into post_pictures (id, post_id, pic_url, serial_number, width, height)'+
			'values (:id, :post_id, :pic_url, :serial_number, :width, :height)';
		var sqlArray = [];
		for (var i = 0; i < data.imgs.length; i++) {
			sqlArray.push({
				statement: sql,
				value: {
					id: data.imgs[i].id,
					post_id: data.id, 
					pic_url: data.imgs[i].pic_url, 
					serial_number: data.imgs[i].serial_number,
					width:  data.imgs[i].width,
					height: data.imgs[i].height
				}
			});
		}
		dbHelper.bacthExecSql(sqlArray, function(err){
			if(err){
				callback(err);
				return;
			}
			result.imgs = 'ok';
			callback(null);
		});
	}
}

//删除帖子里的图片
function deletePostPic (pic_id, callback) {
	var sql = 'delete from post_pictures where id= :pic_id';
	dbHelper.execSql(sql, {pic_id: pic_id}, function (err, data) {
		if(err){
			callback(err);
			return;
		}
		callback(null, data);
	})
}

//增加帖子里的照片
function addPostPic (data, callback) {
	var sql = 'insert into post_pictures (id, post_id, pic_url, serial_number) values (:id, :post_id, :pic_url, :serial_number)';

	dbHelper.execSql(sql, {id: data.id, post_id: data.post_id, pic_url: data.pic_url, serial_number: data.serial_number}, function (err, data) {
		if(err){
			callback(err);
			return;
		}
		callback(null, data);
	});
}

//编辑帖子
function editPostData (data, callback) {
	var result = {
		post:'',
		imgs:''
	};
	async.parallel([updatePosts, updatePic], function (err) {
		if(err){
            callback(err);
            return;
        }
        callback(null,result);
	});

	//更新posts表
	function updatePosts (callback) {
		var sql = 'update posts set title= :title, content= :content, is_hot= :is_hot, is_top= :is_top, account_id= :account_id where id= :post_id';

		dbHelper.execSql(sql,{title: data.title, content: data.content, is_hot: data.is_hot, is_top: data.is_top, account_id: data.creator_id, post_id: data.post_id}, function (err, data) {
			if(err){
				callback(err);
				return;
			}
			result.post = 'ok';
			callback(null);
		});
	}

	//更新post_picture表
	function updatePic (callback) {
		var sql      = 'update post_pictures set serial_number= :serial_number where id= :pic_id';
		var sqlArray = [];
		for (var i = 0; i < data.imgs.length; i++) {
			sqlArray.push({
				statement: sql,
				value: { 
					serial_number: data.imgs[i].serial_number,
					pic_id: data.imgs[i].pic_id
				}
			});
		};
		dbHelper.bacthExecSql(sqlArray, function(err){
			if(err){
				callback(err);
				return;
			}
			result.imgs = 'ok';
			callback(null);
		});
	}
}

//清空帖子(更改字段替代删除)
function clearPostData (community_id, callback) {
	var result = {
		message:''
	};

	async.waterfall([getPostList, clearposts], function (err) {
		if(err){
            callback(err);
            return;
        }
        callback(null,result);
	});

	//先获取post_id列表
	function getPostList (callback) {
		var sql = "select id 'post_id' from posts where community_id= :community_id";
		dbHelper.execSql(sql, {community_id: community_id}, function (err, data) {
			if(err){
				callback(err);
				return;
			}
			callback(null, data);
		});
	}

	//清除posts表 （更改字段替代删除）
	function clearposts (imgs, callback) {
		var sql = 'update posts set status=0 where id= :post_id';
		var sqlArray = [];
		for (var i = 0; i < imgs.length; i++){
			sqlArray.push({
				statement:sql,
				value:{
					post_id: imgs[i].post_id
				}
			});
		};
		dbHelper.bacthExecSql(sqlArray, function(err){
			if(err){
				callback(err);
				return;
			}
			result.message = 'ok';
			callback(null);
		});
	}
}

//获取举报的帖子
function getReportData (community_id, callback) {

	var sql = "select a.id 'post_id',a.account_id 'account_id',a.create_date 'create_date',a.title 'title', "+
		"a.content 'content',a.is_reported 'is_reported',a.is_hot 'is_hot',a.is_top 'is_top',b.nickname 'nickname' "+
		"from posts a left join accounts b on a.account_id=b.id where a.is_reported='1' and a.community_id=:community_id ";

	dbHelper.execSql(sql, {community_id: community_id}, function (err, data) {
		if(err){
			callback(err);
			return;
		}
		callback(null, data);
	});
}

//获取搜索的帖子列表
function getSearchData (community_id, key, callback) {

	var sql =  "select a.id 'post_id',a.account_id 'account_id',a.create_date 'create_date',a.title 'title', "+
		"a.content 'content',a.is_reported 'is_reported',a.is_hot 'is_hot',a.is_top 'is_top',b.nickname 'nickname' "+
		"from posts a left join accounts b on a.account_id=b.id where a.community_id=:community_id and (a.title LIKE '%" + key + 
		"%' or a.content like '%"+key+"%' or b.nickname like '%"+key+"%') order by abs(length(a.title)-length(:key)),abs(length(b.nickname)-length(:key)),abs(length(a.content)-length(:key))";

	dbHelper.execSql(sql, {community_id: community_id, key: key}, function (err, data) {
		if(err){
			callback(err);
			return;
		}
		callback(null, data);
	});
}