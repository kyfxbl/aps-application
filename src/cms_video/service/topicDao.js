var sqlHelper                  = require(FRAMEWORKPATH + "/db/sqlHelper");
var dbHelper                   = require(FRAMEWORKPATH + "/utils/dbHelper");
var uuid                       = require('node-uuid');
var accountService             = require('./accountService');

//视频表操作
exports.addTopic               = addTopic;
exports.updateTopic            = updateTopic;
exports.deleteTopic            = deleteTopic;
exports.listTopic              = listTopic;
exports.quertTopicTotal        = quertTopicTotal;
exports.quertTopicDetail       = quertTopicDetail;
exports.listTopicCate          = listTopicCate;
exports.listTopTopicCate       = listTopTopicCate;
exports.addTopicCate           = addTopicCate;
exports.updateTopicCate        = updateTopicCate;
exports.deleteTopicCate        = deleteTopicCate;
exports.updateTopicCatePromote = updateTopicCatePromote;
exports.getPosters             = getPosters;
exports.addPoster              = addPoster;
exports.deletePoster           = deletePoster;
exports.editPoster             = editPoster;
exports.queryTags              = queryTags;
exports.queryTagsLimit         = queryTagsLimit;
exports.addTag                 = addTag;
exports.deleteTag              = deleteTag;
exports.getAuthorAccounts      = getAuthorAccounts;

exports.getDeleteTopicList     = getDeleteTopicList;
exports.deleteTopicData        = deleteTopicData;
exports.getSearchTopicList     = getSearchTopicList;
exports.getNoPushlish          = getNoPushlish;
exports.togglePublish          = togglePublish;

//图片表操作

//文章


function addTopic(topicObject,callback){
    var sqlArray = [];
    async.series([buildTopicSql,topics_has_videos_sql,categories_has_topics_sql,topics_has_tag_sql,topics_pictureAndArticles_sql,topic_and_goods,exec],function(error){
        if(error){
            callback(error);
            return;
        }else{
            callback(null)
        }
    });

    function buildTopicSql(nextStep){
        topicObject.topic.id  = uuid.v1();
        sqlArray.push(sqlHelper.getServerInsertSqlOfObj(null, "topics", topicObject.topic));
        nextStep(null);
    }
    function categories_has_topics_sql(nextStep){
        for(var i=0; i<topicObject.category_ids.length; i++){
            sqlArray.push({
                statement: "insert into categories_has_topics (id,category_id,topic_id,promote,serial_number) values (:id,:category_id,:topic_id,0,:serial_number)",
                value: {
                    id:uuid.v1(),
                    topic_id:topicObject.topic.id ,
                    category_id:topicObject.category_ids[i],
                    serial_number:topicObject.serial_number
                }
            });
        }
        nextStep(null);

    }


    function topics_has_videos_sql(nextStep){
        sqlArray.push({
            statement: "insert into topics_has_videos (id,topic_id,video_id) values (:id,:topic_id,:video_id)",
            value: {
                id:uuid.v1(),
                topic_id:topicObject.topic.id ,
                video_id:topicObject.video_id
            }
        });
        nextStep(null);

    }

    function  topics_has_tag_sql(nextStep){
        _.each(topicObject.tag_ids,function(tagId){
            var article_id = uuid.v1();
            sqlArray.push({
                statement: "insert into topics_has_tags (id,topic_id,tag_id) values (:id,:topic_id,:tag_id)",
                value: {
                    id:uuid.v1(),
                    topic_id:topicObject.topic.id ,
                    tag_id:tagId
                }
            });

        });
        nextStep(null);
    }

    function topics_pictureAndArticles_sql(nextStep){
        _.each(topicObject.pictureAndArticles,function(pictureAndArticle){
            var article_id = uuid.v1();
            sqlArray.push({
                statement: "insert into topics_has_pictures (id,topic_id,picture_id,serial_number) values (:id,:topic_id,:pictures_id,:serial_number)",
                value: {
                    id:uuid.v1(),
                    topic_id:topicObject.topic.id ,
                    pictures_id:pictureAndArticle.pictures_id,
                    serial_number:pictureAndArticle.serial_number
                }
            });
            sqlArray.push({
                statement: "insert into topics_has_articles (id,topic_id,article_id,serial_number) values (:id,:topic_id,:article_id,:serial_number)",
                value: {
                    id:uuid.v1(),
                    topic_id:topicObject.topic.id ,
                    article_id:article_id ,
                    serial_number:pictureAndArticle.serial_number
                }
            });
            sqlArray.push({
                statement: "insert into articles (id,content) values (:id,:content)",
                value: {
                    id:article_id,
                    content:pictureAndArticle.content
                }
            });

        });
        nextStep(null);
    }

    //将主题与商品绑定
    function topic_and_goods (nextStep) {

        _.each(topicObject.goods,function(goods){
            var topic_goods_id = uuid.v1();

            sqlArray.push({
                statement: "insert into topics_has_commodities (id,topic_id,commodity_id,description,serial_number,create_date) values (:id,:topic_id,:commodity_id,:description,:serial_number,:create_date)",
                value: {
                    id:topic_goods_id,
                    topic_id:topicObject.topic.id ,
                    commodity_id:goods.commodity_id,
                    description:goods.description,
                    serial_number:goods.serial_number-1,
                    create_date:(new Date()).getTime()
                }
            });
        });
        nextStep(null);
    }

    function exec(nextStep){
        dbHelper.bacthExecSql(sqlArray, nextStep);
    }

}

//更新主题
function updateTopic(topicObject, callback){


    var sqlArray = [];
    async.series([_updateTopic, _updateCategoryHasTopic, _updateTopicHasTag,
        _updateTopicHasVideos, _updateTopicHasPicturesAndArticles,_updateTopicHasCommodities, _execSql], callback);

    function _updateTopic(nextStep){
        sqlArray.push({
            statement: "update topics set account_id=:account_id,title=:title   ," +
            "status=:status,thumb_pic_url=:thumb_pic_url,big_thumb_pic_url=:big_thumb_pic_url where id=:id",
            value: {
                id: topicObject.topic.id,
                account_id: topicObject.topic.account_id,
                title: topicObject.topic.title,
                status: topicObject.topic.status,
                thumb_pic_url: topicObject.topic.thumb_pic_url,
                big_thumb_pic_url: topicObject.topic.big_thumb_pic_url
            }
        });
        nextStep();
    }
    function _updateCategoryHasTopic(nextStep){
        var sql = "select * from categories_has_topics where topic_id = :id";
        dbHelper.execSql(sql, {id: topicObject.topic.id}, function(err, result){
            if(err){
                return nextStep(err);
            }
            sqlArray.push({
                statement: "delete from categories_has_topics where topic_id=:id",
                value: {
                    id: topicObject.topic.id
                }
            });
            _.each(topicObject.category_ids, function(cateId) {
                var promote = 0;
                for(var i=0; i<result.length; i++){
                    if(result[i].category_id == cateId){
                        promote = result[i].promote;
                        break;
                    }
                }
                sqlArray.push({
                    //statement: "update categories_has_topics set category_id=:category_id,serial_number=:serial_number where topic_id=:id",
                    statement: "insert into categories_has_topics(id,category_id,topic_id,promote,serial_number) values(:id,:category_id,:topic_id,:promote,:serial_number)",
                    value: {
                        id: uuid.v1(),
                        topic_id: topicObject.topic.id,
                        serial_number: topicObject.serial_number,
                        category_id: cateId,
                        promote: promote
                    }
                });
            });
            nextStep();
        });
    }
    function _updateTopicHasTag(nextStep){
        sqlArray.push({
            statement: "delete from topics_has_tags where topic_id=:id",
            value: {
                id: topicObject.topic.id
            }
        });
        _.each(topicObject.tag_ids, function(tagId){
            sqlArray.push({
                statement: "insert into topics_has_tags(id,topic_id,tag_id) values(:id,:topic_id,:tag_id)",
                value: {
                    id: uuid.v1(),
                    topic_id: topicObject.topic.id,
                    tag_id: tagId
                }
            });
        });
        nextStep();
    }
    function _updateTopicHasVideos(nextStep){

        sqlArray.push({
            statement: "update topics_has_videos set video_id=:video_id where topic_id=:topic_id",
            value: {
                topic_id: topicObject.topic.id,
                video_id: topicObject.video_id
            }
        });



        nextStep();
    }
    function _updateTopicHasPicturesAndArticles(nextStep){
        sqlArray.push({
            statement: "delete from topics_has_pictures where topic_id=:id",
            value: {
                id: topicObject.topic.id
            }
        });
        sqlArray.push({
            statement: "delete from topics_has_articles where topic_id=:id",
            value: {
                id: topicObject.topic.id
            }
        });
        _.each(topicObject.pictureAndArticles, function(pictureAndArticle){
            var article_id = uuid.v1();
            sqlArray.push({
                statement: "insert into topics_has_pictures (id,topic_id,picture_id,serial_number) values (:id,:topic_id,:pictures_id,:serial_number)",
                value: {
                    id: uuid.v1(),
                    topic_id: topicObject.topic.id ,
                    pictures_id: pictureAndArticle.pictures_id,
                    serial_number: pictureAndArticle.serial_number
                }
            });
            sqlArray.push({
                statement: "insert into topics_has_articles (id,topic_id,article_id,serial_number) values (:id,:topic_id,:article_id,:serial_number)",
                value: {
                    id: uuid.v1(),
                    topic_id: topicObject.topic.id ,
                    article_id: article_id ,
                    serial_number: pictureAndArticle.serial_number
                }
            });
            sqlArray.push({
                statement: "insert into articles (id,content) values (:id,:content)",
                value: {
                    id:article_id,
                    content:pictureAndArticle.content
                }
            });

        });
        nextStep();
    }

    //先删除topics_has_commodities里面的联系在重新插入联系
    function _updateTopicHasCommodities(nextStep){

        // 删除
        sqlArray.push({
            statement: "delete from topics_has_commodities where topic_id=:id",
            value: {
                id: topicObject.topic.id
            }
        });

        //新插
        _.each(topicObject.goods,function(goods){
            var topic_goods_id = uuid.v1();

            sqlArray.push({
                statement: "insert into topics_has_commodities (id,topic_id,commodity_id,description,serial_number,create_date) values (:id,:topic_id,:commodity_id,:description,:serial_number,:create_date)",
                value: {
                    id:topic_goods_id,
                    topic_id:topicObject.topic.id ,
                    commodity_id:goods.commodity_id,
                    description:goods.description,
                    serial_number:goods.serial_number-1,
                    create_date:(new Date()).getTime()
                }
            });
        });
        nextStep();
    }

    function _execSql(nextStep){
        dbHelper.bacthSeriesExecSql(sqlArray, nextStep);
    }
}

function deleteTopic(id,cateId,callback){
    var sql = "delete from categories_has_topics where  topic_id=:id and category_id = :cateId";
    dbHelper.execSql(sql, {id:id,cateId:cateId}, function (error, result) {
        if (error) {
            callback(error);
            return
        }
        callback(null,result);
    });
}

function listTopic(whereSql,whereVar,callback){
    var sql = "select distinct t.*,b.serial_number 'serialNumber' from topics t left join categories_has_topics b on t.id = b.topic_id where "+ (whereSql?whereSql:" 1=1 ");
    dbHelper.execSql(sql, whereVar, function (error, result) {
        if (error) {
            callback(error);
            return
        }
        callback(null,result);
    });
}

function quertTopicTotal(whereSql,whereVar,callback){
    var sql = "select count(1) as no from topics t where "+(whereSql?whereSql:" 1=1 ");
    dbHelper.execSql(sql, whereVar, function (error, result) {
        if (error) {
            callback(error);
            return
        }
        callback(null,result[0].no);
    });
}

function quertTopicDetail(id,callback){
    //关联查询主题信息、主题关联的图片、关联的视频、关联的文章
    //返回组合后的主题信息
}

function listTopicCate(callback){
    var sql = "select * from topic_categories";
    dbHelper.execSql(sql, {}, function (error, result) {
        if (error) {
            callback(error);
            return
        }
        callback(null,result);
    });
}

function listTopTopicCate(callback){
   var cateMap = {};
   var accountIds = [];

    async.series([queryCates,mergeAuthor],function(error){
        if(error){
            callback(error);
            return;
        }else{
            callback(null, _.values(cateMap))
        }
    });

    function queryCates(nextStep){
        var sql = "select * from topic_categories as a  left JOIN ( select b.category_id, b.topic_id,b.promote,c.thumb_pic_url,c.account_id,c.title,c.create_date from  categories_has_topics as b  INNER JOIN topics as c on b.topic_id=c.id) as d on  a.id=d.category_id and d.promote = 1 order by a.serial_number";
        dbHelper.execSql(sql, {}, function (error, result) {
            if (error) {
                nextStep(error);
                return
            }
            if(result && result.length>0){
                _.each(result,function(one){
                    if(!cateMap[one.id]){
                        cateMap[one.id] = {
                            categoryId:one.id,
                            categoryName:one.name,
                            serial_number:one.serial_number,
                            pic_url:one.pic_url,
                            categoryType: one.type,
                            home_menu_icon: one.home_menu_icon,
                            need_show: one.need_show,
                            topTopics:[

                            ]
                        };
                    }
                    cateMap[one.id].topTopics.push({
                        topicId: one.topic_id,
                        title: one.title,
                        thumbUrl: one.thumb_pic_url,
                        account_id: one.account_id,
                        createDate: one.create_date
                    });
                    accountIds.push(one.account_id);
                });
                nextStep(null);
            }else{
                nextStep(null);
            }
        });
    }

    function mergeAuthor(nextStep){
        accountService.getAccount(accountIds,function(error,authors){
            if(error){
                nextStep(error);
                return;
            }
            _.each(cateMap,function(one,key){
                _.each(one.topTopics,function(topic){
                    topic.author = authors[topic.account_id];
                });
            });
            nextStep(null);
        })
    }
}

function updateTopicCate(topicCate,callback){
    var sql = "update topic_categories set serial_number=:serial_number,name=:name,pic_url=:pic_url,home_menu_icon=:home_menu_icon,need_show=:need_show where id=:id ";

    dbHelper.execSql(sql, topicCate, function (error, result) {
        if (error) {
            callback(error);
            return
        }
        callback(null,result);
    });
}

function deleteTopicCate(id,callback){
    var sql = "delete from topic_categories where  id=:id";
    dbHelper.execSql(sql, {id:id}, function (error, result) {
        if (error) {
            callback(error);
            return
        }
        callback(null,result);
    });
}

function addTopicCate(topicCate,callback){
    var sqlArray = [];
    topicCate.id  = uuid.v1();
    sqlArray.push(sqlHelper.getServerInsertSqlOfObj(null, "topic_categories", topicCate));
    dbHelper.bacthExecSql(sqlArray, function(error){
        callback(null,topicCate);
    });
}

function updateTopicCatePromote(topic_id, category_id, promote,callback){
    var sql = "update categories_has_topics set promote=:promote where category_id=:category_id and topic_id=:topic_id";
    dbHelper.execSql(sql, {category_id:category_id,topic_id:topic_id,promote:promote}, function (error, result) {
        if (error) {
            callback(error);
            return
        }
        callback(null,result);
    });
}

function getPosters(callback){
    var sql = "select a.id,a.pic_url ,a.topic_id ,a.active,a.serial_number,a.create_date,b.title from posters a left join topics b on a.topic_id=b.id where a.active=1 order by a.serial_number";
    dbHelper.execSql(sql, {}, function (error, result) {
        if (error) {
            callback(error);
            return
        }
        callback(null,result);
    });
}

//轮播图
function addPoster(poster,callback){
    var sqlArray = [];
    poster.id = uuid.v1();
    sqlArray.push(sqlHelper.getServerInsertSqlOfObj("", "posters", poster));
    dbHelper.bacthExecSql(sqlArray, callback);
}

function deletePoster(id,callback){
    var sql = "update posters set active=0, end_date=:end_date where id=:id";
    dbHelper.execSql(sql, {end_date: (new Date()).getTime(), id: id}, function (error, result) {
        if (error) {
            callback(error);
            return
        }
        callback(null,result);
    });
}

//编辑轮播图 
function editPoster (poster,callback) {
    var sql = "update posters set serial_number=:serial_number, pic_url=:pic_url where id=:id";

    dbHelper.execSql(sql,poster, function (error, result) {
        if (error) {
            callback(error);
            return
        }
        callback(null,result);
    })
}

function queryTags(callback){
    var sql = "select * from tags";
    dbHelper.execSql(sql, {}, function (error, result) {
        if (error) {
            callback(error);
            return
        }
        callback(null,result);
    });
}


function queryTagsLimit(startIndex, size, callback){
    var sql = "select * from tags limit :startIndex,:size";
    dbHelper.execSql(sql, {startIndex:startIndex,size:size}, function (error, result) {
        if (error) {
            callback(error);
            return
        }
        callback(null,result);
    });
}

//添加标签
function addTag(tag, callback) {
    var sqlArray = [];
    tag.id  = uuid.v1();

    sqlArray.push(sqlHelper.getServerInsertSqlOfObj('', "tags", tag));
    dbHelper.bacthExecSql(sqlArray, callback)
}

//删除标签
function deleteTag(id,callback) {
    var sqlArray = [];
    async.series([deletetopics_has_tags,deletetags],function(error){
        if(error){
            callback(error);
            return;
        }
        callback(null);
    })
    function deletetopics_has_tags(nextStep){
        var sql = "delete from topics_has_tags where  id=:id";
        dbHelper.execSql(sql, {id:id}, function (error, result) {
            if (error) {
                nextStep(error);
                return
            }
            nextStep(null,result);
        });
    }

    function deletetags(nextStep){
        var sql = "delete from tags where  id=:id";
        dbHelper.execSql(sql, {id:id}, function (error, result) {
            if (error) {
                nextStep(error);
                return
            }
            nextStep(null,result);
        });
    }
    function exec(nextStep){
        dbHelper.bacthExecSql(sqlArray, nextStep);
    }
}

function getAuthorAccounts(callback){
    var sql = "select * from accounts where type=5";
    dbHelper.execSql(sql, {}, function (error, result) {
        if (error) {
            callback(error);
            return
        }
        callback(null,result);
    });
}

//获取topic列表（带类别明）
function getDeleteTopicList (page, perPage, callback) {
    var startIndex  = (page-1)*perPage;
    var result      = {
        pageData:'',
        count:0
    };

    async.parallel([getData, getCount], function (err) {
        if(err){
            callback(err);
            return;
        }
        callback(null,result);
    });

    function getData (callback) {
        var sql = "select a.id 'topic_id', a.title, a.create_date, a.is_publish, b.serial_number, c.name from topics a left join categories_has_topics b on a.id=b.topic_id left join topic_categories c on b.category_id=c.id order by b.serial_number limit :startIndex,:perPage"; 

        dbHelper.execSql(sql, {startIndex: startIndex, perPage: parseInt(perPage)}, function (err, data) {
            if(err){
                callback(err);
                return;
            }
            result.pageData = data;
            callback(null);
        });
    }

    function getCount (callback) {
        var sql = "select count(*) as count from topics a left join categories_has_topics b on a.id=b.topic_id left join topic_categories c on b.category_id=c.id"; 

        dbHelper.execSql(sql, {}, function (err, data) {
            if(err){
                callback(err);
                return;
            }
            result.count = data[0].count;
            callback(null);
        });
    }
}

//彻底删除topic
function deleteTopicData (topic_id, callback) {    

    var result = 0;

    async.series([deleteTyepe1, deleteTyepe2, deleteTyepe3, deleteTyepe4, deleteTyepe5, deleteTyepe6, deleteTyepe7, deleteTyepe8], function (err) {
        if(err){
            callback(err);
            return;
        }
        callback(null,result);
    });

    //删除categories_has_topics 类别与主题的联系    
    function deleteTyepe1 (callback) {
        var sql = "delete from categories_has_topics where topic_id=:topic_id";
        dbHelper.execSql(sql, {topic_id: topic_id}, function (err, data) {
            if(err){
                    callback(err);
                    return;
            }
            result = 1;
            callback(null);   
        }); 
    }

    //删除topics_action topic 操作记录    
    function deleteTyepe2 (callback) {
        var sql = "delete from topic_actions where topic_id=:topic_id";
        dbHelper.execSql(sql, {topic_id: topic_id}, function (err, data) {
            if(err){
                    callback(err);
                    return;
            }
            result = 2;
            callback(null);   
        }); 
    }

    //删除topics_has_articles 主题里面的文章
    function deleteTyepe3 (callback) {
        var sql = "delete from topics_has_articles where topic_id=:topic_id";
        dbHelper.execSql(sql, {topic_id: topic_id}, function (err, data) {
            if(err){
                    callback(err);
                    return;
            }
            result = 3;
            callback(null);   
        }); 
    }

    //删除topics_has_pictures 主题里的图片
    
    function deleteTyepe4 (callback) {
        var sql = "delete from topics_has_pictures where topic_id=:topic_id";
        dbHelper.execSql(sql, {topic_id: topic_id}, function (err, data) {
            if(err){
                    callback(err);
                    return;
            }
            result = 4;
            callback(null);   
        }); 
    }

    //删除topics_has_tags 主题里的标签    
    function deleteTyepe5 (callback) {
        var sql = "delete from topics_has_tags where topic_id=:topic_id";
        dbHelper.execSql(sql, {topic_id: topic_id}, function (err, data) {
            if(err){
                    callback(err);
                    return;
            }
            result = 5;
            callback(null);   
        }); 
    }

    //删除topics_has_videos 主题里面的视频   
    function deleteTyepe6 (callback) {
        var sql = "delete from topics_has_videos where topic_id=:topic_id";
        dbHelper.execSql(sql, {topic_id: topic_id}, function (err, data) {
            if(err){
                    callback(err);
                    return;
            }
            result = 6;
            callback(null);   
        }); 
    }

    //删除comments 主题里面的评论    
    function deleteTyepe7 (callback) {
        var sql = "delete from comments where topic_id=:topic_id";
        dbHelper.execSql(sql, {topic_id: topic_id}, function (err, data) {
            if(err){
                    callback(err);
                    return;
            }
            result = 7;
            callback(null);   
        }); 
    }

    //删除topics 主题    
    function deleteTyepe8 (callback) {
        var sql = "delete from topics where id=:topic_id";
        dbHelper.execSql(sql, {topic_id: topic_id}, function (err, data) {
            if(err){
                    callback(err);
                    return;
            }
            result = 8;
            callback(null);   
        }); 
    }
}

//搜索topic列表（带类别明）
function getSearchTopicList (key, page, perPage, callback) {
    var startIndex  = (page-1)*perPage;
    var result      = {
        pageData:'',
        count:0
    };

    async.parallel([getData, getCount], function (err) {
        if(err){
            callback(err);
            return;
        }
        callback(null,result);
    });

    function getData (callback) {
        var sql = "select a.id 'topic_id', a.title, a.create_date, a.is_publish, b.serial_number, c.name from topics a "+
        "left join categories_has_topics b on a.id=b.topic_id left join topic_categories c on b.category_id=c.id where a.title like '%"+key+
        "%' order by b.serial_number limit :startIndex,:perPage"; 

        dbHelper.execSql(sql, {startIndex: startIndex, perPage: parseInt(perPage)}, function (err, data) {
            if(err){
                callback(err);
                return;
            }
            result.pageData = data;
            callback(null);
        });
    }

    function getCount (callback) {
        var sql = "select count(1) as count from topics a left join categories_has_topics b on a.id=b.topic_id left join topic_categories c on b.category_id=c.id where a.title like '%"+key+"%'"; 

        dbHelper.execSql(sql, {}, function (err, data) {
            if(err){
                callback(err);
                return;
            }
            result.count = data[0].count;
            callback(null);
        });
    }
}

//查询所有已经被下架的主题（临时下架）
function getNoPushlish (page, perPage, callback) {
    var startIndex  = (page-1)*perPage;
    var result      = {
        pageData:'',
        count:0
    };

    async.parallel([getData, getCount], function (err) {
        if(err){
            callback(err);
            return;
        }
        callback(null,result);
    });

    function getData (callback) {
         var sql = "select a.id 'topic_id', a.title, a.create_date, a.is_publish, b.serial_number, c.name from topics a "+
        "left join categories_has_topics b on a.id=b.topic_id left join topic_categories c on b.category_id=c.id where a.is_publish=0"+
        " order by b.serial_number limit :startIndex,:perPage"; 
        dbHelper.execSql(sql, {startIndex: startIndex, perPage: parseInt(perPage)}, function (err, data) {
            if(err){
                callback(err);
                return;
            }
            result.pageData = data;
            callback(null);
        });
    }

    function getCount (callback) {
        var sql = "select count(1) as count from topics a left join categories_has_topics b on a.id=b.topic_id left join topic_categories c on b.category_id=c.id where a.is_publish=0"; 

        dbHelper.execSql(sql, {}, function (err, data) {
            if(err){
                callback(err);
                return;
            }
            result.count = data[0].count;
            callback(null);
        });
    }
}

//下架和上架主题
function togglePublish (topic_id, is_publish, callback) {
    var sql = "update topics set is_publish= :is_publish where id= :topic_id";
    dbHelper.execSql(sql, {is_publish: is_publish, topic_id: topic_id}, function (err, data) {
            if(err){
                callback(err);
                return;
            }
            callback(null, data);
    });
}