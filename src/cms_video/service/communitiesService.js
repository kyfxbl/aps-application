// 圈子服务
var logger                  = require(FRAMEWORKPATH + "/utils/logger").getLogger();
var uuid                    = require('node-uuid');

var communitiesDao          = require("./communitiesDao.js");

exports.communitiesList     = communitiesList;			//圈子列表
exports.addcommunityData    = addcommunityData;			//添加圈子
exports.deletecommunityData = deletecommunityData;		//删除圈子
exports.updatecommunityData = updatecommunityData;		//更新圈子
exports.attrList            = attrList;					//获取圈子类别列表
exports.parentList          = parentList;				//获取可以有子圈子的圈子列表
exports.reportList			= reportList;				//获取被举报的圈子列表
exports.searchList 			= searchList;				//获取搜索的圈子列表
exports.communityFans		= communityFans;			//获取所有圈子用户
exports.searchFans          = searchFans;				//获取搜索的圈子内用户
exports.deleteFans			= deleteFans;				//解除圈子与用户的联系
exports.clearFans           = clearFans;				//清除圈子内的用户
exports.getchildList 		= getchildList;				//获取子圈子列表
exports.deletechild			= deletechild;				//删除一个子圈子
exports.clearchild			= clearchild;				//清空子圈子

//获取圈子列表
function communitiesList (req, res, next) {
	
	var page    = req.query.page;
	var perPage = req.query.perPage;

	communitiesDao.getCommunitiesList(page,perPage,function (err,result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, result);
	});
}

//创建圈子
function addcommunityData (req, res, next) {
	var data = {
		creator_id: req.body.creator_id,
		id: uuid.v1(),
		name: req.body.name,
		pic_url: req.body.pic_url,
		attr: req.body.attr,
		create_date: (new Date()).getTime(),
		is_official: req.body.is_official,
		description: req.body.description,
		parent_id: req.body.parent_id,
		is_container: req.body.is_container,
		status: req.body.status
	}

	communitiesDao.addCommunity(data, function (err, result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, result);
	});
}

//删除圈子
function deletecommunityData (req, res, next) {
	var id = req.body.community_id;

	communitiesDao.deleteCommunity(id, function (err, result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		if(result.type != 0){
			doResponse(req, res, result);
			return;
		}
		doResponse(req, res, result);
	});
}
  
//更新圈子
function updatecommunityData (req, res, next) {
  	var id   = req.body.community_id;
  	var data = {
  		create_date: (new Date()).getTime(),
  		creator_id: req.body.creator_id,
  		old_creator: req.body.old_creator,
  		name: req.body.name,
		pic_url: req.body.pic_url,
		attr: req.body.attr,
		description: req.body.description,
		parent_id: req.body.parent_id,
		is_container: req.body.is_container,
		status: req.body.status,
		is_official: req.body.is_official
  	};
  	
  	communitiesDao.updateCommunity(id, data, function (err, result) {
  		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, result);
  	});
}  

// 获取圈子类别列表
function attrList (req, res, next) {
	communitiesDao.getAttrList(function (err, result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, result);
	});
}

//获取可以有子圈子的圈子列表
function parentList (req, res, next) {
	communitiesDao.getParentList(function (err, result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, result);
	});
}

//获取被举报的圈子列表
function reportList(req, res, next) {
	communitiesDao.getreportList(function (err, result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, result);
	});
}

//获取搜索的圈子列表
function searchList(req, res, next) {
	var key = req.query.key;
	
	communitiesDao.getsearchList(key ,function (err, result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, result);
	});
}

//获取圈子用户列表
function communityFans (req, res, next) {
	var page         = req.query.page;
	var perPage      = req.query.perPage;
	var community_id = req.query.community_id;
	communitiesDao.getallfans(page, perPage, community_id, function (err, result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, result);
	});
}

//获取搜索的圈子内用户
function searchFans (req, res, next) {
	var page         = req.query.page;
	var perPage      = req.query.perPage;
	var community_id = req.query.community_id;
	var key 		 = req.query.key;

	communitiesDao.getsearchfans(page, perPage, community_id, key, function (err, result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, result);
	});
}

//解除圈子与用户的联系
function deleteFans (req, res, next) {
	var account_id   = req.body.account_id;
	var community_id = req.body.community_id;

	communitiesDao.deletefans(account_id, community_id, function (err, result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, result);
	});
}

//清除圈子内的用户
function clearFans (req, res, next) {
	var account_id   = req.body.account_id;			//圈子作者id
	var community_id = req.body.community_id;

	communitiesDao.deleteallfans(account_id, community_id, function (err, result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, result);
	});
}

//获取子圈子列表
function getchildList (req, res, next) {
	var page    = req.query.page;
	var perPage = req.query.perPage;
	var community_id = req.query.community_id;

	communitiesDao.getchildData(community_id, page, perPage, function (err, result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, result);
	});
}

// 删除一个子圈子
function deletechild (req, res, next) {
	var community_id = req.body.community_id;

	communitiesDao.deletechildData(community_id, function (err, result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, {message:'ok'});
	});
}

// 清空子圈子
function clearchild (req, res, next) {
	var parent_id = req.body.parent_id;
	communitiesDao.clearchildData(parent_id, function (err, result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, {message:'ok'});
	});
}