var logger = require(FRAMEWORKPATH + "/utils/logger").getLogger();
var redis = require('redis');

exports.set = set;
exports.hset = hset;
exports.hmset = hmset;
exports.hgetall = hgetall;
exports.flushdb = flushdb;
exports.del = del;
exports.zadd = zadd;
exports.zrevrange = zrevrange;
exports.zrem = zrem;

var redis_ip = "10.161.228.193";
var redis_port = 6379;

function getClient(database, callback){

    // var client = redis.createClient(6379, '192.168.1.121');
    var client = redis.createClient(redis_port, redis_ip);
    client.on("error", function(err){
        logger.error(err);
    });
    if(typeof database != "number"){
        database = 0;
    }
    client.select(database, function(){
        callback(client);
    });
}

function set(database, key, value, callback){
    getClient(database, function(client){
        client.set(key, value, function(err){
            callback(err);
            client.quit();
        });
    });
}

function hset(database, hashkey, key, value, callback){
    getClient(database, function(client){
        client.hset(hashkey, key, value, function(err){
            callback(err);
            client.quit();
        });
    });
}

function hmset(database, hashkey, obj, callback){
    getClient(database, function(client){
        client.hmset(hashkey, obj, function(err){
            callback(err);
            client.quit();
        });
    });
}

function hgetall(database, hashkey, callback){
    getClient(database, function(client){
        client.hgetall(hashkey, function(err, obj){
            callback(err, obj);
            client.quit();
        });
    });
}

function flushdb(database, callback){
    getClient(database, function(client){
        client.flushdb(function(err, obj){
            callback(err, obj);
            client.quit();
        });
    });
}

function del(database, key, callback){
    getClient(database, function(client){
        client.del(key, function(err, obj){
            callback(err, obj);
            client.quit();
        });
    });
}

function zadd(database, key, score, member, callback){
    getClient(database, function(client){
        client.zadd(key, score, member, function(err, obj){
            callback(err, obj);
            client.quit();
        });
    });
}

function zrevrange(database, key, start, stop, callback){
    getClient(database, function(client){
        client.zrevrange(key, start, stop, function(err, obj){
            callback(err, obj);
            client.quit();
        });
    });
}

function zrem(database, key, member, callback){
    getClient(database, function(client){
        client.zrem(key, member, function(err, obj){
            callback(err, obj);
            client.quit();
        });
    });
}
