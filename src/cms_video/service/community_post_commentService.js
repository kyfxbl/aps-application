var logger                    = require(FRAMEWORKPATH + "/utils/logger").getLogger();
var uuid                      = require('node-uuid');

var community_post_commentDao = require("./community_post_commentDao.js");

exports.commentList           = commentList;			//获取可以帖子的评论列表
exports.searchList            = searchList;				//搜索帖子中的评论
exports.deleteComment         = deleteComment;			//删除帖子中的评论

// 获取帖子评论列表
function commentList (req, res, next) {

	var post_id = req.query.post_id;
	var page    = req.query.page;
	var perPage = req.query.perPage;

	community_post_commentDao.getCommentsList(post_id, page, perPage, function (err, result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, result);
	});
}

//搜索帖子评论列表
function searchList (req, res, next) {
	var post_id = req.query.post_id;
	var key     = req.query.key;

	community_post_commentDao.searchCommentLIst(post_id, key, function (err, result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, result);
	});
}

//删除评论
function deleteComment (req, res, next) {
	
	var comment_id = req.body.comment_id;

	community_post_commentDao.deletecommentData(comment_id, function (err, result) {
		if(err){
			logger.error(err);
			next(err);
			return;
		}
		doResponse(req, res, {message:'ok'});
	});
}