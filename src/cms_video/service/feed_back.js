var dbHelper = require(FRAMEWORKPATH + "/utils/dbHelper");

exports.feedBackMessage = feedBackMessage;
exports.delFeedBackMsg = delFeedBackMsg;

function feedBackMessage(req, res, next){
    var page = req.query["page"] || 1;
    var perPage = parseInt(req.query["perPage"]) || 20;
    var startIndex = (page - 1) * perPage;
    var results = {result: '',count:''};
    var sql1 = "select count(*) as count from feedbacks";
    var sql2 = "select a.id 'id',a.content 'content',a.create_date 'create_date',b.username 'username',b.nickname 'nickname' " +
        "from feedbacks a left join accounts b on a.account_id = b.id " +
        "order by a.create_date desc limit :startIndex,:perPage";
    dbHelper.execSql(sql2, {startIndex: startIndex,perPage: perPage}, function(error, data1){
       if(error){
           console.log(error);
           return next(error);
       }
        results.result = data1;
        dbHelper.execSql(sql1, {}, function(err, data2){
            if(err){
                return next(err);
            }
            results.count = data2;
            doResponse(req, res, results);
        });
    });
}

function delFeedBackMsg(req, res, next){
    var id = req.query["id"];
    var sql = "delete from feedbacks where id = :id";
    dbHelper.execSql(sql, {id: id}, function(error, data){
       if(error){
           console.log(error);
           return next(error);
       }
        doResponse(req, res, "ok");
    });
}