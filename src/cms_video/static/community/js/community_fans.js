app.controller('communityfansCtrl', ['$rootScope', '$scope', '$http', '$location', '$stateParams', '$state', function ($rootScope, $scope, $http,$location, $stateParams, $state){

	$scope.fansData = {
		pageData:'',
		deleteData:'',
		clearData:'',
		key:'',
		search:false
	}

	$scope.filterOptions = {
        filterText: "",
        useExternalFilter: true  
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [20, 50, 100],
        pageSize: 20,
        currentPage: 1
    };

	//返回圈子列表
    $scope.goBackCommunities = function(){
        $state.go('app.communities', {

        });
    }

    //创建表单
    $scope.setPagingData = function(data){
        $scope.myData = data;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };

    //获取帖子列表数据
    $scope.getPagedDataAsync = function (page, prePage) {
    	$scope.fansData.search = false;
        setTimeout(function () {
            var url = '/cms/communities/communityFans?community_id='+$stateParams.community_id+'&perPage='+ prePage + '&page=' + page;
            $http.get(url).success(function (data) {
                $scope.setPagingData(data.result.pageData);
                $scope.totalServerItems = data.result.count;
            });
        }, 100);
    };

    //待创建表单
    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && (newVal.currentPage !== oldVal.currentPage || newVal.pageSize !== oldVal.pageSize)) {
        	if($scope.fansData.search){
        		$scope.getsearchfans($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize);
        		return;
        	}
            $scope.getPagedDataAsync($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
        	if($scope.fansData.search){
        		$scope.getsearchfans($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize);
        		return;
        	}
            $scope.getPagedDataAsync($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize);
        }
    }, true);

    $scope.columnDefs = [
        {field: 'username', displayName: '用户名', cellTemplate: '<span style="margin-left:10px">{{row.entity.username}}</span>'},
        {field: 'nickname', displayName: '昵称', cellTemplate: '<span>{{row.entity.nickname?row.entity.nickname:"没有昵称"}}</span>'},
        {field: 'photo_url', displayName: '头像',cellTemplate: '<div><img style="height:80px;" src="{{row.entity.photo_url}}" alt="" /></div>'},
        {field: 'type', displayName: '身份',cellTemplate: '<span>{{row.entity.type}}</span>'},
        {field: '', displayName: '操作', cellTemplate: '<span ng-show="row.entity.id != $stateParams.creator_id" class="label bg-danger" data-toggle="modal" data-target=".deletefans" ng-click="deletefans(row.entity)">删除用户</span><span ng-show="row.entity.id == $stateParams.creator_id" class="label bg-danger">圈主</span>'}
    ];
    $scope.gridOptions = {
        data: 'myData',
        columnDefs: $scope.columnDefs,
        enablePaging: true,
        showFooter: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        multiSelect: false,
        rowHeight:80,
        i18n: 'zh_cn'
    };
    window.ngGrid.i18n['zh_cn'] = yilos_i18n.resource;
    $scope.getPagedDataAsync(1, 20);

    //搜索圈子用户
    $scope.getsearchfans = function (page, prePage) {
        if(!$scope.fansData.key){
            alert('搜索关键词不能为空');
            return;
        }
        $scope.fansData.search = true;
        var url = '/cms/communities/searchFans?community_id='+$stateParams.community_id+'&key='+$scope.fansData.key+'&perPage='+ prePage + '&page=' + page;
        $http.get(url).success(function (data) {
           $scope.setPagingData(data.result.pageData);
           $scope.totalServerItems = data.result.count;
        });
    }

    //获取圈子所有用户
    $scope.getallfans = function () {
    	$scope.pagingOptions.currentPage = 1;
    	$scope.fansData.search = false;
    	$scope.getPagedDataAsync(1, 20);
    }

    //删除圈子成员
    $scope.deletefans = function (data) {
    	$scope.fansData.deleteData = data;
    }

     //删除圈子成员确认
    $scope.deletefansOk = function () {
    	$http.post("/cms/communities/deleteFans", {account_id: $scope.fansData.deleteData.id, community_id: $stateParams.community_id}).success(function (data) {
            $scope.pagingOptions.currentPage = 1;
            $scope.fansData.search = false;
            $scope.getPagedDataAsync(1, 20);
    		$(".deletefans").modal('hide');
    	});
    }

    //清除圈子成员
    $scope.clearfansOk = function () {
    	$http.post("/cms/communities/clearFans", {account_id: $stateParams.creator_id, community_id: $stateParams.community_id}).success(function (data) {
            $scope.pagingOptions.currentPage = 1;
            $scope.fansData.search = false;
            $scope.getPagedDataAsync(1, 20);
    		$(".clearfans").modal('hide');
    	});
    }
}]);