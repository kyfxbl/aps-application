app.controller('postcommentCtrl', ['$rootScope', '$scope', '$http', '$location', '$stateParams', '$state', function ($rootScope, $scope, $http,$location, $stateParams, $state){

    $scope.modalData = {
        comments:'',
        show:{
            community_id:'',
            title:'',
            delete:'',
            key:''
        }
    };

	$scope.filterOptions = {
        filterText: "",
        useExternalFilter: true  
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [20, 50, 100],
        pageSize: 20,
        currentPage: 1
    };

    //返回圈子列表
    $scope.goBackpost = function(){
        $state.go('app.community_post', {
            community_id: $scope.modalData.show.community_id
        });
    }

    //创建表单
    $scope.setPagingData = function(data){
        $scope.myData = data;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };

    //获取评论列表数据
    $scope.getPagedDataAsync = function (page, prePage) {
        setTimeout(function () {
            var url = '/cms/community_post_comment/commentList?post_id='+$stateParams.post_id +'&perPage='+ prePage + '&page=' + page;
            $http.get(url).success(function (data) {
                $scope.setPagingData(data.result.pageData, page, prePage);
                $scope.totalServerItems            = data.result.count;
                $scope.modalData.show.title        = data.result.post_title;
                $scope.modalData.show.community_id = data.result.community_id;
            });
        }, 100);
    };

    //待创建表单
    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && (newVal.currentPage !== oldVal.currentPage || newVal.pageSize !== oldVal.pageSize)) {
            $scope.getPagedDataAsync($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize, $scope.filterOptions.filterText);
        }
    }, true);

    $scope.columnDefs = [
        {field: 'nickname', displayName: '作者', cellTemplate: '<span style="margin-left:10px">{{row.entity.nickname}}</span>'},
        {field: 'content', displayName: '内容', cellTemplate: '<span>{{row.entity.content?row.entity.content:"没有内容"}}</span>'},
        {field: 'at_nickname', displayName: '@作者',cellTemplate: '<div>{{row.entity.at_nickname}}</div>'},
        {field: 'create_data', displayName: '日期',cellTemplate: '<span>{{row.entity.create_data |date:"yyyy-MM-dd hh:mm"}}</span>'},
        {field: '', displayName: '操作', cellTemplate: '<span class="label bg-danger" data-toggle="modal" data-target=".deleteComment" ng-click="deleteComment(row.entity.comment_id)">删除</span>'}
    ];
    $scope.gridOptions = {
        data: 'myData',
        columnDefs: $scope.columnDefs,
        enablePaging: true,
        showFooter: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        multiSelect: false,
        rowHeight:80,
        i18n: 'zh_cn'
    };
    window.ngGrid.i18n['zh_cn'] = yilos_i18n.resource;

    $scope.getPagedDataAsync(1, 20);

    //查看所有评论
    $scope.getAllData = function () {
        $scope.getPagedDataAsync(1, 20);
        $scope.pagingOptions.currentPage = 1;
    }

    //搜索评论
    $scope.getsearchdata = function () {
        if(!$scope.modalData.show.key){
            alert('搜索关键词不能为空');
            return;
        }
        var url = '/cms/community_post_comment/searchList?post_id='+$stateParams.post_id +'&key='+$scope.modalData.show.key;
        $http.get(url).success(function (data) {
            $scope.setPagingData(data.result, 1, 1);
            $scope.totalServerItems = data.result.length;
        });
    }

    //删除评论
    $scope.deleteComment = function (comment_id) {
       $scope.modalData.show.delete = comment_id;
    }

    //确认删除

    $scope.deleteBtnOk = function () {
        var url = '/cms/community_post_comment/deleteComment';
        $http.post(url, {comment_id: $scope.modalData.show.delete}).success(function (data) {
            
            $(".deleteComment").modal('hide');
            $scope.getPagedDataAsync(1, 20);
            $scope.pagingOptions.currentPage = 1;
        });
    }
}]);