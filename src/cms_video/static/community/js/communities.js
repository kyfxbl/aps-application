app.controller('communitiesCtrl', ['$rootScope', '$scope', '$http','$location','$state', function ($rootScope, $scope, $http,$location, $state){

    $scope.modalData = {
        addData: {
            show: {
                key: '',
                is_container: false,
                tips: '不允许创建子圈子',
                authorlist: '',
                attrlist: '',
                parentlist: '',
                parentlistshow: true,
                statusshow:false,
                status:'未推荐',
                officialshow:false,
                official:'非官方圈子'
            },
            data: {
                creator_id:'',
                nickname:'',
                name:'',
                pic_url: '',
                attr: '',
                description: '',
                parent_id: null,       //有允许有子圈子时，不允许有父圈子，不允许有子圈子时，可以有父圈子
                is_container: 0,       //1允许有子圈子，0不允许有子圈子 
                status:0,              //0不推荐圈子，1推荐圈子
                is_official:0,         //0非官方圈子，1官方圈子
            }
        },
        deleteData: '',
        editData: {
            show: {
                is_container: true,
                tips: '允许创建子圈子',
                parentlistshow: false,
                statusshow:false,
                status:'未推荐',
                officialshow:false,
                official:'非官方圈子'
            },
            data: {
                nickname:'',
                old_creator:'',
                community_id: '',
                creator_id:'',
                name: '',
                pic_url: '',
                attr: '',
                description: '',
                parent_id: null,       //有允许有子圈子时，不允许有父圈子，不允许有子圈子时，可以有父圈子
                is_container: 1,      //1允许有子圈子，0不允许有子圈子 
                status:0,              //0不推荐圈子，1推荐圈子
                is_official:0,         //0非官方圈子，1官方圈子 
            }
        }
    };

	$scope.filterOptions = {
        filterText: "",
        useExternalFilter: true  
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [20, 50, 100],
        pageSize: 20,
        currentPage: 1
    };

    //添加圈子选择搜索作者的数据
    $scope.addselectedAuthor = function (data) {
        if(data){
            $scope.modalData.addData.data.creator_id = data.originalObject.id;
            return
        }
        //重置表格数据
        
    }

    //搜索作者传参数
    $scope.remoteUrlRequestFn = function(str) {
        return {key: str};
    };

    //编辑圈子选择搜索作者的数据
    $scope.editselectedAuthor = function (data) {
        if(data){
            $scope.modalData.editData.data.creator_id = data.originalObject.id;
            return
        }
        //重置表格数据
    }

    //创建表单
    $scope.setPagingData = function(data){
        $scope.myData = data;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };

    //获取圈子列表数据
    $scope.getPagedDataAsync = function (page, prePage) {
        setTimeout(function () {
            var url = '/cms/communities/communitiesList?perPage=' + prePage + "&page=" + page;
            $http.get(url).success(function (data) {
                $scope.setPagingData(data.result.pageData, page, prePage);
                $scope.totalServerItems = data.result.count;
            });
        }, 100);
    };

    //待创建表单
    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && (newVal.currentPage !== oldVal.currentPage || newVal.pageSize !== oldVal.pageSize)) {
            $scope.getPagedDataAsync($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize, $scope.filterOptions.filterText);
        }
    }, true);

    var operation = '<div >&nbsp;&nbsp;&nbsp;<span class="label bg-success" data-toggle="modal" data-target=".editData" ng-click="editCommunity(row.entity)">编辑</span>';
    operation += ' <span class="label bg-danger" data-toggle="modal" data-target=".deletData" ng-click="deleteCommunity(row.entity)">删除</span>';
    operation += ' <span class="label bg-info" data-toggle="modal" data-target=".delete-user" ng-click="openCommunity(row.entity)">查看</span>';
    operation += ' <span class="label bg-success" data-toggle="modal" data-target=".delete-user" ng-click="openFans(row.entity)">成员</span></div>';

    $scope.columnDefs = [
        {filed: 'pic_url', displayName: '缩略图', cellTemplate: '<div><img style="height:80px;" src="{{row.entity.pic_url}}" alt="" /></div>'},
        {field: 'name', displayName: '名称', cellTemplate: '<span style="margin-left:10px">{{row.entity.name}}</span>'},
        {field: 'is_container', displayName: '类型', cellTemplate: '<span ng-show="row.entity.is_container == 1">父圈子</span><span ng-show="row.entity.is_container != 1">子圈子</span>'},
        {field: 'nickname', displayName: '作者',cellTemplate: '<div>{{row.entity.nickname?row.entity.nickname:"官方"}}</div>'},
        {field: 'create_date', displayName: '日期',cellTemplate: '<span>{{row.entity.create_date |date:"yyyy-MM-dd hh:mm"}}</span>'},
        {field: '', displayName: '操作', cellTemplate: operation}
    ];
    $scope.gridOptions = {
        data: 'myData',
        columnDefs: $scope.columnDefs,
        enablePaging: true,
        showFooter: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        multiSelect: false,
        rowHeight:80,
        i18n: 'zh_cn'
    };
    window.ngGrid.i18n['zh_cn'] = yilos_i18n.resource;

    $scope.getPagedDataAsync(1, 20);

    //点击删除按钮为模态框赋值
    $scope.deleteCommunity = function (data) {
        $scope.modalData.deleteData = data;
    };

    //删除模态框确认操作
    $scope.modalDelete = function () {
        if ( !$scope.modalData.deleteData) {
            $(".deletData").modal('hide');
            return;
        }
        $http.post("/cms/communities/deletecommunityData", {community_id:$scope.modalData.deleteData.community_id}).success(function(data){
            if(data.result.type == 1){
                alert('圈子下有子圈子，不允许删除！');
            }
            if(data.result.type == 2){
                alert('圈子下有帖子，不允许删除！');
            }
            $scope.getPagedDataAsync(1, 20);
            $scope.pagingOptions.currentPage = 1;
            $(".deletData").modal('hide');
        });
    };

    //点击新建按钮初始化圈子数据模型
    $scope.addNewData = function () {
        var newData = {
            creator_id:'',
            nickname:'',
            name:'',
            pic_url:'',
            attr:'',
            description:'',
            parent_id:'',       
            is_container:0,
            status:0,
            is_official:0     
        };
        $scope.modalData.addData.data                = newData;
        $scope.modalData.addData.show.parentlistshow = true;
        $scope.modalData.addData.show.is_container   = false;
        $scope.modalData.addData.show.tips           = "不允许创建子圈子";
        $scope.modalData.addData.show.statusshow     = false;
        $scope.modalData.addData.show.status         = "未推荐";
        $scope.modalData.addData.show.officialshow   = false;
        $scope.modalData.addData.show.official       = '非官方圈子';
        $scope.modalData.addData.show.creator        = 'null';

        setTimeout(function () {
            $("#uplpadimg").hide();
        },100)
    };

    //获取圈子类别列表
    $scope.getAttrList = function () {
        var url = '/cms/communities/attrList';
        $http.get(url).success(function (data) {
            $scope.modalData.addData.show.attrlist = data.result;
        });
    };
    $scope.getAttrList();

    //选择圈子类别
    $scope.chooseattr = function (data) {
        $scope.modalData.addData.data.attr = data;
    };

    //是否允许拥有子圈子，如果不允许则显示允许拥有子圈子的圈子列表
    $scope.showParentList = function () {
        $scope.modalData.addData.show.is_container = !$scope.modalData.addData.show.is_container;
        if ($scope.modalData.addData.show.is_container) {
            $scope.modalData.addData.show.tips = "允许创建子圈子";
            $scope.modalData.addData.show.parentlistshow = false;
            $scope.modalData.addData.data.is_container = 1;
            $scope.modalData.addData.data.parent_id = null;
            return;
        }
        $scope.modalData.addData.show.tips = "不允许创建子圈子";
        $scope.modalData.addData.show.parentlistshow = true;
        $scope.modalData.addData.data.is_container = 0;
    };

    //是否推荐圈子，如果推荐则status为1
    $scope.showstatus = function () {
        $scope.modalData.addData.show.statusshow = !$scope.modalData.addData.show.statusshow;
        if ($scope.modalData.addData.show.statusshow) {
            $scope.modalData.addData.show.status = "已推荐";
            $scope.modalData.addData.data.status = 1;
            return;
        }
        $scope.modalData.addData.show.status = "未推荐";
        $scope.modalData.addData.data.status = 0;
    };

    //是否是官方圈子，如果是则is_official为1
    $scope.showofficial = function () {
        $scope.modalData.addData.show.officialshow = !$scope.modalData.addData.show.officialshow;
        if ($scope.modalData.addData.show.officialshow) {
            $scope.modalData.addData.show.official = "官方圈子";
            $scope.modalData.addData.data.is_official = 1;
            return;
        }
        $scope.modalData.addData.show.official = "非官方圈子";
        $scope.modalData.addData.data.is_official = 0;
    };

    //获取可以创建拥有子圈子的圈子列表
    $scope.getParentList = function () {
        var url = '/cms/communities/parentList';
        $http.get(url).success(function (data) {
            $scope.modalData.addData.show.parentlist = data.result;
        });
    };
    $scope.getParentList();

    //选择父级圈子
    $scope.chooseparent = function () {
        if(!$scope.modalData.addData.data.parent_id){
            $scope.modalData.addData.data.parent_id = null;
            return;
        }
    };

    //确认添加新的圈子
    $scope.addCommunity  = function () {
        var url          = '/cms/communities/addcommunityData';
        var creator_id   = $scope.modalData.addData.data.creator_id;
        var name         = $scope.modalData.addData.data.name;
        var pic_url      = $scope.modalData.addData.data.pic_url;
        var attr         = $scope.modalData.addData.data.attr;
        var description  = $scope.modalData.addData.data.description;
        var parent_id    = $scope.modalData.addData.data.parent_id;
        var is_container = $scope.modalData.addData.data.is_container;
        var status       = $scope.modalData.addData.data.status;
        var is_official  = $scope.modalData.addData.data.is_official;

        if( !creator_id){
            alert("请在右侧作者列表中选择作者");
            return;
        }

        if(name && pic_url && attr && description){
            $http.post(url, {creator_id: creator_id, name: name, pic_url: pic_url, attr: attr, description: description, parent_id: parent_id, is_container: is_container, status: status, is_official: is_official}).success(function (data) {
                $scope.getPagedDataAsync(1, 20);
                $scope.pagingOptions.currentPage = 1;
                $scope.getAttrList();
                $scope.getParentList();
                $(".addData").modal('hide');
            })
            return;
        }
        alert("请将信息填写完整");
    };

    //点击编辑按钮为模态框赋值
    $scope.editCommunity = function (data) {

        $scope.modalData.editData.data.old_creator  = data.creator_id;
        $scope.modalData.editData.data.nickname     = data.nickname;
        $scope.modalData.editData.data.creator_id   = data.creator_id;
        $scope.modalData.editData.data.community_id = data.community_id;
        $scope.modalData.editData.data.name         = data.name;
        $scope.modalData.editData.data.pic_url      = data.pic_url;
        $scope.modalData.editData.data.attr         = data.attr;
        $scope.modalData.editData.data.description  = data.description;
        $scope.modalData.editData.data.parent_id    = data.parent_id;
        $scope.modalData.editData.data.is_container = data.is_container;
        $scope.modalData.editData.data.status       = data.status;
        $scope.modalData.editData.data.is_official  = data.is_official;

        var newshowdata = {
            is_container: true,
            tips: '允许创建子圈子',
            parentlistshow: false,
            statusshow:false,
            status:'未推荐',
            officialshow:false,
            official:'非官方圈子',
            creator:'null'

        };
        $scope.modalData.editData.show = newshowdata;

        if(data.is_container == 0){
            $scope.modalData.editData.show.tips           = '不允许创建子圈子';
            $scope.modalData.editData.show.parentlistshow = true;
            $scope.modalData.editData.show.is_container   = false;
        }
        if(data.status == 1){
            $scope.modalData.editData.show.status = '已推荐';
            $scope.modalData.editData.show.statusshow = true;
        }
        if(data.is_official == 1){
            $scope.modalData.editData.show.official = '官方圈子';
            $scope.modalData.editData.show.officialshow = true;
        }

        setTimeout(function () {
            $("#uplpadimg").hide();
        },100);
    };

    //编辑模态框选择类别
    $scope.editchooseattr = function (data) {
        $scope.modalData.editData.data.attr = data;
    };

    //编辑模式下是否允许拥有子圈子，如果不允许则显示允许拥有子圈子的圈子列表
    $scope.editshowParentList = function () {
        $scope.modalData.editData.show.is_container = !$scope.modalData.editData.show.is_container;

        if ($scope.modalData.editData.show.is_container) {
            $scope.modalData.editData.show.tips = "允许创建子圈子";
            $scope.modalData.editData.show.parentlistshow = false;
            $scope.modalData.editData.data.is_container = 1;
            return;
        }

        $scope.modalData.editData.show.tips = "不允许创建子圈子";
        $scope.modalData.editData.show.parentlistshow = true;
        $scope.modalData.editData.data.is_container = 0;
    };

    //编辑模式下选择父级圈子
    $scope.editchooseparent = function () {
        if(!$scope.modalData.editData.data.parent_id){
            $scope.modalData.editData.data.parent_id = null;
        }else if($scope.modalData.editData.data.parent_id == $scope.modalData.editData.data.community_id){
            alert("父级圈子不能选择自己");
            $scope.modalData.editData.data.parent_id = null;
        }
    };

    //编辑模式下是否推荐圈子
    $scope.editshowstatus = function () {
        $scope.modalData.editData.show.statusshow = !$scope.modalData.editData.show.statusshow;

        if ($scope.modalData.editData.show.statusshow) {
            $scope.modalData.editData.show.status = "已推荐";
            $scope.modalData.editData.data.status = 1;
            return;
        }

        $scope.modalData.editData.show.status = "未推荐";
        $scope.modalData.editData.data.status = 0;
    };

    //编辑模式下是否是官方圈子
    $scope.editshowofficial = function () {
        $scope.modalData.editData.show.officialshow = !$scope.modalData.editData.show.officialshow;

        if ($scope.modalData.editData.show.officialshow) {
            $scope.modalData.editData.show.official = "官方圈子";
            $scope.modalData.editData.data.is_official = 1;
            return;
        }

        $scope.modalData.editData.show.official = "非官方圈子";
        $scope.modalData.editData.data.is_official = 0;
    };

    //确认编辑完成
    $scope.editCommunityDone = function () {
        var url          = '/cms/communities/updatecommunityData';
        var creator_id   = $scope.modalData.editData.data.creator_id;
        var community_id = $scope.modalData.editData.data.community_id;
        var name         = $scope.modalData.editData.data.name;
        var pic_url      = $scope.modalData.editData.data.pic_url;
        var attr         = $scope.modalData.editData.data.attr;
        var description  = $scope.modalData.editData.data.description;
        var is_container = $scope.modalData.editData.data.is_container;
        var old_creator  = $scope.modalData.editData.data.old_creator;
        var is_official  = $scope.modalData.editData.data.is_official;
        var status       = $scope.modalData.editData.data.status;

        if(is_container == 0){
            var parent_id    = $scope.modalData.editData.data.parent_id;
        }else{
            $scope.modalData.editData.data.parent_id = null;
        }

        if(name && pic_url && attr && description){
            $http.post(url, {community_id: community_id, name: name, pic_url: pic_url, attr: attr, description: description, parent_id: parent_id, is_container: is_container, creator_id: creator_id, is_official: is_official, status: status, old_creator: old_creator}).success(function (data) {
                $scope.getPagedDataAsync(1, 20);
                $scope.pagingOptions.currentPage = 1;
                $scope.getAttrList();
                $scope.getParentList();
                if(data.result.post && is_container == 1){
                    alert("设置父级圈子，请先清空此圈子下的帖子");
                }
                $(".editData").modal('hide');
            })
            return;
        }
        alert("请将信息填写完整");
    }

    //新建圈子上传图片
    $scope.addinitUploadWidget = function(){
        $('#commouity_pic_fileupload').fileupload({
            url: '/cms/picture/upload',
            prependFiles:"html",
            getFilesFromResponse:getThumb_pic_urlFromResponse,
            acceptFileTypes: /(\.|\/)(jpg|png)$/i,
            formData:function () {
                return [];
            }
        }).bind('fileuploadfinished',function(e,data){
                $(".uplpadimg").hide();
            })
            .addClass('fileupload-processing');

        function getThumb_pic_urlFromResponse(data){
            $scope.modalData.addData.data.pic_url  = data.result.result.files[0].url;
            $scope.digestScope();
            return data.result.result.files;
        }       
    };
    $scope.addinitUploadWidget();

    //新建圈子删除图片
    $scope.deleteaddimg = function () {
        $scope.modalData.addData.data.pic_url = '';
    }
 
    // 编辑圈子上传图片
    $scope.editinitUploadWidget = function(){
        $('#edit_pic_fileupload').fileupload({
            url: '/cms/picture/upload',
            prependFiles:"html",
            getFilesFromResponse:getThumb_pic_urlFromResponse,
            acceptFileTypes: /(\.|\/)(jpg|png)$/i,
            formData:function () {
                return [];
            }
        }).bind('fileuploadfinished',function(e,data){
                $(".uplpadimg").hide();
            })
            .addClass('fileupload-processing');

        function getThumb_pic_urlFromResponse(data){
            $scope.modalData.editData.data.pic_url  = data.result.result.files[0].url;
            $scope.digestScope();
            return data.result.result.files;
        }       
    };

    //编辑圈子删除图片
    $scope.editaddimg = function () {
        $scope.modalData.editData.data.pic_url = '';
    }

    //查看被举报的圈子
    $scope.getreported = function () {
        var url = '/cms/communities/reportList';
        $http.get(url).success(function (data) {
            $scope.setPagingData(data.result, 1, 1);
            $scope.totalServerItems = data.result.length;
        });
    }

    //查看所有圈子
    $scope.getAllData = function () {
        $scope.getPagedDataAsync(1, 20);
        $scope.pagingOptions.currentPage = 1;
    }

    //搜索圈子
    $scope.getsearchdata = function () {
        if(!$scope.modalData.addData.show.key){
            alert('搜索关键词不能为空');
            return;
        }
        var url = '/cms/communities/searchList?key='+$scope.modalData.addData.show.key;
        $http.get(url).success(function (data) {
            $scope.setPagingData(data.result, 1, 1);
            $scope.totalServerItems = data.result.length;
        });
    }

    //查看圈子里的帖子
    $scope.openCommunity = function (data) {
        if(data.is_container == 1){
            $state.go('app.community_child', {
                community_id: data.community_id
            });
            return;
        }
        $state.go('app.community_post', {
            community_id: data.community_id
        });
    };

    //查看圈子成员列表
    $scope.openFans = function  (data) {
        $state.go('app.community_fans', {
            community_id: data.community_id,
            creator_id: data.creator_id
        });
    }

    $scope.editinitUploadWidget();

    $scope.digestScope = function () {
        setTimeout(function() {
            try {
                $scope.$digest();
            }
            catch(e) {
                console.log(e);
            }
        });
    };
}]);