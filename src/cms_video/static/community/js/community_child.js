app.controller('communitieschildCtrl', ['$rootScope', '$scope', '$http', '$location', '$stateParams', '$state', function ($rootScope, $scope, $http,$location, $stateParams, $state){

    $scope.child = {
        deleteData:''
    };

	$scope.filterOptions = {
        filterText: "",
        useExternalFilter: true  
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [20, 50, 100],
        pageSize: 20,
        currentPage: 1
    };

    //创建表单
    $scope.setPagingData = function(data){
        $scope.myData = data;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };

    //获取圈子列表数据
    $scope.getPagedDataAsync = function (page, prePage) {
        setTimeout(function () {
            var url = '/cms/communities/getchildList?community_id='+$stateParams.community_id+'&perPage=' + prePage + "&page=" + page;
            $http.get(url).success(function (data) {
                $scope.setPagingData(data.result.pageData, page, prePage);
                $scope.totalServerItems = data.result.count;
            });
        }, 100);
    };

    //待创建表单
    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && (newVal.currentPage !== oldVal.currentPage || newVal.pageSize !== oldVal.pageSize)) {
            $scope.getPagedDataAsync($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize, $scope.filterOptions.filterText);
        }
    }, true);

  
    var operation = ' <div ><span class="label bg-danger" data-toggle="modal" data-target=".deletData" ng-click="deletechild(row.entity)">删除</span>';
    operation += ' <span class="label bg-info" data-toggle="modal" data-target=".delete-user" ng-click="openCommunity(row.entity)">查看</span>';
    operation += ' <span class="label bg-success" data-toggle="modal" data-target=".delete-user" ng-click="openFans(row.entity)">成员</span></div>';

    $scope.columnDefs = [
        {filed: 'pic_url', displayName: '缩略图', cellTemplate: '<div><img style="height:80px;" src="{{row.entity.pic_url}}" alt="" /></div>'},
        {field: 'name', displayName: '名称', cellTemplate: '<span style="margin-left:10px">{{row.entity.name}}</span>'},
        {field: 'attr', displayName: '类别', cellTemplate: '<span>{{row.entity.attr?row.entity.attr:"未填写"}}</span>'},
        {field: 'nickname', displayName: '作者',cellTemplate: '<div>{{row.entity.nickname?row.entity.nickname:"官方"}}</div>'},
        {field: 'create_date', displayName: '日期',cellTemplate: '<span>{{row.entity.create_date |date:"yyyy-MM-dd hh:mm"}}</span>'},
        {field: '', displayName: '操作', cellTemplate: operation}
    ];
    $scope.gridOptions = {
        data: 'myData',
        columnDefs: $scope.columnDefs,
        enablePaging: true,
        showFooter: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        multiSelect: false,
        rowHeight:80,
        i18n: 'zh_cn'
    };
    window.ngGrid.i18n['zh_cn'] = yilos_i18n.resource;

    $scope.getPagedDataAsync(1, 20);

    //点击删除按钮为模态框赋值
    $scope.deletechild = function (data) {
        $scope.child.deleteData = data;
    };

    //删除模态框确认操作
    $scope.deletechildOk = function () {
        $http.post("/cms/communities/deletechild", {community_id: $scope.child.deleteData.community_id}).success(function(data){
            $scope.getPagedDataAsync(1, 20);
            $scope.pagingOptions.currentPage = 1;
            $(".deletData").modal('hide');
        });
    };

    //清空子圈子确认操作
    $scope.clearchildOk = function () {
        $http.post("/cms/communities/clearchild", {parent_id: $stateParams.community_id}).success(function(data){
            $scope.getPagedDataAsync(1, 20);
            $scope.pagingOptions.currentPage = 1;
            $(".clearData").modal('hide');
        });
    };

    //返回圈子列表
    $scope.goBackCommunities = function(){
        $state.go('app.communities', {

        });
    }

    //查看圈子里的帖子
    $scope.openCommunity = function (data) {
        $state.go('app.community_post', {
            community_id: data.community_id
        });
    };

    //查看圈子成员列表
    $scope.openFans = function  (data) {
        $state.go('app.community_fans', {
            community_id: data.community_id,
            creator_id: data.creator_id
        });
    }
    
}]);