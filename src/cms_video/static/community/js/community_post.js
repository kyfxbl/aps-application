app.controller('communitypostCtrl', ['$rootScope', '$scope', '$http', '$location', '$stateParams', '$state', function ($rootScope, $scope, $http,$location, $stateParams, $state){

    $scope.modalData = {
        show:{
            name:'',
            title:'',
            img:'',
            num:1,
            key:'',
            top:{
                post_id:'',
                tips:'',
                is_top:'',
                showtop:''      //利用对象的引用来改变视图
            },
            hot:{
                post_id:'',
                tips:'',
                is_hot:'',
                showhot:''      //利用对象的引用来改变视图
            },
            best:{
                post_id:'',
                tips:'',
                is_best:'',
                showbest:''      //利用对象的引用来改变视图
            },
            delete:'',
            pushdata:{
                post_id:'',
                content:'',
                tips:'',
                title:'美甲大咖团队'
            }
        },
        add:{
            nickname:'',
            creator_id:'',
            title:'',
            is_hot:0,
            is_top:0,
            content:'',
            imgs:[]
        },
        edit:{
            post:{
                nickname:'',
                creator_id:'',
                content:'',
                title:'',
                post_id:'',
                is_top:'',
                is_hot:''
            },
            imgs:'',
            addimg:'',
            deleteimg:''
        }     
    };

	$scope.filterOptions = {
        filterText: "",
        useExternalFilter: true  
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [20, 50, 100],
        pageSize: 20,
        currentPage: 1
    };

    //处理按钮状态
    $scope.btntype = function (data) {
        if(data){
            return 'bg-danger';
        }
        return 'bg-success';
    }

    //添加帖子选择搜索作者的数据
    $scope.addselectedAuthor = function (data) {
        if(data){
            $scope.modalData.add.creator_id = data.originalObject.id;
            return
        }
    }

     //编辑帖子选择搜索作者的数据
    $scope.editselectedAuthor = function (data) {
        if(data){
            $scope.modalData.edit.post.creator_id = data.originalObject.id;
            return
        }
    }

    //搜索作者传参数
    $scope.remoteUrlRequestFn = function(str) {
        return {key: str};
    };


    //返回圈子列表
    $scope.goBackCommunities = function(){
        $state.go('app.communities', {

        });
    }

    //创建表单
    $scope.setPagingData = function(data){
        $scope.myData = data;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };

    //获取帖子列表数据
    $scope.getPagedDataAsync = function (page, prePage) {
        setTimeout(function () {
            var url = '/cms/community_post/postList?community_id='+$stateParams.community_id +'&perPage='+ prePage + '&page=' + page;
            $http.get(url).success(function (data) {
                $scope.setPagingData(data.result.pageData, page, prePage);
                $scope.totalServerItems = data.result.count;
                $scope.modalData.show.name = data.result.comminity_name;
            });
        }, 100);
    };

    //待创建表单
    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && (newVal.currentPage !== oldVal.currentPage || newVal.pageSize !== oldVal.pageSize)) {
            $scope.getPagedDataAsync($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize, $scope.filterOptions.filterText);
        }
    }, true);

    var operation = '<div >&nbsp;&nbsp;&nbsp;<span class="label" data-toggle="modal" data-target=".toggleTop" ng-click="toggleTopBtn(row.entity)" ng-class="btntype(row.entity.is_top != 0)">{{row.entity.is_top != 0? "已置顶": "置顶"}}</span>';
    operation += ' <span class="label" data-toggle="modal" data-target=".toggleHot" ng-click="toggleHotBtn(row.entity)" ng-class="btntype(row.entity.is_hot != 0)">{{row.entity.is_hot != 0? "已推荐": "推荐"}}</span>';
    operation += ' <span class="label" data-toggle="modal" data-target=".toggleBest" ng-click="toggleBestBtn(row.entity)" ng-class="btntype(row.entity.is_best != 0)">{{row.entity.is_best != 0? "已加精": "加精"}}</span>';
    operation += ' <span class="label bg-danger" data-toggle="modal" data-target=".deletePost" ng-click="deletePost(row.entity)">删除</span>';
    operation += ' <span class="label bg-success" data-toggle="modal" data-target=".editPost" ng-click="editPostBtn(row.entity)">编辑</span>';
    operation += ' <span class="label bg-info" data-toggle="modal" data-target=".pushPost" ng-click="pushPostBtn(row.entity)">推送</span></div>';

    $scope.columnDefs = [
        {field: 'title', displayName: '标题', cellTemplate: '<span style="margin-left:10px">{{row.entity.title}}</span>'},
        {field: 'content', displayName: '内容', cellTemplate: '<span>{{row.entity.content?row.entity.content:"没有内容"}}</span>'},
        {field: '', displayName: '图片',cellTemplate: '<span class="label bg-info" data-toggle="modal" data-target=".showPostImg" ng-click="showPostImg(row.entity)">查看图片</span><span style="margin-left:10px;" class="label bg-success" ng-click="openComments(row.entity)">查看评论</span>'},
        {field: 'nickname', displayName: '作者',cellTemplate: '<div>{{row.entity.nickname?row.entity.nickname:"官方"}}</div>'},
        {field: 'create_date', displayName: '日期',cellTemplate: '<span>{{row.entity.create_date |date:"yyyy-MM-dd hh:mm"}}</span>'},
        {field: '', displayName: '操作', cellTemplate: operation}
    ];
    $scope.gridOptions = {
        data: 'myData',
        columnDefs: $scope.columnDefs,
        enablePaging: true,
        showFooter: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        multiSelect: false,
        rowHeight:80,
        i18n: 'zh_cn'
    };
    window.ngGrid.i18n['zh_cn'] = yilos_i18n.resource;

    $scope.getPagedDataAsync(1, 20);

    //获取帖子中的图片
    $scope.getPostImg = function (post_id, callback) {
        var url = '/cms/community_post/postImg?post_id='+post_id;
        $http.get(url).success(function (data) { 
            callback(data);
        });
    }

    //点击查看图片按钮
    $scope.showPostImg = function (data) {
        $scope.modalData.show.title= data.title;
        $scope.getPostImg(data.post_id, function (data) {
            $scope.modalData.show.img = data.result;
        });
    }


    //置顶和取消置顶按钮
    $scope.toggleTopBtn = function (data) {
        $scope.modalData.show.title       = data.title;
        $scope.modalData.show.top.post_id = data.post_id;
        $scope.modalData.show.top.showtop = data;

        if(data.is_top != 0){
            $scope.modalData.show.top.tips   = "确定取消置顶？";
            $scope.modalData.show.top.is_top = 0;
            return;
        }

        $scope.modalData.show.top.tips   = "确定置顶帖子？";
        $scope.modalData.show.top.is_top = 1;
    }

    //top模态框确定
    $scope.toggleTopOk = function () {
        var url = '/cms/community_post/toggleTop';
        $http.post(url,{post_id: $scope.modalData.show.top.post_id, is_top: $scope.modalData.show.top.is_top}).success(function (data) {
            $(".toggleTop").modal('hide');
            if($scope.modalData.show.top.is_top != 0){
                $scope.modalData.show.top.showtop.is_top = 1;
                return;
            }
            $scope.modalData.show.top.showtop.is_top = 0; 
        });
        return;
    }

    //推荐和取消推荐按钮
    $scope.toggleHotBtn = function (data) {
        $scope.modalData.show.title       = data.title;
        $scope.modalData.show.hot.post_id = data.post_id;
        $scope.modalData.show.hot.showhot = data;

        if(data.is_hot != 0){
            $scope.modalData.show.hot.tips   = "确定取消推荐？";
            $scope.modalData.show.hot.is_hot = 0;
            return;
        }
        $scope.modalData.show.hot.tips   = "确定推荐帖子？";
        $scope.modalData.show.hot.is_hot = 1;
    }

    //hot模态框确定
    $scope.toggleHotOk = function () {
        var url = '/cms/community_post/toggleHot';
        $http.post(url,{post_id: $scope.modalData.show.hot.post_id, is_hot: $scope.modalData.show.hot.is_hot}).success(function (data) {
            $(".toggleHot").modal('hide');
            if($scope.modalData.show.hot.is_hot != 0){
                $scope.modalData.show.hot.showhot.is_hot = 1;
                return;
            }
            $scope.modalData.show.hot.showhot.is_hot = 0;
        });
        return;
    }

    //加精和取消加精按钮
    $scope.toggleBestBtn = function (data) {
        $scope.modalData.show.title       = data.title;
        $scope.modalData.show.best.post_id = data.post_id;
        $scope.modalData.show.best.showbest = data;

        if(data.is_best != 0){
            $scope.modalData.show.best.tips   = "确定取消加精？";
            $scope.modalData.show.best.is_best = 0;
            return;
        }
        $scope.modalData.show.best.tips   = "确定加精帖子？";
        $scope.modalData.show.best.is_best = 1;
    }

    //帖子推送按钮
    $scope.pushPostBtn = function (data) {
        $scope.modalData.show.pushdata.content = '';
        $scope.modalData.show.pushdata.title   = '美甲大咖团队';
        $scope.modalData.show.pushdata.post_id = data.post_id;
        $scope.modalData.show.pushdata.tips    = data.title;
    }

    //帖子推送确定按钮
    $scope.pushPostOk = function () {
        var url = "/cms/msg/pushPost";
        var post_id = $scope.modalData.show.pushdata.post_id;
        var content = $scope.modalData.show.pushdata.content;
        var title   = $scope.modalData.show.pushdata.title;
        if(content){
            $http.post(url,{post_id: post_id, title: title, content: content}).success(function (data) {
                $(".pushPost").modal('hide');
                console.log(data);
            });
        }
    }

    //best模态框确定

    //加精模态框确认按钮
    $scope.toggleBestOk = function () {
        var url = '/cms/community_post/toggleBest';
        $http.post(url,{post_id: $scope.modalData.show.best.post_id, is_best: $scope.modalData.show.best.is_best}).success(function (data) {
            $(".toggleBest").modal('hide');
            if($scope.modalData.show.best.is_best != 0){
                $scope.modalData.show.best.showbest.is_best = 1;
                return;
            }
            $scope.modalData.show.best.showbest.is_best = 0;
        });
        return;
    }

    //新建帖子按钮重置add数据
    $scope.addNewData = function () {
        $scope.modalData.show.creator   = 'null';
        $scope.modalData.add.nickname   = '';
        $scope.modalData.add.creator_id = '';
        $scope.modalData.add.is_hot     = 0;
        $scope.modalData.add.is_top     = 0;
        $scope.modalData.add.title      = '';
        $scope.modalData.add.content    = '';
        $scope.modalData.add.post_id    = '';
        $scope.modalData.add.imgs       = [];
    }

    //新建帖子top开关
    $scope.chooseTop = function () {
        if($scope.modalData.add.is_top != 0){
            $scope.modalData.add.is_top = 0;
            return;
        }
           $scope.modalData.add.is_top = 1; 
    }

    //新建帖子hot开关
    $scope.chooseHot = function () {
        if($scope.modalData.add.is_hot != 0){
            $scope.modalData.add.is_hot = 0;
            return;
        }
           $scope.modalData.add.is_hot = 1; 
    }

    //点击新建确定按钮
    $scope.addPostOk = function () {
        var url          = "/cms/community_post/addPost";
        var creator_id   = $scope.modalData.add.creator_id;
        var community_id = $stateParams.community_id;
        var title        = $scope.modalData.add.title;
        var content      = $scope.modalData.add.content;
        var is_hot       = $scope.modalData.add.is_hot;
        var is_top       = $scope.modalData.add.is_top;
        var imgs         = $scope.modalData.add.imgs;
        if( creator_id && title && content){
            $http.post(url,{community_id: community_id, title: title, content: content, is_hot: is_hot, is_top: is_top, imgs: imgs, creator_id: creator_id}).success(function (data) {
                $(".addPost").modal('hide');
                $scope.getPagedDataAsync(1, 20);
                $scope.pagingOptions.currentPage = 1;
            });
            return;
        }
        alert("请将信息填写完整");
    }

    //新建帖子上传图片
    $scope.addinitUploadWidget = function(){
        $('#addpostpic_fileupload').fileupload({
            url: '/cms/picture/upload',
            prependFiles:"html",
            getFilesFromResponse:getThumb_pic_urlFromResponse,
            acceptFileTypes: /(\.|\/)(jpg|png)$/i,
            formData:function () {
                return [];
            }
        }).bind('fileuploadfinished',function(e,data){
              
                $("#uplpadimg").hide();
               
            })
            .addClass('fileupload-processing');

        function getThumb_pic_urlFromResponse(data){
            var img = {
                id:'',
                pic_url:data.result.result.files[0].url,
                serial_number:$scope.modalData.show.num
            };

            $scope.modalData.add.imgs.push(img);
            $scope.modalData.show.num++;

            $scope.digestScope();
            return data.result.result.files;
        }       
    };
    $scope.addinitUploadWidget();
    
    //删除图片
    $scope.deletepostimg = function (index) {

        $scope.modalData.add.imgs.splice(index,1);
    }

    //点击编辑按钮
    $scope.editPostBtn = function (data) {
        $scope.modalData.edit.creator        = 'null';
        $scope.modalData.edit.post.content    = data.content;
        $scope.modalData.edit.post.is_hot     = data.is_hot;
        $scope.modalData.edit.post.is_top     = data.is_top;
        $scope.modalData.edit.post.post_id    = data.post_id;
        $scope.modalData.edit.post.title      = data.title;
        $scope.modalData.edit.post.creator_id = data.account_id;
        $scope.modalData.edit.post.nickname   = data.nickname;

        //获取帖子中的图片
        $scope.getPostImg(data.post_id, function (result) {
            $scope.modalData.edit.imgs = result.result;
            for (var i = 0; i < $scope.modalData.edit.imgs.length; i++) {
                $scope.modalData.edit.imgs[i].serial_number = $scope.modalData.edit.imgs[i].serial_number+1;
            };
        });
    }

    //编辑帖子top开关
    $scope.editTop = function () {
        if($scope.modalData.edit.post.is_top != 0){
            $scope.modalData.edit.post.is_top = 0;
            return;
        }

        $scope.modalData.edit.post.is_top = 1; 
    }

    //编辑帖子hot开关
    $scope.editHot = function () {
        if($scope.modalData.edit.post.is_hot != 0){
            $scope.modalData.edit.post.is_hot = 0;
            return;
        }

        $scope.modalData.edit.post.is_hot = 1; 
    }

    //编辑帖子添加图片
    $scope.editaddimg = function(post_id, serial_number, pic_url) {
        var url = '/cms/community_post/addPostImg';
        $http.post(url, {post_id: post_id, serial_number: serial_number, pic_url: pic_url}).success(function (data) {

            //添加图片成功后重新获取图片列表
            $scope.getPostImg(post_id, function (result) {
                $scope.modalData.edit.imgs = result.result;
                for (var i = 0; i < $scope.modalData.edit.imgs.length; i++) {
                    $scope.modalData.edit.imgs[i].serial_number = $scope.modalData.edit.imgs[i].serial_number+1;
                };
            });
        });
    }

    //编辑帖子上传图片
    $scope.editinitUploadWidget = function(){
        $('#editpostpic_fileupload').fileupload({
            url: '/cms/picture/upload',
            prependFiles:"html",
            getFilesFromResponse:getThumb_pic_urlFromResponse,
            acceptFileTypes: /(\.|\/)(jpg|png)$/i,
            formData:function () {
                return [];
            }
        }).bind('fileuploadfinished',function(e,data){
              
                $("#uplpadimg").hide();
               
            })
            .addClass('fileupload-processing');

        function getThumb_pic_urlFromResponse(data){
            var post_id       = $scope.modalData.edit.post.post_id;
            var serial_number = $scope.modalData.edit.imgs.length;
            var pic_url       = data.result.result.files[0].url;

            //编辑帖子添加图片
            $scope.editaddimg(post_id, serial_number, pic_url);

            $scope.digestScope();
            return data.result.result.files;
        }       
    };
    $scope.editinitUploadWidget();

    //编辑帖子删除图片
    $scope.editdeletepostimg = function (pic_id) {
        var url = '/cms/community_post/deletePostImg';
        $http.post(url, {pic_id: pic_id}).success(function (data) {
            var post_id = $scope.modalData.edit.post.post_id;
            $scope.getPostImg(post_id, function (result) {
                $scope.modalData.edit.imgs = result.result;
                for (var i = 0; i < $scope.modalData.edit.imgs.length; i++) {
                    $scope.modalData.edit.imgs[i].serial_number = $scope.modalData.edit.imgs[i].serial_number+1;
                };
            });
        });
    }
    
    //点击编辑确定按钮
    $scope.editPostOk = function () {
        var url        = "/cms/community_post/editPost";
        var post_id    = $scope.modalData.edit.post.post_id;
        var title      = $scope.modalData.edit.post.title;
        var content    = $scope.modalData.edit.post.content;
        var is_top     = $scope.modalData.edit.post.is_top;
        var is_hot     = $scope.modalData.edit.post.is_hot;
        var creator_id = $scope.modalData.edit.post.creator_id;

        if( creator_id && title && content){
           $http.post(url, {post_id: post_id, title: title, content: content, is_hot: is_hot, is_top: is_top, imgs: $scope.modalData.edit.imgs, creator_id: creator_id}).success(function (data) {
                $(".editPost").modal('hide');
                $scope.getPagedDataAsync(1, 20);
                $scope.pagingOptions.currentPage = 1;
            });
            return; 
        }
        alert("请将信息填写完整");      
    }

    //点击删除按钮
    $scope.deletePost = function (data) {
        $scope.modalData.show.delete = data;
    }

    //点击删除确定按钮
    $scope.deletePostOk = function () {
        var url = "/cms/community_post/deletePost";
        var post_id = $scope.modalData.show.delete.post_id;

        $http.post(url, {post_id: post_id}).success(function (data) {
            $(".deletePost").modal('hide');
            $scope.getPagedDataAsync(1, 20);
            $scope.pagingOptions.currentPage = 1;
        });
    }

    //清空圈子内的帖子
    $scope.clearPostOk = function () {
       var url          = "/cms/community_post/clearPost";
       var community_id = $stateParams.community_id;
       $http.post(url, {community_id: community_id}).success(function (data) {
            $(".clearPost").modal('hide');
            $scope.getPagedDataAsync(1, 20);
            $scope.pagingOptions.currentPage = 1;
        });
    }

    $scope.digestScope = function () {
        setTimeout(function() {
            try {
                $scope.$digest();
            }
            catch(e) {
                console.log(e);
            }
        });
    }

    //查看被举报的帖子
    $scope.getreported = function () {
        var url = '/cms/community_post/reportList?community_id='+$stateParams.community_id ;
        $http.get(url).success(function (data) {
            $scope.setPagingData(data.result, 1, 1);
            $scope.totalServerItems = data.result.length;
        });
    }

    //查看所有帖子
    $scope.getAllData = function () {
        $scope.getPagedDataAsync(1, 20);
        $scope.pagingOptions.currentPage = 1;
    }

    //搜索帖子
    $scope.getsearchdata = function () {
        if(!$scope.modalData.show.key){
            alert('搜索关键词不能为空');
            return;
        }
        var url = '/cms/community_post/searchList?community_id='+$stateParams.community_id +'&key='+$scope.modalData.show.key;
        $http.get(url).success(function (data) {
            $scope.setPagingData(data.result, 1, 1);
            $scope.totalServerItems = data.result.length;
        });
    }

    //跳转到帖子评论列表
    $scope.openComments = function(data){
        $state.go('app.community_post_comments', {
            post_id: data.post_id
        });
    }
}]);