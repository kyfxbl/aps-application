app.controller('goodsSearchCtrl', ['$rootScope', '$scope', '$http','$location','$stateParams','$state', function ($rootScope, $scope, $http,$location, $stateParams,$state){

    // 0 for all, 1 for pull off, 2 for recommend
	$scope.goodsSearch = {
		show:{
			type: 0,
            delete:'',
            toggleshow:{
                name:'',
                id:'',
                available:'',
                hot:''
            },
            key:''
        }
	};

	// grid data init
	$scope.filterOptions = {
	    filterText: "",
	    useExternalFilter: true  
	};
	$scope.totalServerItems = 0;
	$scope.pagingOptions = {
	    pageSizes: [20, 50, 100],
	    pageSize: 20,
	    currentPage: 1
	};	

    // init search key
    $scope.goodsSearch.show.key = $stateParams.key;

    // create grid
    $scope.setPagingData = function(data){
        $scope.myData = data;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };

    // query list for key
    $scope.getSearchData = function (page, prePage, key) {
        setTimeout(function () {
            var url = '/cms/goods/getsearchList?key='+$scope.goodsSearch.show.key+'&perPage=' + prePage + "&page=" + page+ "&type=0";
            $http.get(url).success(function (data) {
                $scope.setPagingData(data.result.pageData);
                $scope.totalServerItems = data.result.count;
            });
        }, 100);
    };

    // get product first time
    if($stateParams.type == 0){
        $scope.getSearchData($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize, $scope.goodsSearch.show.key);
        $scope.goodsSearch.show.type = 0;
    }else if($stateParams.type == 1){
        getNoShowGoods($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize);
        $scope.goodsSearch.show.type = 1;
    }else{
        getHotGoods();
        $scope.goodsSearch.show.type = 2;
    }

    // watch data change
    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && (newVal.currentPage !== oldVal.currentPage || newVal.pageSize !== oldVal.pageSize)) {
            if($scope.goodsSearch.show.type == 0){
                $scope.getSearchData($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize, $scope.goodsSearch.show.key);
                return;
            }
            getNoShowGoods($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize);
        }
    }, true);

    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            if($scope.goodsSearch.show.type == 0){
                $scope.getSearchData($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize, $scope.goodsSearch.show.key);
                return;
            }
            getNoShowGoods($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize);
        }
    }, true);

    var operation = '<div >&nbsp;&nbsp;&nbsp;<span class="label bg-success" ng-click="editgoods(row.entity)">edit</span>';
    operation += ' <span class="label bg-info" data-toggle="modal" ng-click="hotgoods(row.entity)">{{row.entity.is_hot != 1? "recommend": "cancel recommend"}}</span>';
    operation += '&nbsp;<a class="label bg-info" href="{{row.entity.url}}" target="_blank"><span>detail</span></a>';
    operation += ' <span class="label bg-danger" data-toggle="modal" data-target=".deletegoods" ng-click="deletegoods(row.entity)">delete</span>';
    operation += ' <span class="label bg-info" data-toggle="modal" data-target=".showgoods" ng-click="showgoods(row.entity)">{{row.entity.available != 1? "put on": "pull off"}}</span></div>';

    $scope.columnDefs = [
        {filed: 'pic_url', displayName: 'picture', cellTemplate: '<div><img style="height:80px;" src="{{row.entity.pic_url}}" alt="" /></div>'},
        {field: 'category_name', displayName: 'category', cellTemplate: '<span style="margin-left:10px">{{row.entity.category_name}}</span>'},
        {field: 'name', displayName: 'name', cellTemplate: '<span style="margin-left:10px">{{row.entity.name}}</span>'},
        {field: 'description', displayName: 'description', cellTemplate: '<span>{{row.entity.description}}</span>'},
        {field: 'price', displayName: 'price',cellTemplate: '<div>{{row.entity.price}}</div>'},
        {field: '', displayName: 'operation', cellTemplate: operation}
    ];

    $scope.gridOptions = {
        data: 'myData',
        columnDefs: $scope.columnDefs,
        enablePaging: true,
        showFooter: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        multiSelect: false,
        rowHeight:80,
        i18n: 'zh_cn'
    };

    window.ngGrid.i18n['zh_cn'] = yilos_i18n.resource;

    // query pull off product
    function getNoShowGoods (prePage, page) {
        setTimeout(function () {
            var url = '/cms/goods/getnotshowList?perPage='+prePage+"&page="+page;
            $http.get(url).success(function (data) {
                $scope.setPagingData(data.result.pageData);
                $scope.totalServerItems = data.result.count;
            });
        }, 100);
    }

    // query recommend
    function getHotGoods () {
        setTimeout(function () {
            var url = '/cms/goods/gethotGoods';
            $http.get(url).success(function (data) {
                $scope.setPagingData(data.result);
                $scope.totalServerItems = data.result.length;
            });
        }, 100);
    }

    // delete product
    $scope.deletegoods = function (data) {
    	$scope.goodsSearch.show.delete = data;
    }

    // delete product confirm
    $scope.deletegoodsOk = function () {
    	var url = "/cms/goods/deletegoods";

    	$http.post(url, {id: $scope.goodsSearch.show.delete.id}).success(function (data) {
    		
	    	$(".deletegoods").modal('hide');
		    
            if($scope.goodsSearch.show.type == 0){
                $scope.getSearchData($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize, $scope.goodsSearch.show.key);
                return;
            }else if( $scope.goodsSearch.show.type == 1){
                getNoShowGoods($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize);
                return;
            }else{
                getHotGoods();
            }
    	});
    }

    // put on or pull off
    $scope.showgoods = function (data) {

		$scope.goodsSearch.show.toggleshow.name = data.name;
		$scope.goodsSearch.show.toggleshow.id   = data.id;
    	if(data.available == 0){
    		$scope.goodsSearch.show.toggleshow.available = 1;
    		return;
    	}
    	$scope.goodsSearch.show.toggleshow.available = 0;
    }

    // confirm put on or pull off
    $scope.showgoodsOk = function () {

     	var url = "/cms/goods/togglegoods";

     	$http.post(url, {id: $scope.goodsSearch.show.toggleshow.id, available: $scope.goodsSearch.show.toggleshow.available}).success(function (data) {
            if(data.result.msg == false){
                alert("please cancel recommend first");
                $(".showgoods").modal('hide');
                return;
            }
    		
	    	$(".showgoods").modal('hide');
		   	
            if($scope.goodsSearch.show.type == 0){
                $scope.getSearchData($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize, $scope.goodsSearch.show.key);
                return;
            }else if( $scope.goodsSearch.show.type == 1){
                getNoShowGoods($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize);
                return;
            }else{
                getHotGoods();
            }  
    	});
    };

    // recommend and cancel recommend
    $scope.hotgoods = function (data) {
        $(".hotgoods").modal('show');
        $scope.goodsSearch.show.toggleshow.name = data.name;
        $scope.goodsSearch.show.toggleshow.id   = data.id;
        if(data.is_hot == 0){
            $scope.goodsSearch.show.toggleshow.hot = 1;
            return;
        }
        $scope.goodsSearch.show.toggleshow.hot = 0;
    }

    // confirm recommend and cancel recommend
    $scope.hotgoodsOk = function () {

        var url = "/cms/goods/togglehot";

        $http.post(url, {id: $scope.goodsSearch.show.toggleshow.id, is_hot: $scope.goodsSearch.show.toggleshow.hot}).success(function (data) {

            if(data.result.msg == false){
                alert("recommend fail");
               $(".hotgoods").modal('hide');
                return;
            }
            
            $(".hotgoods").modal('hide');

            if($scope.goodsSearch.show.type == 0){
                $scope.getSearchData($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize, $scope.goodsSearch.show.key);
            }else if( $scope.goodsSearch.show.type == 1){
                getNoShowGoods($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize);
            }else{
                getHotGoods();
            }
        });
    };

    // jump to edit page
    $scope.editgoods = function (data) {
        $state.go('app.goods_edit', {
            goods_id: data.id
        });
    };

    // back
    $scope.goBack = function () {
        
        $state.go('app.goods_list', {
            cate: $stateParams.cate
        });
    };

    // choose data
    $scope.selectedGoods = function (data) {

        // refresh grid data
        $scope.totalServerItems = 0;
        $scope.pagingOptions.pageSize = 20;
        $scope.pagingOptions.currentPage = 1;
        $scope.setPagingData([data.originalObject]);
        $scope.totalServerItems = 1;
        $scope.goodsSearch.show.type = 0;
    };

    $scope.remoteUrlRequestFn = function(str) {
        return {key: str};
    };

}]);