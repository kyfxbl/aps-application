app.controller('goodsEditCtrl', ['$rootScope', '$scope', '$http','$location','$stateParams', '$state', function ($rootScope, $scope, $http,$location,$stateParams, $state){

	$scope.goodsEdit = {
		show:{
			data:{
				name:''
			},
			cates:''
		},
		edit:{
			id:'',
			category_id: '',
			name: '',
			price: '',
			url: '',
			pic_url: '',
			description: '',
			available: 0,
			real_id: '',
			cost:'',
			is_hot:''
		}
	};

	// get product detail by id
	function getGoods () {

		var url = '/cms/goods/getgoods?id=' + $stateParams.goods_id;

		$http.get(url).success(function (data) {

			$scope.goodsEdit.show.data.name = data.result[0].name;
			$scope.goodsEdit.edit.id = $stateParams.goods_id;
			$scope.goodsEdit.edit.category_id = data.result[0].category_id;
			$scope.goodsEdit.edit.name = data.result[0].name;
			$scope.goodsEdit.edit.price = data.result[0].price;
			$scope.goodsEdit.edit.url = data.result[0].url;
			$scope.goodsEdit.edit.pic_url = data.result[0].pic_url;
			$scope.goodsEdit.edit.description = data.result[0].description;
			$scope.goodsEdit.edit.available = data.result[0].available;
			$scope.goodsEdit.edit.real_id = data.result[0].real_id;
			$scope.goodsEdit.edit.cost = data.result[0].cost;
			$scope.goodsEdit.edit.is_hot = data.result[0].is_hot;
		});
	}

	// get product categories
	function getCates () {

		var url = '/cms/goods/getcategoryList';
		$http.get(url).success(function (data) {
			$scope.goodsEdit.show.cates = data.result;
			getGoods ();			
		});
	}

	getCates ();

	$scope.chooseCate = function (data) {
		if (data == 'null') {
			$scope.goodsEdit.edit.category_id = null;
		}
	};

	// if put on shelf
	$scope.goodsShow = function (data) {
		if(data == 0){
			$scope.goodsEdit.edit.available = 1;
			return;
		}
		$scope.goodsEdit.edit.available = 0;
	};

	// if recommend
	$scope.goodsHot = function (data) {
		if(data == 0){
			$scope.goodsEdit.edit.is_hot = 1;
			return;
		}
		$scope.goodsEdit.edit.is_hot = 0;
	};

	// confirm edit product
	$scope.editGoodsOk = function () {

		var category_id = $scope.goodsEdit.edit.category_id;
		var name		= $scope.goodsEdit.edit.name;
		var price		= $scope.goodsEdit.edit.price;
		var url         = $scope.goodsEdit.edit.url;
		var real_id	    = $scope.goodsEdit.edit.real_id;
		var cost	    = $scope.goodsEdit.edit.cost;
		
		if(category_id && name &&  url && real_id){

			var url = "/cms/goods/editgoods";

			$http.post(url, $scope.goodsEdit.edit).success(function (data) {

				if(data.result.msg == false){
					alert("fail, too much recommend product");
					$(".editGoods").modal('hide');
					return;
				}

                alert("edit succeed");
                $(".editGoods").modal('hide');

                // return to product list
                $state.go('app.goods_list', {
                	cate: $stateParams.cate
        		});
            });
            return;
		}

		alert("please fill in product info");
	};

	// upload picture
    $scope.addinitUploadWidget = function(){

        $('#goods_pic_fileupload').fileupload({
            url: '/cms/picture/upload',
            prependFiles:"html",
            getFilesFromResponse:getThumb_pic_urlFromResponse,
            acceptFileTypes: /(\.|\/)(jpg|png)$/i,
            formData:function () {
                return [];
            }
        }).bind('fileuploadfinished',function(e,data){
        		 $(".uplpadimg").hide();
            })
            .addClass('fileupload-processing');

        function getThumb_pic_urlFromResponse(data){
            $scope.goodsEdit.edit.pic_url  = data.result.result.files[0].url;
            $scope.digestScope();
            return data.result.result.files;
        }       
    };

    $scope.addinitUploadWidget();

    // delete picture
    $scope.editaddimg = function () {
        $scope.goodsEdit.edit.pic_url = '';
    };

    $scope.digestScope = function () {

        setTimeout(function() {
            try {
                $scope.$digest();
            }
            catch(e) {
                console.log(e);
            }
        });
    };

    // back
    $scope.goBack = function () {
    	
    	$state.go('app.goods_list', {
    		cate: $stateParams.cate
        });
    }

}]);