app.controller('goodsNewCtrl', ['$rootScope', '$scope', '$http','$location','$state', function ($rootScope, $scope, $http,$location, $state){

	$scope.goodsNew = {
		show:{
			cates:'',
			cate:'null'
		},
		add:{
			category_id: '',
			name: '',
			price: '',
			url: '',
			pic_url: '',
			description: '',
			available: 1,
			real_id:'',
			cost:'',
			is_hot:0
		}
	};

	// get categories
	function getCates() {

		var url = '/cms/goods/getcategoryList';
		$http.get(url).success(function (data) {
			$scope.goodsNew.show.cates = data.result;
								
		});
	}

	getCates();

	// select category
	$scope.chooseCate = function (cate) {
		if(cate == 'null'){
			$scope.goodsNew.add.category_id = null;
			return;
		}
		$scope.goodsNew.add.category_id = JSON.parse(cate).category_id;
	};

	// resolve if put on or pull off
	$scope.goodsShow = function (data) {
		if(data == 0){
			$scope.goodsNew.add.available = 1;
			return;
		}
		$scope.goodsNew.add.available = 0;
	};

	// resolve if recommend
	$scope.goodsHot = function (data) {
		if(data == 0){
			$scope.goodsNew.add.is_hot = 1;
			return;
		}
		$scope.goodsNew.add.is_hot = 0;
	};

	// confirm add product
	$scope.addGoodsOK = function(){

		var category_id = $scope.goodsNew.add.category_id;
		var name        = $scope.goodsNew.add.name;
		var price       = $scope.goodsNew.add.price;
		var url         = $scope.goodsNew.add.url;
		var real_id     = $scope.goodsNew.add.real_id;
		var cost        = $scope.goodsNew.add.cost;

		if(category_id && name && url && real_id){
			var url = "/cms/goods/createnewGoods";	
			$http.post(url,$scope.goodsNew.add).success(function (data) {
				if(data.result.msg == false){
					alert("add product fail, too many recommend");
					return;
				}
                alert("add succeed");
                $scope.goodsNew.show.cate = 'null';
                $scope.goodsNew.add = {
                	category_id: '',
					name: '',
					price: '',
					url: '',
					pic_url: '',
					description: '',
					available: 1,
					real_id: '',
					cost:'',
					is_hot:''
                };
            });
            return;
		}
		alert("please fill in complete product info");
	};

	// add product upload picture
    $scope.addinitUploadWidget = function(){
        $('#goods_pic_fileupload').fileupload({
            url: '/cms/picture/upload',
            prependFiles:"html",
            getFilesFromResponse:getThumb_pic_urlFromResponse,
            acceptFileTypes: /(\.|\/)(jpg|png)$/i,
            formData:function () {
                return [];
            }
        }).bind('fileuploadfinished',function(e,data){
        		$(".uplpadimg").hide();
            })
            .addClass('fileupload-processing');

        function getThumb_pic_urlFromResponse(data){
            $scope.goodsNew.add.pic_url  = data.result.result.files[0].url;
            $scope.digestScope();
            return data.result.result.files;
        }       
    };
    $scope.addinitUploadWidget();

    // add product delete picture
    $scope.deleteaddimg = function () {
        $scope.goodsNew.add.pic_url = '';
    };

    $scope.digestScope = function () {
        setTimeout(function() {
            try {
                $scope.$digest();
            }
            catch(e) {
                console.log(e);
            }
        });
    };
}]);