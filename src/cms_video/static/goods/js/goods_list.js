app.controller('goodsListCtrl', ['$rootScope', '$scope', '$http','$location','$stateParams','$state', function ($rootScope, $scope, $http,$location,$stateParams, $state){

	$scope.colors = ['primary', 'info', 'success', 'warning', 'danger', 'dark'];

	$scope.goodsList = {
		show:{
			cates:'',
			selected:0,
			category_id:'',
			category_name:'',
            key:''
		},
		add:{
			cate:{
				name:'',
                pic_url:''
			}
		},
		edit:{
			cate:{
                id:'',
                name:'',
                pic_url:''
            },
			goods:{
				name:'',
				id:'',
				available:'',
                is_hot:''
			}
		},
		delete:{
			cate:'',
			goods:''
		}
	};

    // resolve which category
    if($stateParams.cate){
        $scope.goodsList.show.selected = $stateParams.cate;
    }

	// init grid data
	$scope.filterOptions = {
	    filterText: "",
	    useExternalFilter: true  
	};
	$scope.totalServerItems = 0;
	$scope.pagingOptions = {
	    pageSizes: [20, 50, 100],
	    pageSize: 20,
	    currentPage: 1
	};	

	// get category list
	function getCates () {

		var url = '/cms/goods/getcategoryList';
		$http.get(url).success(function (data) {
			$scope.goodsList.show.cates = data.result;

			// random color
			angular.forEach($scope.goodsList.show.cates, function(cate){
                cate.color = $scope.colors[Math.floor((Math.random()*6))];
                cate.selected = false;
            });

			$scope.selectCate($scope.goodsList.show.selected);// select first category by default
		});
	}
	getCates ();

	// select category
	$scope.selectCate = function(index){

		$scope.goodsList.show.selected = index;// current selected category
		$scope.goodsList.show.category_id = $scope.goodsList.show.cates[index].category_id;
		$scope.goodsList.show.category_name = $scope.goodsList.show.cates[index].category_name;

		// init, no category selected
		angular.forEach($scope.goodsList.show.cates, function(cate){
            cate.selected = false;
        });

		// change category, reload data
	 	$scope.totalServerItems = 0;
	 	$scope.pagingOptions.pageSize = 20;
	 	$scope.pagingOptions.currentPage = 1;	

        // get products
		$scope.getgoodsData($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize, $scope.goodsList.show.cates[index].category_id);	
        $scope.goodsList.show.cates[index].selected = true;
    };

    // add new category
    $scope.addCate = function () {
        $scope.goodsList.add.cate = {
            name:'',
            pic_url:''
        }
    };

    // confirm add category
    $scope.addCateOk =function () {
    	var url = '/cms/goods/createnewCategory';
    	if($scope.goodsList.add.cate.name){
    		$http.post(url, $scope.goodsList.add.cate).success(function (data) {
    			$(".addCate").modal('hide');
	    		getCates();// refresh
	    	});
	    	return;
    	}
    	alert("category can't be empty");
    };
    	
    // edit product button pressed
    $scope.editCate = function (cate) {
        $scope.goodsList.edit.cate.id = cate.category_id;
        $scope.goodsList.edit.cate.name = cate.category_name;
        $scope.goodsList.edit.cate.pic_url = cate.pic_url;
    };

    // confirm edit
    $scope.editCateOk = function () {

    	var url = '/cms/goods/editcategory';

    	if($scope.goodsList.edit.cate.name){
    		$http.post(url,$scope.goodsList.edit.cate).success(function (data) {
    			$(".editCate").modal('hide');
	    		getCates();
	    	});
	    	return;
    	}
    	alert("商品类别名不能为空");
    };

    // delete button pressed
    $scope.deleteCate = function (cate) {
    	$scope.goodsList.delete.cate = cate;
    };

    // confirm delete
    $scope.deleteCateOk = function () {
    	var url = '/cms/goods/deletecategory';
    	
    	$http.post(url, {category_id: $scope.goodsList.delete.cate.category_id}).success(function (data) {
    		if (data.result.message) {
    			$scope.goodsList.show.selected = 0;
	    		$(".deleteCate").modal('hide');
		    	getCates ();
		    	return;
    		}
    		alert("please clear product first");
    		$(".deleteCate").modal('hide');
	    });
    };

    // grid
    $scope.setPagingData = function(data){
        $scope.myData = data;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };

    // get products
    $scope.getgoodsData = function (page, prePage, category_id) {

        setTimeout(function () {
            var url = '/cms/goods/getgoodsList?category_id='+category_id+'&perPage=' + prePage + "&page=" + page;
            $http.get(url).success(function (data) {
                $scope.setPagingData(data.result.pageData);
                $scope.totalServerItems = data.result.count;
            });
        }, 100);
    };

    // watch data change
    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && (newVal.currentPage !== oldVal.currentPage || newVal.pageSize !== oldVal.pageSize)) {
        	$scope.getgoodsData($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize, $scope.goodsList.show.category_id);
        }
    }, true);

    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getgoodsData($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize, $scope.goodsList.show.category_id);
        }
    }, true);

    var operation = '<div >&nbsp;&nbsp;&nbsp;<span class="label bg-success" ng-click="editgoods(row.entity)">edit</span>';
    operation += '<span class="label bg-info" data-toggle="modal" ng-click="hotgoods(row.entity)">{{row.entity.is_hot != 1? "recommend": "cancel recommend"}}</span>';
    operation += '<span class="label bg-danger" data-toggle="modal" data-target=".deletegoods" ng-click="deletegoods(row.entity)">delete</span>';
    operation += '<span class="label bg-info" data-toggle="modal" data-target=".showgoods" ng-click="showgoods(row.entity)">{{row.entity.available != 1? "put on": "pull off"}}</span></div>';

    $scope.columnDefs = [
        {filed: 'pic_url', displayName: 'picture', cellTemplate: '<div><img style="height:80px;" src="{{row.entity.pic_url}}" alt="" /></div>'},
        {field: 'name', displayName: 'name', cellTemplate: '<span style="margin-left:10px">{{row.entity.name}}</span>'},
        {field: 'description', displayName: 'description', cellTemplate: '<span>{{row.entity.description}}</span>'},
        {field: 'price', displayName: 'price',cellTemplate: '<div>{{row.entity.price}}</div>'},
        {field: 'url', displayName: 'detail',cellTemplate: '<a href="{{row.entity.url}}" target="_blank"><span class="label bg-info">detail</span></a>'},
        {field: '', displayName: 'operation', cellTemplate: operation}
    ];

    $scope.gridOptions = {
        data: 'myData',
        columnDefs: $scope.columnDefs,
        enablePaging: true,
        showFooter: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        multiSelect: false,
        rowHeight:80,
        i18n: 'zh_cn'
    };
    window.ngGrid.i18n['zh_cn'] = yilos_i18n.resource;

    // delete product
    $scope.deletegoods = function (data) {
    	$scope.goodsList.delete.goods = data;
    };

    // confirm delete product
    $scope.deletegoodsOk = function () {

    	var url = "/cms/goods/deletegoods";

    	$http.post(url, {id: $scope.goodsList.delete.goods.id}).success(function (data) {
    		
	    	$(".deletegoods").modal('hide');
		    getCates();
    	});
    };

    // put on or pull off
    $scope.showgoods = function (data) {
		$scope.goodsList.edit.goods.name = data.name;
		$scope.goodsList.edit.goods.id   = data.id;
    	if(data.available == 0){
    		$scope.goodsList.edit.goods.available = 1;
    		return;
    	}
    	$scope.goodsList.edit.goods.available = 0;
    };

    // confirm put on or pull off
    $scope.showgoodsOk = function () {

     	var url = "/cms/goods/togglegoods";

     	$http.post(url, {id: $scope.goodsList.edit.goods.id, available: $scope.goodsList.edit.goods.available}).success(function (data) {
            if(data.result.msg == false){
                alert("上架商品失败，请先取消推荐商品！");
                $(".showgoods").modal('hide');
                return;
            }
    		
	    	$(".showgoods").modal('hide');
		    getCates();
    	});
    };

    // recommend or cancel recommend
    $scope.hotgoods = function (data) {

        $(".hotgoods").modal('show');
        $scope.goodsList.edit.goods.name = data.name;
        $scope.goodsList.edit.goods.id   = data.id;
        if(data.is_hot == 0){
            $scope.goodsList.edit.goods.is_hot = 1;
            return;
        }
        $scope.goodsList.edit.goods.is_hot = 0;
    };

    // confirm recommend
    $scope.hotgoodsOk = function () {

        var url = "/cms/goods/togglehot";

        $http.post(url, {id: $scope.goodsList.edit.goods.id, is_hot: $scope.goodsList.edit.goods.is_hot}).success(function (data) {
            if(data.result.msg == false){
                alert("推荐商品失败，推荐商品数量超过限制！");
               $(".hotgoods").modal('hide');
                return;
            }
            $(".hotgoods").modal('hide');
            getCates();
        });
    };

    // jump to edit page
    $scope.editgoods = function (data) {
        $state.go('app.goods_edit', {
            goods_id: data.id,
            cate: $scope.goodsList.show.selected
        });
    };

    // jump to search page
    $scope.gosearch = function (data) {
        $state.go('app.goods_search', {
            key: data,
            type: 0,
            cate: $scope.goodsList.show.selected
        });
    };

    // query pull off product
    $scope.getNoShowGoodsBtn = function (data){
        $state.go('app.goods_search', {
            key: '',
            type: 1,
            cate: $scope.goodsList.show.selected
        });
    };

    // query recommend products
    $scope.getHotGoodsBtn = function (data){
        $state.go('app.goods_search', {
            key: '',
            type: 2,
            cate: $scope.goodsList.show.selected
        });
    };

    // delete picture
    $scope.deleteaddimg = function () {
        $scope.goodsList.add.cate.pic_url = '';
    };

    // delete picture
    $scope.deleteeditimg = function () {
        $scope.goodsList.edit.cate.pic_url = '';
    };

    // add category upload picture
    $scope.addinitUploadWidget = function(){

        $('#add_cat_fileupload').fileupload({
            url: '/cms/picture/upload',
            prependFiles:"html",
            getFilesFromResponse:getThumb_pic_urlFromResponse,
            acceptFileTypes: /(\.|\/)(jpg|png)$/i,
            formData:function () {
                return [];
            }
        }).bind('fileuploadfinished',function(e,data){
                $(".uplpadimg").hide();
            })
            .addClass('fileupload-processing');

        function getThumb_pic_urlFromResponse(data){
            $scope.goodsList.add.cate.pic_url  = data.result.result.files[0].url;
            $scope.digestScope();
            return data.result.result.files;
        }       
    };

    $scope.addinitUploadWidget();

    // edit category upload picture
    $scope.editinitUploadWidget = function(){
        $('#edit_cat_fileupload').fileupload({
            url: '/cms/picture/upload',
            prependFiles:"html",
            getFilesFromResponse:getThumb_pic_urlFromResponse,
            acceptFileTypes: /(\.|\/)(jpg|png)$/i,
            formData:function () {
                return [];
            }
        }).bind('fileuploadfinished',function(e,data){
                $(".uplpadimg").hide();
            })
            .addClass('fileupload-processing');

        function getThumb_pic_urlFromResponse(data){
            $scope.goodsList.edit.cate.pic_url  = data.result.result.files[0].url;
            $scope.digestScope();
            return data.result.result.files;
        }       
    };

    $scope.editinitUploadWidget();

    $scope.digestScope = function () {

        setTimeout(function() {
            try {
                $scope.$digest();
            }
            catch(e) {
                console.log(e);
            }
        });
    };
}]);