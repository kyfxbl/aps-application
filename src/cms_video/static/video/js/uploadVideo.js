app.controller('VideoUploadCtrl', [
        '$scope', '$http','$location',
        function ($scope, $http,$location) {
            $scope.video = {
               name:""
            };

            $('#video_fileupload').fileupload({
                url: '/cms/video/upload',
                prependFiles:"html",
                getFilesFromResponse:getFilesFromResponse,
                acceptFileTypes: /(\.|\/)(mp4)$/i,
                formData:function () {
                    return [{name:"name",value:$scope.video.name}];
                }
            })
                .bind('fileuploadfinished',function(e,data){
                    window.location.hash = "/app/video_list";
                })
                .addClass('fileupload-processing');
            function getFilesFromResponse(data){
                return data.result.result.files;
            }

        }
    ]);