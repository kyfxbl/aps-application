app.controller('VideoCtrl', ['$rootScope', '$scope', '$http','$location', function ($rootScope, $scope, $http,$location) {

    function init() {
        _cleanMsg();
        $scope.getPagedDataAsync( $scope.pagingOptions.currentPage,$scope.pagingOptions.pageSize);
    }
    function _cleanMsg() {
        $scope.viewMsg = {errorMg: null, successMsg: null};
    }

    $scope.pagingOptions = {
        pageSizes: [20, 50, 100],
        pageSize: 20,
        currentPage: 1,
        totalServerItems: 0
    };

    $scope.filterOptions = {
        filterText: "",
        traceStatus: "",
        useExternalFilter: true
    };


    $scope.setPagingData = function( page, pageSize,filterOptions){
        //查询服务器接口
        var serverurl = "/cms/video/videoList";
        var url = serverurl+"?p="+page+"&s="+pageSize;
        url = encodeURI(url);
        $http.get(url)
            .success(function(data){
                $scope.totalServerItems = 5;
                $scope.pagingOptions.totalServerItems = 5;
                $scope.tableData = data.result.pageData;
                $scope.totalServerItems = data.result.total;
                $scope.pagingOptions.totalServerItems = data.result.total;
                if (!$scope.$$phase) {
                    $scope.$apply();
                }
            })
            .error(function(error){
                //提示数据初始化错误
            });
    };
    $scope.getPagedDataAsync = function (page,pageSize,  filterOptions) {
        $scope.setPagingData(page,pageSize,filterOptions);
    };
    $scope.openDeleteVideo = function(data){
        $scope.video = data;
        $scope.video.size_format = (data.size / (1024 * 1024)).toFixed(2) + "M";
        $scope.video.upload_date_format = getLocalTime(data.upload_date);
    }

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
            $scope.getPagedDataAsync( $scope.pagingOptions.currentPage,$scope.pagingOptions.pageSize, $scope.filterOptions);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync(1,$scope.pagingOptions.pageSize, $scope.filterOptions);
        }
    }, true);


    $scope.deleteVideo = function(){
        var serverurl = "/cms/video/" + $scope.video.id;
        $http.delete(serverurl)
            .success(function(data){
                $scope.tableData = _.filter($scope.tableData, function(item){
                    return item.id != $scope.video.id;
                });
                $scope.totalServerItems--;
                $(".delete-video").modal('hide');
            })
            .error(function(error){
                //提示数据初始化错误
            });
    }

    var operation = ' <span class="label bg-danger" data-toggle="modal" data-target=".delete-video" ng-click="openDeleteVideo(row.entity)">删除</span></div>';

    $scope.columnDefs = [
        {field: 'name', displayName: '名称' },
        {field: 'size', displayName: '视频大小',width: '100px',cellTemplate:"<div>{{(row.entity.size/(1024*1024)).toFixed(2)}}M</div>"},
        {field: 'oss_url', displayName: '视频地址',width: '500px'},
        {field: 'upload_date', displayName: '上传时间',cellTemplate:"<div>{{row.entity.upload_date | date:'yyyy-MM-dd HH:mm:ss'}}</div>"},
        {field: '', displayName: '操作', cellTemplate: operation}
    ];
    $scope.gridOptions = {
        data: 'tableData',
        columnDefs: $scope.columnDefs,
        enablePaging: true,
        showFooter: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        multiSelect: false,
        i18n: 'zh_cn'
    };
    window.ngGrid.i18n['zh_cn'] = yilos_i18n.resource;
    init();

    function getLocalTime(timeStr) {
        var d = new Date(timeStr);

        return d.getFullYear()+"年"+((d.getMonth()+1)<10?"0"+(d.getMonth()+1):(d.getMonth()+1))+"月"+(d.getDate()<10?"0"+d.getDate():d.getDate())+
            "日 "+(d.getHours()<10?"0"+d.getHours():d.getHours())+":"+(d.getMinutes()<10?"0"+d.getMinutes():d.getMinutes())+":"+(d.getSeconds()<10?"0"+d.getSeconds():d.getSeconds());
    }
}]);