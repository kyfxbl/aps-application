app.controller('JiaoChengCtrl', ['$rootScope', '$scope', '$http','$location','$state', function ($rootScope, $scope, $http,$location, $state) {

    var page = 1;
    var pageCount = 0;

    init(page);
    function init(page){
        $http.get('/cms/qjc/getHistoryActivities?page='+page).success(function(data){
            $scope.activities = data.result.history[0];
            $scope.count = data.result.count[0].count;
            var countPage = Math.ceil($scope.count/20);
            pageCount = countPage;
            doPageNumber(countPage);
        });
    }
    function doPageNumber(counts){
        $scope.pages = [];
        for(var i=1;i<=counts;i++){
            $scope.pages.push(i);
        }
    }

    $scope.doPage = function(pageNumber){
        page = pageNumber;
        init(page);
    };

    $scope.prePage = function(){
        if(page > 1){
            --page;
            init(page);
        }
    };

    $scope.nextPage = function(){
        if(page < pageCount){
            ++page;
            init(page);
        }
    };

    $scope.addActivities = function(){
        if(timeLimit($scope.upload_end_time,$scope.make_end_time,$scope.talk_end_time)){
            var postData = {
                begin_time: transdateDate($scope.begin_time),
                upload_end_time: transdateDate($scope.upload_end_time),
                make_end_time: transdateDate($scope.make_end_time)
            };
            $http.post('/cms/qjc/addActivities', postData).success(function(data){

                init(1);
                $(".add-jiaocheng").modal('hide');
            });
        }else{
            alert("时间应依次增大");
        }

    };

    $scope.openDelActivities = function(activity){
        $scope.activity = activity;
    };
    $scope.deleteActivities = function(){
        var postData = {
            id: $scope.activity.id
        };

        $http.post('/cms/qjc/deleteActivities', postData).success(function(data){

            init(1);
            $(".del-jiaocheng").modal('hide');
        });
    };


    $scope.goJcDetail = function(activity) {
        $state.go('app.jcDetail', {
            id: activity.id
        });
    };

    $scope.editActivities = function(activity){
        $scope.editActivity = activity;
        var now_time = Date.parse(new Date());
        $scope.new_begin_time = getLocalTime(activity.begin_time);
        $scope.new_upload_end_time = getLocalTime(activity.upload_end_time);
        $scope.new_make_end_time = getLocalTime(activity.make_end_time);
        if(now_time>=activity.upload_end_time){
            $http.get('/cms/qjc/queryQjcTopics').success(function(data){
                $scope.topics = data.result;
                /*
                $scope.selectedTopic= _.find($scope.topics, function (item) {
                    return item.id === $scope.editActivity.topic_id;
                });
                */
                for(var i=0; i<$scope.topics.length; i++){
                    var item = $scope.topics[i];
                    if(item.id === $scope.editActivity.topic_id){
                        $scope.selectedTopic = item;
                        break;
                    }
                }
            });
        }
        if(now_time<activity.begin_time){
            $scope.jieduan = "活动未开始";
        }else if(now_time>=activity.begin_time&&now_time<activity.upload_end_time){
            $scope.jieduan = "投票阶段";
        }else if(now_time>=activity.upload_end_time&&now_time<=activity.make_end_time){
            $scope.jieduan = "视频制作阶段";
        }else{
            $scope.jieduan = "本期活动已结束";
        }
     };
    $scope.commitEditActivities = function(){
        if(timeLimit(transdateDate($scope.new_upload_end_time), transdateDate($scope.new_make_end_time))){
            var postData = {
                begin_time: transdateDate($scope.new_begin_time),
                upload_end_time: transdateDate($scope.new_upload_end_time),
                make_end_time: transdateDate($scope.new_make_end_time),
                id:$scope.editActivity.id,
                topic_id: $scope.selectedTopic?$scope.selectedTopic.id:''
            };
            $http.post('/cms/qjc/updateActivities', postData).success(function(data){

                init(page);
                $(".edit-jiaocheng").modal('hide');
            });
        }else{
            alert("时间应依次增大");
        }
    };

    function timeLimit(time1, time2){
        return (time2 && time2 > time1);
    }

    function transdateDate(time){
        var date = new Date();
        date.setFullYear(time.substring(0,4));
        date.setMonth(time.substring(5,7)-1);
        date.setDate(time.substring(8,10));
        date.setHours(time.substring(12,14));
        date.setMinutes(time.substring(15,17));
        date.setSeconds(time.substring(18,20));
        return Date.parse(date);
    }
    function getLocalTime(timeStr) {
        var d = new Date(timeStr);

        return d.getFullYear()+"年"+((d.getMonth()+1)<10?"0"+(d.getMonth()+1):(d.getMonth()+1))+"月"+(d.getDate()<10?"0"+d.getDate():d.getDate())+
            "日 "+(d.getHours()<10?"0"+d.getHours():d.getHours())+":"+(d.getMinutes()<10?"0"+d.getMinutes():d.getMinutes())+":"+(d.getSeconds()<10?"0"+d.getSeconds():d.getSeconds());
    }
}]);