app.controller('jcDetail', ['$rootScope', '$scope', '$http','$location','$stateParams','$state', function ($rootScope, $scope, $http,$location,$stateParams,$state) {

    $scope.filterOptions = {
        filterText: "",
        useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [10, 20, 50],
        pageSize: 10,
        currentPage: 1
    };

    $scope.goBackJiaoCheng = function(){
        $state.go('app.jiaocheng', {});
    }

    $scope.setPagingData = function(data){
        for(var i=0;i<data.length;i++){
            switch(data[i].status){
                case 0:
                    data[i].statusName = "被删除";
                    break;
                case 1:
                    data[i].statusName = "正常";
                    break;
                case 2:
                    data[i].statusName = "被举报";
                    break;
                default:
                    data[i].statusName = "状态数据异常";
                    break;
            }
        }
        $scope.myData = data;
    };
    $scope.getPagedDataAsync = function (perPage, page) {
        var url = "/cms/qjc/getCandidatesByActivityId?id=" + $stateParams.id + "&page=" + page + "&perPage=" + perPage;
        $http.get(url).success(function (data) {
            $scope.setPagingData(data.result);
        });
    };
    $scope.getPageDataCountAsync = function () {
        var url = "/cms/qjc/getCandidatesCountByActivityId?id=" + $stateParams.id;
        $http.get(url).success(function(data){
            if(data.code == 0){
                $scope.totalServerItems = data.result.count;
            }
        });
    }
    $scope.getActivityTime = function(){
        var url = "/cms/qjc/getActivityTime?id=" + $stateParams.id;
        $http.get(url).success(function(data){
            if(data.code == 0){
                $scope.activityStage = data.result;
                $scope.activityId = $stateParams.id;
            }
        });
    }
    $scope.initAuthorAccounts = function(){
        var url = "/cms/topic/getAuthorAccounts";
        $http.get(url).success(function (data){
            if(data.code != 0){
                alert("查询用户接口调用失败");
                return;
            }
            $scope.accounts = data.result.authors;
        });
    }
    $scope.deleteCandidate = function(){
        var url = "/cms/qjc/deleteCandidateById/" + $scope.candidate.id;
        $http.post(url).success(function(data){
            if(data.code == 0){
                $scope.myData = _.filter($scope.myData, function(item){
                    return item.id != $scope.candidate.id;
                });
                $scope.totalServerItems--;
                $(".delete-candidate").modal('hide');
            }else{
                alert("删除失败，请联系管理员");
            }
        });
    };
    $scope.deleteReportCandidate = function(id){
        var url = "/cms/qjc/deleteReportCandidate/" + id;
        $http.post(url).success(function(data){
            if(data.code == 0){
                $scope.myData = _.filter($scope.myData, function(item){
                    if(item.id == id){
                        item.status = 1;
                        item.statusName = "正常";
                    }
                    return true;
                });
            }else{
                alert("删除失败，请联系管理员");
            }
        });
    }
    $scope.editCandidate = function(){
        var url = "/cms/qjc/editCandidateById/";
        $http.post(url, $scope.candidate).success(function(data){
            $(".edit-candidate").modal('hide');
            if(data.code != 0){
                alert("删除失败，请联系管理员");
            }
        });
    }
    $scope.addCandidate = function(){
        var url = "/cms/qjc/postCandidate/" + $stateParams.id;
        $http.post(url, $scope.formData).success(function(data){
            $(".add-candidate").modal('hide');
            if(data.code != 0){
                return alert("增加失败，请联系管理员");
            }
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        });
    }
    $scope.openDeleteCandidate = function(candidate){
        $scope.candidate = candidate;
    };
    $scope.openEditCandidate = function(candidate){
        $scope.candidate = candidate;
    };

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage && newVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    var operation = '<div style="line-height:120px"><span class="label bg-success" data-toggle="modal" data-target=".edit-candidate" ng-click="openEditCandidate(row.entity)">编辑</span>';
    operation += ' <span class="label bg-danger" data-toggle="modal" data-target=".delete-candidate" ng-click="openDeleteCandidate(row.entity)">删除</span></div>';

    var statusEdit = "<div style='line-height:120px'>{{row.entity.statusName}}</div>";
    $scope.columnDefs = [
        {field: 'picUrl', displayName: '求教程图片' ,cellTemplate: "<img ng-src='{{row.entity.picUrl}}' style='height:100px;margin-top:10px;margin-left:15px;margin-right:auto'/>"},
        {field: 'username', displayName: '用户名',cellTemplate:"<div style='line-height:120px'>{{row.entity.username}}</div>"},
        {field: 'nickname', displayName: '用户昵称',cellTemplate:"<div style='line-height:120px'>{{row.entity.nickname}}</div>"},
        {field: 'createDate', displayName: '上传日期',cellTemplate:"<div style='line-height:120px'>{{row.entity.createDate | date:'yyyy-MM-dd HH:mm:ss'}}</div>"},
        {field: 'voteNum', displayName: '投票数',cellTemplate:"<div style='line-height:120px'>{{row.entity.voteNum}}</div>"},
        {field: 'statusName', displayName: '状态',cellTemplate:"<div style='line-height:120px;'>{{row.entity.statusName}}<span style='display: inline-block;margin-left:15px;' class='label bg-info' ng-show='row.entity.status==2' ng-click='deleteReportCandidate(row.entity.id)'>取消举报</span></div>"},
        {field: '', displayName: '操作', cellTemplate: operation}
    ];
    $scope.gridOptions = {
        data: 'myData',
        columnDefs: $scope.columnDefs,
        enablePaging: true,
        showFooter: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        rowHeight: '120',
        multiSelect: false,
        i18n: 'zh_cn'
    };
    window.ngGrid.i18n['zh_cn'] = yilos_i18n.resource;

    //初始操作
    function init(){
        //获取活动所处阶段
        $scope.getActivityTime();
        //获取数据总数
        $scope.getPageDataCountAsync();
        //获取当页数据
        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        //查询用户
        $scope.initAuthorAccounts();
        $scope.formData = {};
        $('.candidate_pic_fileupload').fileupload({
            url: '/cms/picture/upload',
            prependFiles:"html",
            getFilesFromResponse:getThumb_pic_urlFromResponse,
            acceptFileTypes: /(\.|\/)(jpg|png)$/i,
            formData:function () {
                return [];
            }
        }).bind('fileuploadfinished',function(e,data){
        }).addClass('fileupload-processing');

        function getThumb_pic_urlFromResponse(data){
            $scope.formData.pic_url =  data.result.result.files[0].url;
            console.log($scope.formData.thumb_pic_url);
            return data.result.result.files;
        }

    }

    init();


}]);