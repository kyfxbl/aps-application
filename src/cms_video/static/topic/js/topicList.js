app.controller('TopicListCtrl', ['$rootScope', '$scope', '$http','$location','$state', function ($rootScope, $scope, $http,$location,$state) {
    var cateListUrl = "/cms/indexData";
    $scope.colors = ['primary', 'info', 'success', 'warning', 'danger', 'dark'];


    setTimeout(init,10);

    function init(){
        var i=1;
        $scope.imageSteps = [
            {
                pic_id:"",
                content:"",
                pic_url:""
            }
        ];
        $scope.formData = {
            video_id:"",
            category_ids:[],
            account_id:"",//作者ID
            thumb_pic_url:"",//缩略图
            big_thumb_pic_url:"",
            title:"",//标题
            serialNumber:0,
            tag_ids:[],
            pic_ids:[],
            articles:[],
            goods:[],  
            activity_url:"",
            type:"activity"
        };
        $scope.viewData = {
            channelFormData:{
                name: "",
                serial_number: 1,
                pic_url: "",
                serial_type: "topic",
                home_menu_icon:'',
                need_show: 1
            },
            edittingChannel:{},
        };

        //商品模型
        $scope.goods = {
            show:{
                key:'',
                type: 0,        //0代表可以编辑（显示确定按钮），1代表不能编辑（显示编辑按钮），选择商品后默认0需要编辑
                list:'',        //搜索到的商品列表
                addgoods:{      
                    goods:'null'
                } 
            },
            add:{
                topic_id:'',
                goods_id:'',
            },
            edit:'',
            delete:''
        }

        $scope.initTopicCateList();
        $scope.initVideoList();
        $scope.initAuthorAccounts();
        $scope.initTopicTags();
        $scope.initUploadWidget();
        $scope.pic_urls = '';
    }

    $scope.digestScope = function () {
        setTimeout(function() {
            try {
                $scope.$digest();
            }
            catch(e) {
                console.log(e);
            }
        });
    };

    $scope.initTopicCateList = function(callback){
        $http.get(cateListUrl).success(function (data){
            if(data.code!=0){
                alert("调用失败");
                if(callback){
                    callback(null);
                }
                return;
            }
            $scope.cates = data.result.channels;
            angular.forEach($scope.cates, function(cate){
                cate.color = $scope.colors[Math.floor((Math.random()*6))];
            });
            $scope.selectCate($scope.cates[0]);
            if(callback){
                callback(null);
            }
        });
    };

    $scope.selectCate = function(cate){
        angular.forEach($scope.cates, function(cate) {
            cate.selected = false;
        });
        $scope.cate = cate;
        $scope.cate.selected = true;
        $scope.selectedCate = cate;
        $scope.formData.category_id = cate.categoryId;
        $scope.pagingOptions.pageSize = 10;
        $scope.pagingOptions.currentPage = 1;
        $scope.getPagedDataAsync($scope.pagingOptions.pageSize,$scope.pagingOptions.currentPage);
    };

    $scope.filterOptions = {
        filterText: "",
        useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [10],
        pageSize: 10,
        currentPage: 1
    };

    $scope.setPagingData = function(data, page, pageSize){
        $scope.myData = data.pageData;
        $scope.totalServerItems = data.total;

        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };

    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
        setTimeout(function () {
            var data;
            if (searchText) {
                var ft = searchText.toLowerCase();
                $http.get('/cms/topic/listTopic?s='+ $scope.pagingOptions.pageSize+'&p='+ page +'&_search_category_id='+$scope.cate.categoryId).success(function (data) {
                    $scope.setPagingData(data.result,page,pageSize);
                });
            } else {

                var url = '/cms/topic/listTopic?s='+ $scope.pagingOptions.pageSize+'&p='+ page +'&_search_category_id='+$scope.cate.categoryId;
                $http.get(url).success(function (data) {
                    $scope.setPagingData(data.result,page,pageSize);
                });
            }
        }, 100);
    };

    $scope.goCommentDetail = function(activity) {
        $state.go('app.commentDetail', {
            id: activity.id
        });
    };

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    var operation = '<span class="label bg-success" data-toggle="modal" data-target=".updata-topic" ng-click="getTopicData(row.entity)" ng-model="topicData">编辑</span>' +
        ' <span class="label bg-info" ng-click="goCommentDetail(row.entity)" >查看评论</span>';

    $scope.columnDefs = [
        {field: 'title', displayName: '主题名称'},
        {field: 'account.nickname', displayName: '主题作者'},
        {field: 'serialNumber', displayName: '序号'},
        {field: 'create_date', displayName: '创建时间',cellTemplate: '<span>{{row.entity.create_date |date:"yyyy-MM-dd hh:mm"}}</span>'},
        {field: '', displayName: '操作', cellTemplate: operation}
    ];

    $scope.gridOptions = {
        data: 'myData',
        columnDefs: $scope.columnDefs,
        enablePaging: true,
        showFooter: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        rowHeight:50,
        multiSelect: false,
        i18n: 'zh_cn'
    };

    window.ngGrid.i18n['zh_cn'] = yilos_i18n.resource;

    $scope.addStep = function(){
        $scope.imageSteps.push(
            {
                pic_id:"",
                content:"",
                pic_url:""
            }
        );
    };

    $scope.addGoodsBtn = function(){
        $scope.goodsSteps.push(
            {
                goods_id:'',
                description:'',
                serial_number:'',
                pic_url:'',
                name:''
            }
        );
    };

    $scope.tab = {
        selected:"video"
    };

    $scope.switchTab =function(current){
        if(current!=$scope.tab.selected){
            $scope.tab.selected = current;
        }
    };

    $scope.initFileoptions =  function(step){
     return {
        url: '/cms/picture/upload',
        prependFiles:"html",
        getFilesFromResponse:function(data){
            step.pic_id  =  data.result.result.files[0].id;
            step.pic_url = data.result.result.files[0].url;
            $scope.digestScope();
            return data.result.result.files;
        },
        acceptFileTypes: /(\.|\/)(jpg|png)$/i,
        formData:function () {
            return [];
        }
     }
    };

    $scope.initVideoList = function(){
        var videoUrl = "/cms/video/videoList?p=1&s=1000";
        $http.get(videoUrl).success(function (data){
            if(data.code!=0){
                alert("调用失败");
                return;
            }
            var i = 0;
            $scope.videos = data.result.pageData;
            angular.forEach($scope.videos, function(video){
                video.name = video.name!=""?video.name:"未命名视频"+(++i);
            });
        });
    };

    $scope.initTopicTags = function(){
        var url = "/cms/topic/queryTags"; 
        $http.get(url).success(function (data){
            if(data.code!=0){
                alert("调用失败");
                return;
            }
            var i = 0;
            $scope.tags = data.result.tags;
        });
    }

    $scope.initAuthorAccounts = function(){

        var url = "/cms/topic/getAuthorAccounts";
        $http.get(url).success(function (data){
            if(data.code!=0){
                alert("调用失败");
                return;
            }
            var i = 0;
            $scope.authors = data.result.authors;
        });
    };

    $scope.toggleTagSelection = function(tagId){
        var idx = $scope.formData.tag_ids.indexOf(tagId);
        // is currently selected
        if (idx > -1) {
            $scope.formData.tag_ids.splice(idx, 1);
        }
        // is newly selected
        else {
            $scope.formData.tag_ids.push(tagId);
        }
    };

    $scope.toggleCateSelection = function(id){
        var idx = $scope.formData.category_ids.indexOf(id);
        // is currently selected
        if (idx > -1) {
            $scope.formData.category_ids.splice(idx, 1);
        }
        // is newly selected
        else {
            $scope.formData.category_ids.push(id);
        }
    };

    $scope.initUploadWidget = function(){
        $('.topic_Thumb_pic_fileupload').fileupload({
            url: '/cms/picture/upload',
            prependFiles:"html",
            getFilesFromResponse:getThumb_pic_urlFromResponse,
            acceptFileTypes: /(\.|\/)(jpg|png)$/i,
            formData:function () {
                return [];
            }
        }).bind('fileuploadfinished',function(e,data){
                $(".uplpadimg").hide();
            })
            .addClass('fileupload-processing');

        function getThumb_pic_urlFromResponse(data){
            $scope.formData.thumb_pic_url =  data.result.result.files[0].url;
            $scope.digestScope();
            return data.result.result.files;
        }

        $('.big_topic_Thumb_pic_fileupload').fileupload({
            url: '/cms/picture/upload',
            prependFiles:"html",
            getFilesFromResponse:getBigThumb_pic_urlFromResponse,
            acceptFileTypes: /(\.|\/)(jpg|png)$/i,
            formData:function () {
                return [];
            }
        }).bind('fileuploadfinished',function(e,data){
            $(".uplpadimg").hide();
        })
            .addClass('fileupload-processing');

        function getBigThumb_pic_urlFromResponse(data){
            $scope.formData.big_thumb_pic_url =  data.result.result.files[0].url;
            $scope.digestScope();
            return data.result.result.files;
        }
    };

    $scope.showAddDialog = function(type){
        if(type === 'topic'){
            $scope.imageSteps = [
                {
                    pic_id:"",
                    content:"",
                    pic_url:""
                }
            ];
            $scope.goodsSteps = [];
            $scope.formData = {
                video_id:"",
                category_ids:[],
                account_id:"",//作者ID
                thumb_pic_url:"",//缩略图
                big_thumb_pic_url:"",
                title:"",//标题
                serialNumber:0,
                tag_ids:[],
                pic_ids:[],
                articles:[],
                goods:[],
                type:"video"
            };
            $scope.tab = {
                selected:"video"
            };
        }else{
            $scope.formData = {
                category_ids:[],
                thumb_pic_url:"",//缩略图
                big_thumb_pic_url:"",
                title:"",//标题
                activity_url:"",
                type:"activity"

            };
            $scope.tab = {
                selected:"video"
            };
        }
        $scope.formData.category_ids.push($scope.selectedCate.categoryId);
    };

    //添加主题
    $scope.addTopicCommit  = function(type){
        // $scope.formData.thumb_pic_url
        if($scope.formData.type === "video"){
            var account_id = $scope.formData.account_id;
            if(account_id == null || account_id == undefined || account_id == ""){
                return alert("请选择作者姓名");
            }

            if($scope.formData.category_ids.length <= 0){
                return alert("请选择分类");
            }

            for(var i=0;i<$scope.imageSteps.length;i++) {
                var step =  $scope.imageSteps[i];
                if(step.pic_id!="" || step.content!=""){
                    $scope.formData.pic_ids.push(step.pic_id);
                    $scope.formData.articles.push(step.content);
                }
            }
            for(var i=0;i<$scope.goodsSteps.length;i++) {
                var step =  $scope.goodsSteps[i];
                if( step.goods_id !=''){
                    $scope.formData.goods.push(step);
                }
            }
        }
        else{
            if($scope.formData.title === ""){
                return alert("请填写活动名称");
            }
        }


        var serverurl = "/cms/topic/addTopic";
        $http.post(serverurl,$scope.formData)
            .success(function(data) {
                $("#add-topic").modal('hide');
                $("#add-activity").modal('hide');
                $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
            }).error(function(error) {      
                alert("error");
            });
    };
     //获取所有主题信息
    $scope.getTopicData = function(entity) {
        var topicId = entity.id;
        $scope.formData.serialNum = entity.serialNumber;
        $scope.imageSteps = [
            {
                pic_id:"",
                content:"",
                pic_url:""
            }
        ];
        $scope.goodsSteps = [];
        var url = "/cms/topics/"+topicId;
        $http.get(url).success(function (data) {
            if(data.code!=0){
                return;
            } 
            $scope.topicData = data.result;
            //获取海报
            $scope.formData.thumb_pic_url = data.result.thumbPicUrl;
            $scope.formData.big_thumb_pic_url = data.result.bigThumbPicUrl;
            //选择视频
            $scope.formData.video = _.find($scope.videos, function (item) {
                return item.id === data.result.videos[0].videoId;
            });
            //主题姓名
            $scope.formData.title = data.result.title;
            //绑定作者
            $scope.formData.account = _.find($scope.authors, function (item) {
                return item.id === data.result.authorId;
            });

            //勾选标签
            $scope.formData.tag_ids = _.pluck(data.result.tags, 'id');
            //所属分类
            $scope.formData.category_ids = _.pluck(data.result.category_ids, 'id');
            //文章和图片

            //处理imageSteps模型的长度
            for (var i = 0; i < data.result.articles.length; i++) {
                if (i>0) {
                    $scope.imageSteps.push(
                        {
                            pic_id:"",
                            content:"",
                            pic_url:""
                        }
                    );
                }
                $scope.imageSteps[i].pic_id = data.result.pictures[i].id;
                $scope.imageSteps[i].content = data.result.articles[i];
                $scope.imageSteps[i].pic_url = data.result.pictures[i].url;
            }

            //处理goodsSteps模型的长度
            for (var i = 0; i < data.result.goods.length; i++) {
                $scope.goodsSteps.push(
                    {
                        goods_id:'',
                        description:'',
                        serial_number:'',
                        pic_url:'',
                        name:''
                    }
                );
                
                $scope.goodsSteps[i].goods_id = data.result.goods[i].goods_id;
                $scope.goodsSteps[i].description = data.result.goods[i].description;
                $scope.goodsSteps[i].serial_number = data.result.goods[i].serial_number+1;
                $scope.goodsSteps[i].pic_url = data.result.goods[i].pic_url;
                $scope.goodsSteps[i].name = data.result.goods[i].name;
            }
            //文章和图片
        }); 
    };

    $scope.removeImageStep = function(index){
        $scope.imageSteps.splice(index, 1);
    };

    //更新主题
    $scope.updataTopicData =function (topicId) {

        $scope.formData.pic_ids = [];
        $scope.formData.articles = [];
        $scope.formData.serialNum = $scope.formData.serialNum?$scope.formData.serialNum:0;

        $scope.formData.goods = [];

        // 处理图文
        for(var i=0;i<$scope.imageSteps.length;i++) {
            var step =  $scope.imageSteps[i];
            if(step.pic_id!="" || step.content!=""){
                $scope.formData.pic_ids.push(step.pic_id);
                $scope.formData.articles.push(step.content);
            }
        }

        for(var i=0;i<$scope.goodsSteps.length;i++) {
            var step =  $scope.goodsSteps[i];
            if(step.goods_id !=''){
                $scope.formData.goods.push(step);
            }
        }


        $scope.formData.video_id = $scope.formData.video.id;
        $scope.formData.account_id = $scope.formData.account.id;
        var url = "/cms/updatetopic/"+topicId;
        $http.post(url,$scope.formData).success(function (data) {
               $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
            }).error(function(error) {      
                alert("error");
            });
    };

    //删除主题
    $scope.deleteTopicData = function (data) {
       $("#deleteTopic").text("确定删除"+data.title+"吗？");
       $("#deleteTopic").attr('data-topicId', data.id);
    };

    $scope.deleteTopic = function () {
        var topicId = $("#deleteTopic").attr("data-topicId");
        var serverurl = "/cms/index/deleteTopic/"+topicId+"?cateId="+$scope.cate.categoryId;
        $http.delete(serverurl)
            .success(function () {
               $scope.getPagedDataAsync();
            })
            .error(function(error) {
               //提示数据初始化错误
            });
    };

    //新增系列初始化
    $scope.addCate = function () {
        setTimeout(function () {
            $(".uplpadimg").hide();
        },100);
        $scope.viewData.channelFormData = {
            name: "",
            serial_number: 1,
            pic_url: "",
            serial_type: "topic",
            home_menu_icon:'',
            need_show: 1
        };
        initCate_pic_fileupload("addCate");
        initCate_newpic_fileupload("addCate");
    }

    //新增系列是否显示
    $scope.showneed = function () {
        if( $scope.viewData.channelFormData.need_show == 1){
            $scope.viewData.channelFormData.need_show = 0;
        }else{
             $scope.viewData.channelFormData.need_show = 1;
        } 
    }

     //新增系列
    $scope.addChannel = function(channelId){
        var url = "/cms/index/addTopicCate";
        $http.post(url,$scope.viewData.channelFormData).success(function(data){
            $scope.cates.push({
                categoryId:data.result.id,
                categoryName:data.result.name,
                serial_number:data.result.serial_number,
                pic_url:data.result.pic_url,
                home_menu_icon:data.result.home_menu_icon,
                need_show:data.result.need_show,
                topTopics:[
                ]
            });
        }).error(function(error){
            //提示数据初始化错误
        });
    };

    //获得系列名称
    $scope.initEditChannel = function(channel) {
        $scope.edittingChannel = channel;
        initCate_pic_fileupload("updateCate");
        initCate_newpic_fileupload("updateCate");
    };

    //编辑系列是否显示
    $scope.editshowneed = function () {
        if( $scope.edittingChannel.need_show == 1){
            $scope.edittingChannel.need_show = 0;
        }else{
            $scope.edittingChannel.need_show = 1;
        }
        
    }

    //修改系列
    $scope.renameChannel = function() {
        var url = "/cms/index/updateTopicCate";
        $http.post(url,{
            id:$scope.edittingChannel.categoryId,
            name:$scope.edittingChannel.categoryName,
            serial_number:$scope.edittingChannel.serial_number,
            pic_url:$scope.edittingChannel.pic_url,
            home_menu_icon:$scope.edittingChannel.home_menu_icon,
            need_show:$scope.edittingChannel.need_show

        }).success(function(data){
            $scope.initTopicCateList();
        }).error(function(error){

        });
    };

    //点击删除系列按钮
    $scope.deleteIndexChannelData = function (channel) {
       $("#deleteCategory").text("确定删除"+channel.categoryName+"吗？");
       $("#deleteCategory").attr('data-categoryId', channel.categoryId);
    };
    //点击模态框确定删除系列
    $scope.deleteIndexChannel = function(){
        var id = $("#deleteCategory").attr("data-categoryId");
        var serverurl = "/cms/index/deleteTopicCate/"+id;
        $http.delete(serverurl)
            .success(function(data){
                $scope.initTopicCateList();
            })
            .error(function(error){
                //提示数据初始化错误
            });
    };

    function initCate_pic_fileupload(optionType){

        $('.cate_pic_fileupload').fileupload({
            url: '/cms/picture/upload',
            prependFiles:"html",
            getFilesFromResponse:getFilesFromResponse,
            acceptFileTypes: /(\.|\/)(jpg|png)$/i,
            formData:function () {
                return [];
            }
        })
            .bind('fileuploadfinished',function(e,data){
                $(".uplpadimg").hide();
            })
            .addClass('fileupload-processing');

        function getFilesFromResponse(data){
            if(optionType === "addCate"){
                $scope.viewData.channelFormData.pic_url = data.result.result.files[0].url;
                console.log('addoic!!!'+$scope.viewData.channelFormData.pic_url);
            }else{
                $scope.edittingChannel.pic_url = data.result.result.files[0].url;
                console.log('editoic!!!'+$scope.viewData.channelFormData.pic_url);
            }
            $scope.digestScope();
            return data.result.result.files;
        }
    }

    function initCate_newpic_fileupload(optionType){

        $('.cate_newpic_fileupload').fileupload({
            url: '/cms/picture/upload',
            prependFiles:"html",
            getFilesFromResponse:getFilesFromResponse,
            acceptFileTypes: /(\.|\/)(jpg|png)$/i,
            formData:function () {
                return [];
            }
        })
            .bind('fileuploadfinished',function(e,data){
                $(".uplpadimg").hide();
            })
            .addClass('fileupload-processing');

        function getFilesFromResponse(data){
            if(optionType === "addCate"){
                $scope.viewData.channelFormData.home_menu_icon = data.result.result.files[0].url;
                console.log('addicon!!!'+$scope.viewData.channelFormData.pic_url);
            }else{
                $scope.edittingChannel.home_menu_icon = data.result.result.files[0].url;
                console.log('editicon!!!'+$scope.viewData.channelFormData.pic_url);
            }
            $scope.digestScope();
            return data.result.result.files;
        }
    }
    //时间模板
    Date.prototype.Format = function(fmt){ //author: meizz
        var o = {
            "M+" : this.getMonth()+1,                 //月份
            "d+" : this.getDate(),                    //日
            "h+" : this.getHours(),                   //小时
            "m+" : this.getMinutes(),                 //分
            "s+" : this.getSeconds(),                 //秒
            "q+" : Math.floor((this.getMonth()+3)/3), //季度
            "S"  : this.getMilliseconds()             //毫秒
        };
        if(/(y+)/.test(fmt))
            fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
        for(var k in o)
            if(new RegExp("("+ k +")").test(fmt))
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
        return fmt;
    }

    //添加主题选择搜索的商品数据
    $scope.addselectedGoods = function (data) {
        $scope.goodsSteps.push(
            {
                goods_id:'',
                description:'',
                serial_number:'',
                pic_url:'',
                name:''
            }
        );
        var index = $scope.goodsSteps.length-1;

        $scope.goodsSteps[index] = {
            goods_id:data.originalObject.id,
            description:data.originalObject.description,
            serial_number:index+1,
            pic_url:data.originalObject.pic_url,
            name:data.originalObject.name
        }
        
    }

    //编辑主题选择搜索的商品数据
    $scope.editselectedGoods = function (data) {
        $scope.goodsSteps.push(
            {
                goods_id:'',
                description:'',
                serial_number:'',
                pic_url:'',
                name:''
            }
        );
        var index = $scope.goodsSteps.length-1;

        $scope.goodsSteps[index] = {
            goods_id:data.originalObject.id,
            description:data.originalObject.description,
            serial_number:index+1,
            pic_url:data.originalObject.pic_url,
            name:data.originalObject.name
        }
    }

    //搜索商品传参数
    $scope.remoteUrlRequestFn = function(str) {
        return {key: str};
    };

    //删除商品
    $scope.deleteGoods = function (index) {
        $scope.goodsSteps.splice(index, 1);
    }

}]);
