app.controller('replyDetail', ['$rootScope', '$scope', '$http','$location','$stateParams','$state', function ($rootScope, $scope, $http,$location,$stateParams,$state) {

    $scope.filterOptions = {
        filterText: "",
        useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [20, 50, 100],
        pageSize: 20,
        currentPage: 1
    };

    $scope.goBackCommentDetail = function(){
        history.back();
    }

    $scope.setPagingData = function(data){
        $scope.myData = data;
    };
    $scope.getPagedDataAsync = function (perPage, page) {
        var url = "/cms/topic/getReplyByCommentId/" + $stateParams.id + "?page=" + page + "&perPage=" + perPage;
        $http.get(url).success(function (data) {
            $scope.setPagingData(data.result);
        });
    };
    $scope.getPageDataCountAsync = function () {
        var url = "/cms/topic/getReplyCountByCommentId/" + $stateParams.id;
        $http.get(url).success(function(data){
            if(data.code == 0){
                $scope.totalServerItems = data.result.count;
            }
        });
    }
    $scope.deleteReply = function(){
        var url = "/cms/topic/deleteReply/" + $scope.reply.id;
        $http.post(url).success(function(data){
            if(data.code == 0){
                $scope.myData = _.filter($scope.myData, function(item){
                    return item.id != $scope.reply.id;
                });
                $scope.totalServerItems--;
                $(".delete-reply").modal('hide');
            }else{
                //alert("删除失败，请联系管理员");
            }
        });
    };
    $scope.openDeleteReply = function(data){
        $scope.reply = data;
        $scope.reply.createDate_format = getLocalTime($scope.reply.createDate);
    };

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage && newVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    var operation = '<div style="line-height:120px"><span class="label bg-danger" data-toggle="modal" data-target=".delete-reply" ng-click="openDeleteReply(row.entity)">删除</span>';

    $scope.columnDefs = [
        {field: 'username', displayName: '用户名',cellTemplate:"<div style='margin-left:15px;line-height:120px'>{{row.entity.username}}</div>"},
        {field: 'nickname', displayName: '用户昵称',cellTemplate:"<div style='line-height:120px'>{{row.entity.nickname}}</div>"},
        {field: 'atNickname', displayName: '回复对象',cellTemplate:"<div style='line-height:120px'>{{row.entity.nickname}}</div>"},
        {field: 'content', displayName: '回复内容',cellTemplate:"<div style='line-height:120px'>{{row.entity.content}}</div>"},
        {field: 'createDate', displayName: '发表时间',cellTemplate:"<div style='line-height:120px'>{{row.entity.createDate | date:'yyyy-MM-dd HH:mm:ss'}}</div>"},
        {field: '', displayName: '操作', cellTemplate: operation}
    ];
    $scope.gridOptions = {
        data: 'myData',
        columnDefs: $scope.columnDefs,
        enablePaging: true,
        showFooter: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        rowHeight: '120',
        multiSelect: false,
        i18n: 'zh_cn'
    };
    window.ngGrid.i18n['zh_cn'] = yilos_i18n.resource;
    //初始操作
    function init(){
        //获取数据总数
        $scope.getPageDataCountAsync();
        //获取当页数据
        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    }

    init();

    function getLocalTime(timeStr) {
        var d = new Date(timeStr);

        return d.getFullYear()+"年"+((d.getMonth()+1)<10?"0"+(d.getMonth()+1):(d.getMonth()+1))+"月"+(d.getDate()<10?"0"+d.getDate():d.getDate())+
            "日 "+(d.getHours()<10?"0"+d.getHours():d.getHours())+":"+(d.getMinutes()<10?"0"+d.getMinutes():d.getMinutes())+":"+(d.getSeconds()<10?"0"+d.getSeconds():d.getSeconds());
    }

}]);