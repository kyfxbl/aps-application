app.controller('TagsListCtrl', ['$rootScope', '$scope', '$http','$location', function ($rootScope, $scope, $http,$location) {
    var cateListUrl = "/cms/indexData";
    setTimeout(init,10);
    function init(){

        $scope.tag = {
            name:''
        }
        $scope.getPagedDataAsync();
    }
    $scope.filterOptions = {
        filterText: "",
        useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [10, 20, 50],
        pageSize: 10,
        currentPage: 1
    };
    $scope.setPagingData = function(data){
        $scope.myData = data.result.tags;

        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.getPagedDataAsync = function (pageSize, page) {
        if(pageSize == undefined || isNaN(pageSize) || pageSize == null){
            pageSize = $scope.pagingOptions.pageSize;
        }
        if(page == undefined || isNaN(page) || page == null){
            page = $scope.pagingOptions.currentPage;
        }
        setTimeout(function () {
            var url = "/cms/topic/queryTags?pageSize=" + pageSize + "&page=" + page;
            $http.get(url).success(function (data) {
                $scope.tags = data.result.tags;
                $scope.setPagingData(data);
            });

            var url = "/cms/topic/countTags";
            $http.get(url).success(function (data) {
                $scope.totalServerItems = data.result.count;
            });
        }, 100);
    };
    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && (newVal.currentPage !== oldVal.currentPage || newVal.pageSize !== oldVal.pageSize)) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    var operation = '<span class="label bg-success" style="color:#fff;background-color: rgb(240, 80, 80);"data-toggle="modal" data-target=".deleteTag" ng-click="deleteTagData(row.entity.id,row.entity.name)">删除</span>';

    $scope.columnDefs = [
        {field: 'name', displayName: '标签名'},
        {field: '', displayName: '操作', cellTemplate: operation}
    ];
    $scope.gridOptions = {
        data: 'myData',
        columnDefs: $scope.columnDefs,
        enablePaging: true,
        showFooter: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        rowHeight:40,
        multiSelect: false
    };
    //创建标签
    $scope.addTopicTag = function () {
        var url ="/cms/topic/addTopicTag";
        var data = $("#addTopicTagInput input").val();
        $http.post(url, $scope.tag).success(function (data) {
            $scope.getPagedDataAsync();
        }).error(function(error) {
            
        });
    } 
    //点击删除按钮弹出提示框
    $scope.deleteTagData = function (id,name) {
        $("#deleteTag").attr('data-id', id);
        $("#deleteTag").text('确定删除'+name+'标签吗？');
    } 
    //删除标签
    $scope.deleteTag = function () {
        var tagId = $("#deleteTag").attr('data-id');
        var url ="/cms/topic/deleteTopicTag"
        $http.post(url,{id: tagId}).success(function (data) {
            $scope.getPagedDataAsync();
        })
    }
}]);