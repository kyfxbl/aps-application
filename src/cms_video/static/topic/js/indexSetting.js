app.controller('IndexSettingCtrl', ['$rootScope', '$scope', '$http','$location', function ($rootScope, $scope, $http,$location) {
    $scope.colors = ['primary', 'info', 'success', 'warning', 'danger', 'dark'];
    //初始化首页设置数据
    $scope.viewData = {
        posters:[],
        channels:[

        ],
        posterFormData:{
            serial_number:"",
            pic_url:"",
            topic_id:""
        },
        editPoster:{
            id:'',
            serial_number:'',
            pic_url:''
        },
        postFormSearchText:"",
        channelFormData:{
            name: "",
            serial_number: 1,
            pic_url: "",
            serial_type: "topic",
            home_menu_icon:'',
            need_show: 1
        },
        edittingChannel:{},
        selectedChannelId:"",
        selectedChannelTopics:[],
        selectedChannelTopTopics:[

        ]
    };
    //1、获取首页数据结构
    $scope.getIndexData = function(callback){
        var url = "/cms/indexData";
        $http.get(url).success(function(data){
            if(data.code!=0){
                alert("调用失败");
                return;
            }
            $scope.viewData.posters = data.result.posters;
            $scope.viewData.channels = data.result.channels;
            angular.forEach($scope.viewData.channels, function(channel){
                channel.color = $scope.colors[Math.floor((Math.random()*6))];
            });
            if ($scope.viewData.selectedChannelId != "") {
                var channel = _.find($scope.viewData.channels,function(channel){                 
                    return channel.categoryId == $scope.viewData.selectedChannelId;
                });
                $scope.selectChannel(channel);
            }else{
                $scope.selectChannel($scope.viewData.channels[0]);                
            }                        
        }).error(function(error){
                //提示数据初始化错误
            callback(error);
        });
    };

    //选择系列,出现系列内的主题
    $scope.selectChannel = function(channel){
        angular.forEach($scope.viewData.channels, function(channel) {
            channel.selected = false;
        });      
        $scope.viewData.selectedChannelTopTopics = channel.topTopics;                
        $scope.channel = channel;
        $scope.channel.selected = true;
        $scope.selectedChannel = channel;
        $scope.viewData.selectedChannelId = channel.categoryId;
        $scope.getPagedDataAsync  (10, 1);
        if(channel.topTopics.length == 0){
            $("#ifShowPoster").hide();
            return;
        }else{
            if (channel.topTopics[0].topicId == null) {
                $("#ifShowPoster").hide();
            }else{
                $("#ifShowPoster").show();
            }
        }
    };
    $scope.filterOptions = {
        filterText: "",
        useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [10],
        pageSize: 10,
        currentPage: 1
    };

    $scope.setPagingData = function(data, page, pageSize){
        $scope.myData = data.pageData;
        $scope.postertotalServerItems = data.total;
        setTimeout(function () {
            //获取已经置顶主题的id
            var channel = _.find($scope.viewData.channels,function(channel){                 
                return channel.categoryId == $scope.viewData.selectedChannelId;
            });
            topTopicsId = _.pluck(channel.topTopics,'topicId');
            if (topTopicsId.length != null) {
                //操作前先回到默认状态
                $(".deflut").css("background-color","#27c24c");
                $(".deflut").text("置顶");
                for (var i = 0; i < topTopicsId.length; i++){
                    $('#'+topTopicsId[i]).css("background-color","#f05050");
                    $('#'+topTopicsId[i]).text("已置顶");
                };
            };
        },150);
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
        setTimeout(function () {
            var data;
            if (searchText) {
                var ft = searchText.toLowerCase();
                $http.get('/cms/topic/listTopic?s='+ $scope.pagingOptions.pageSize+'&p='+ $scope.pagingOptions.currentPage+'&_search_category_id='+$scope.viewData.selectedChannelId).success(function (data) {
                    $scope.setPagingData(data.result,page,pageSize);
                });
            } else {

                var url = '/cms/topic/listTopic?s='+ $scope.pagingOptions.pageSize+'&p='+ $scope.pagingOptions.currentPage+'&_search_category_id='+$scope.viewData.selectedChannelId;
                $http.get(url).success(function (data) {
                    $scope.setPagingData(data.result,page,pageSize);
                });
            }
        }, 100);
    };


    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    var operation = ' <span class="label bg-success ng-scope ng-pristine ng-valid deflut" ng-click="topic2Top(row.entity)" id="{{row.entity.id}}">置顶</span>';
    $scope.columnDefs = [
        {field: 'title', displayName: '主题名称'},
        {field: 'account.nickname', displayName: '主题作者'},
        {field: 'create_date', displayName: '创建时间',cellTemplate: '<span>{{row.entity.create_date |date:"yyyy-MM-dd hh:mm"}}</span>'},
        {field: 'like', displayName: '点赞数'},
        {field: 'collect', displayName: '收藏数'},
        {field: '', displayName: '操作', cellTemplate: operation}
    ];
    $scope.gridOptions = {
        data: 'myData',
        columnDefs: $scope.columnDefs,
        enablePaging: true,
        showFooter: true,   
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        rowHeight:50,
        i18n: 'zh_cn',
        multiSelect: false
    };
    window.ngGrid.i18n['zh_cn'] = yilos_i18n.resource;

    //新增系列初始化
    $scope.addCate = function () {
        setTimeout(function () {
            $(".uplpadimg").hide();
        },100);
        $scope.viewData.channelFormData = {
            name: "",
            serial_number: 1,
            pic_url: "",
            serial_type: "topic",
            home_menu_icon:'',
            need_show: 1
        };
        initCate_pic_fileupload("addCate");
        initCate_newpic_fileupload("addCate");
    }

    //新增系列是否显示
    $scope.showneed = function () {
        if( $scope.viewData.channelFormData.need_show == 1){
            $scope.viewData.channelFormData.need_show = 0;
        }else{
             $scope.viewData.channelFormData.need_show = 1;
        }
        
    }

    //新增系列
    $scope.addChannel = function(channelId){
        var url = "/cms/index/addTopicCate";
        $http.post(url,$scope.viewData.channelFormData).success(function(data){
            $scope.viewData.channels.push({
                categoryId:data.result.id,
                categoryName:data.result.name,
                serial_number:data.result.serial_number,
                pic_url:data.result.pic_url,
                home_menu_icon:data.result.home_menu_icon,
                need_show:data.result.need_show,
                topTopics:[
                ]
            });
        }).error(function(error){
            //提示数据初始化错误
        });
    };
    //获得系列名称
    $scope.initEditChannel = function(channel) {
        $scope.edittingChannel = channel;
        initCate_pic_fileupload("updateCate");
        initCate_newpic_fileupload("updateCate");
    };

    //编辑系列是否显示
    $scope.editshowneed = function () {
        if( $scope.edittingChannel.need_show == 1){
            $scope.edittingChannel.need_show = 0;
        }else{
            $scope.edittingChannel.need_show = 1;
        }
        
    }

    //修改系列名称
    $scope.renameChannel = function() {
        var url = "/cms/index/updateTopicCate";
        $http.post(url,{
            id:$scope.edittingChannel.categoryId,
            name:$scope.edittingChannel.categoryName,
            serial_number:$scope.edittingChannel.serial_number,
            pic_url:$scope.edittingChannel.pic_url,
            home_menu_icon:$scope.edittingChannel.home_menu_icon,
            need_show:$scope.edittingChannel.need_show

        }).success(function(data){
            $scope.refreshIndexPage();
        }).error(function(error){

        });
    };

    //点击删除系列按钮
    $scope.deleteIndexChannelData = function (channel) {
       $("#deleteCategory").text("确定删除"+channel.categoryName+"吗？");
       $("#deleteCategory").attr('data-categoryId', channel.categoryId);
    };
    //点击模态框确定删除系列
    $scope.deleteIndexChannel = function(){
        var id = $("#deleteCategory").attr("data-categoryId");
        var serverurl = "/cms/index/deleteTopicCate/"+id;
        $http.delete(serverurl)
            .success(function(data){
                _.each($scope.viewData.channels,function(one,index){
                        if(id === one.categoryId){
                           $scope.viewData.channels.splice(index,1);     
                        }
                })
            })
            .error(function(error){
                //提示数据初始化错误
            });
    };
    //主题置顶
    $scope.topic2Top = function(topic){
        var serverurl = "/cms/index/topic2Top/"+$scope.viewData.selectedChannelId+"/"+topic.id;
        $http.post(serverurl,{}).success(function(data){
            $scope.refreshIndexPage();
        }).error(function(error){});
    };

    //主题去除置顶
    $scope.cancelTopic2Top  = function(topic){
        var serverurl = "/cms/index/cancelTopic2Top/"+$scope.viewData.selectedChannelId+"/"+topic.topicId;
        $http.post(serverurl,{})
            .success(function(data){
                $scope.refreshIndexPage()

            })
            .error(function(error){

            });
    };

    $scope.getIndexData(function(){});

    //初始化海报图片上传组件
    initPoster_pic_fileupload();

    //初始化编辑海报图片上传组件
    editPoster_pic_fileupload();

    //初始化系列封面图片上传组件
    initCate_pic_fileupload("addCate");
    //初始化系列新图标上传组件
    initCate_newpic_fileupload("addCate");


    //重新刷新首页，让数据载入
    $scope.refreshIndexPage = function(){
        //载入页面数据
        $scope.getIndexData(function(error){
            
            //刷新被选中的系列的数据
            $scope.selectIndexChannel($scope.viewData.selectedChannelId,function(error){
                if(error){
                    //错误处理
                    alert(error);
                    return;
                }
            });
        });
    };
    //图片上传
    function initPoster_pic_fileupload(){
        $('#poster_pic_fileupload').fileupload({
            url: '/cms/picture/upload',
            prependFiles:"html",
            getFilesFromResponse:getFilesFromResponse,
            acceptFileTypes: /(\.|\/)(jpg|png)$/i,
            formData:function () {
                return [];
            }
        })
            .bind('fileuploadfinished',function(e,data){
                $(".uplpadimg").hide();
            })
            .addClass('fileupload-processing');
            
        function getFilesFromResponse(data){
            $scope.viewData.posterFormData.pic_url = data.result.result.files[0].url;
            $scope.digestScope();
            return data.result.result.files;
        }   
    }

    //编辑海报图片上传
    function editPoster_pic_fileupload(){
        $('#poster_edit_fileupload').fileupload({
            url: '/cms/picture/upload',
            prependFiles:"html",
            getFilesFromResponse:getFilesFromResponse,
            acceptFileTypes: /(\.|\/)(jpg|png)$/i,
            formData:function () {
                return [];
            }
        })
            .bind('fileuploadfinished',function(e,data){
                $(".uplpadimg").hide();
            })
            .addClass('fileupload-processing');
            
        function getFilesFromResponse(data){
            $scope.viewData.editPoster.pic_url = data.result.result.files[0].url;
            $scope.digestScope();
            return data.result.result.files;
        }   
    }

    function initCate_pic_fileupload(optionType){

        $('.cate_pic_fileupload').fileupload({
            url: '/cms/picture/upload',
            prependFiles:"html",
            getFilesFromResponse:getFilesFromResponse,
            acceptFileTypes: /(\.|\/)(jpg|png)$/i,
            formData:function () {
                return [];
            }
        })
            .bind('fileuploadfinished',function(e,data){
                $(".uplpadimg").hide();
            })
            .addClass('fileupload-processing');

        function getFilesFromResponse(data){
            if(optionType === "addCate"){
                $scope.viewData.channelFormData.pic_url = data.result.result.files[0].url;
            }else{
                $scope.edittingChannel.pic_url = data.result.result.files[0].url;
            }
            $scope.digestScope();
            return data.result.result.files;
        }
    }

    function initCate_newpic_fileupload(optionType){

        $('.cate_newpic_fileupload').fileupload({
            url: '/cms/picture/upload',
            prependFiles:"html",
            getFilesFromResponse:getFilesFromResponse,
            acceptFileTypes: /(\.|\/)(jpg|png)$/i,
            formData:function () {
                return [];
            }
        })
            .bind('fileuploadfinished',function(e,data){
                $(".uplpadimg").hide();
            })
            .addClass('fileupload-processing');

        function getFilesFromResponse(data){
            if(optionType === "addCate"){
                $scope.viewData.channelFormData.home_menu_icon = data.result.result.files[0].url;
            }else{
                $scope.edittingChannel.home_menu_icon = data.result.result.files[0].url;
            }
            $scope.digestScope();
            return data.result.result.files;
        }
    }

    $scope.digestScope = function () {
        setTimeout(function() {
            try {
                $scope.$digest();
            }
            catch(e) {
                console.log(e);
            }
        });
    };

    $scope.addPosterBtn = function () {
        $scope.viewData.posterFormData = {
            serial_number:"",
            pic_url:"",
            topic_id:""
        };
    }

    //添加轮播主题
    $scope.addPoster = function () {
        var serverurl = "/cms/index/addPoster/";
        if($scope.viewData.posterFormData.pic_url && $scope.viewData.posterFormData.topic_id){
            $http.post(serverurl,$scope.viewData.posterFormData)
                .success(function(data) {
                    alert("新增成功");
                    $("#posterModal").modal('hide');
                    //载入页面数据
                    $scope.getIndexData(function(error){
                        if(error){
                            //错误处理
                            alert(error);
                            return;
                        }
                        //刷新被选中的系列的数据
                        $scope.selectIndexChannel($scope.viewData.selectedChannelId,function(error){
                            if(error){
                                //错误处理
                                alert(error);
                                return;
                            }
                        });
                    });
                }).error(function(error) {
                    alert("error");
                });
        }else if(!$scope.viewData.posterFormData.pic_url){
            alert("请上传一张图片用作海报");
        }else{
            alert("请选择一个主题");
        }
    };

    
    //点击删除海报按钮
    $scope.deletePosterData = function (poster) {
       $("#deletePoster").text("确定删除"+poster.title+"吗？");
       $("#deletePoster").attr('data-posterId', poster.id);
    };
    //点击模态框确定按钮去除海报
    $scope.deletePoster = function(){
        var id = $("#deletePoster").attr("data-posterId");
        var serverurl = "/cms/index/deletePoster/"+id;
        $http.delete(serverurl)
            .success(function(data){
                _.each($scope.viewData.posters,function(one,index){
                    if(id === one.id){
                        $scope.viewData.posters.splice(index,1);
                    }
                })
            })
            .error(function(error){
                //提示数据初始化错误
            });
    };

    $scope.editPosterData = function (poster) {
        $scope.viewData.editPoster.id = poster.id;
        $scope.viewData.editPoster.serial_number = poster.serial_number;
        $scope.viewData.editPoster.pic_url = poster.pic_url;
    }

    $scope.editPoster = function () {
        var serverurl = "/cms/index/editPoster";

            $http.post(serverurl,$scope.viewData.editPoster)
                .success(function(data) {
                    $(".edit-poster").modal('hide');
                    //载入页面数据
                    $scope.getIndexData();
                });
            
    }

    //生成首页
    $scope.createIndex = function(){

        //首页数据静态化
        //redis数据静态化
        var redis_url = "/cms/index/updateRedis";
        $http.get(redis_url).success(function(data){
            if(data.code != 0){
                alert("生成首页失败，请联系系统管理员");
            }else{
                alert("生成首页成功");
            }
        });
    };
    //创建海报获取主题列表



    $scope.selectPosterChannel = function(channel){
        angular.forEach($scope.viewData.channels, function(channel) {
            channel.selected = false;
        });        
        $scope.viewData.selectedChannelTopTopics = channel.topTopics;                
        $scope.channel = channel;
        $scope.channel.selected = true;
        $scope.selectedChannel = channel;
        $scope.viewData.selectedChannelId = channel.categoryId;

        $scope.posterpagingOptions.pageSize = 10;
        $scope.posterpagingOptions.currentPage = 1;
        $scope.postergetPagedDataAsync($scope.posterpagingOptions.pageSize, $scope.posterpagingOptions.currentPage);
    };

    $scope.posterposterfilterOptions = {
        filterText: "",
        useExternalFilter: true
    };
    $scope.postertotalServerItems = 0;
    $scope.posterpagingOptions = {
        pageSizes: [10, 20, 30],
        pageSize: 10,
        currentPage: 1
    };

    $scope.postersetPagingData = function(data, page, pageSize){
        $scope.myData = data.pageData;
        $scope.postertotalServerItems = data.total;

        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.postergetPagedDataAsync = function (pageSize, page, searchText) {
        setTimeout(function () {
            var data;
            if (searchText) {
                var ft = searchText.toLowerCase();
                $http.get('/cms/topic/listTopic?s=' + pageSize +'&p='+ page + '&_search_category_id=' + $scope.viewData.selectedChannelId).success(function (data) {
                    $scope.postersetPagingData(data.result,page,pageSize);
                });
            } else {

                var url = '/cms/topic/listTopic?s=' + pageSize +'&p='+ page + '&_search_category_id=' + $scope.viewData.selectedChannelId;
                $http.get(url).success(function (data) {
                    $scope.postersetPagingData(data.result,page,pageSize);
                });
            }
        }, 100);
    };


    $scope.$watch('posterpagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && (newVal.currentPage !== oldVal.currentPage || newVal.pageSize !== oldVal.pageSize)) {
            $scope.postergetPagedDataAsync($scope.posterpagingOptions.pageSize, $scope.posterpagingOptions.currentPage);
        }
    }, true);
    $scope.$watch('posterfilterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.postergetPagedDataAsync($scope.posterpagingOptions.pageSize, $scope.posterpagingOptions.currentPage, $scope.posterfilterOptions.filterText);
        }
    }, true);

    var posteroperation = '<label class="i-checks m-b-none"><input type="radio"  name="post2Topic" ng-click="selectAddPoster(row.entity.id)"><i></i></label>';
    $scope.postercolumnDefs = [
        {field: 'title', displayName: '主题名称'},
        {field: 'account.nickname', displayName: '创建时间',cellTemplate: '<span>{{row.entity.create_date |date:"yyyy-MM-dd hh:mm"}}</span>'},
        {field: '', displayName: '操作', cellTemplate: posteroperation}
    ];
    $scope.postergridOptions = {
        data: 'myData',
        columnDefs: $scope.postercolumnDefs,
        enablePaging: true,
        showFooter: true,
        totalServerItems: 'postertotalServerItems',
        pagingOptions: $scope.posterpagingOptions,
        filterOptions: $scope.posterfilterOptions,
        rowHeight:50,
        i18n: 'zh_cn',
        multiSelect: false
    };

    window.ngGrid.i18n['zh_cn'] = yilos_i18n.resource;
    //加海报中勾选主题
    $scope.selectAddPoster = function (topicId) {
        $scope.viewData.posterFormData.topic_id = topicId;
    }
    
    $scope.digestScope = function () {
        setTimeout(function() {
            try {
                $scope.$digest();
            }
            catch(e) {
                console.log(e);
            }
        });
    };

}]);