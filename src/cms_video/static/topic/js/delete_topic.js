app.controller('deleteTopicData', ['$rootScope', '$scope', '$http', '$location', '$stateParams', '$state', function ($rootScope, $scope, $http,$location, $stateParams, $state){
	$scope.topicData = {
		key:'',
		type:0,		//0 查看所有主题，1查看搜索主题 ,2查看所有被下架的主题
		delete:'',
        publish:{
            tips:'',
            is_publish:'',
            topic_id:''
        }
	};

	$scope.filterOptions = {
        filterText: "",
        useExternalFilter: true  
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [20, 50, 100],
        pageSize: 20,
        currentPage: 1
    };

    //创建表单
    $scope.setPagingData = function(data){
        $scope.myData = data;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };

    //获取主题列表数据
    $scope.getTopicList = function (page, prePage) {
        setTimeout(function () {
            var url = '/cms/index/getAllTopic?perPage='+ prePage + '&page=' + page;
            $http.get(url).success(function (data) {
                $scope.setPagingData(data.result.pageData);
                $scope.totalServerItems = data.result.count;
            });
        }, 100);
    };

    //获取搜索主题数据
    $scope.getSearchList = function (page, prePage) {
        setTimeout(function () {
            var url = '/cms/index/getSearchTopic?key='+$scope.topicData.key+'&perPage='+ prePage + '&page=' + page;
            $http.get(url).success(function (data) {
                $scope.setPagingData(data.result.pageData);
                $scope.totalServerItems = data.result.count;
            });
        }, 100);
    };

    //获取被举报的主题数据
    $scope.getnopublishList = function (page, prePage) {
        setTimeout(function () {
            var url = '/cms/index/getnopublish?perPage='+ prePage + '&page=' + page;
            $http.get(url).success(function (data) {
                $scope.setPagingData(data.result.pageData);
                $scope.totalServerItems = data.result.count;
            });
        }, 100);
    }

    //监听表单
    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && (newVal.currentPage !== oldVal.currentPage || newVal.pageSize !== oldVal.pageSize)) {

        	if($scope.topicData.type == 0){
        		$scope.getTopicList($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize);
        		return;
        	}else if($scope.topicData.type == 1){
                $scope.getSearchList($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize);
            }else{
                $scope.getnopublishList($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize);
            }
            
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            if($scope.topicData.type == 0){
        		$scope.getTopicList($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize);
        		return;
        	}else if($scope.topicData.type == 1){
                $scope.getSearchList($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize);
            }else{
                $scope.getnopublishList($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize);
            }
        }
    }, true);

    var operation = '<span class="label bg-danger ng-scope" data-toggle="modal" data-target=".deletData" ng-click="deleteTopic(row.entity)">删除</span>' +
        ' <span class="label bg-info" data-toggle="modal" data-target=".publishData" ng-click="togglepublish(row.entity)" >{{row.entity.is_publish != 0? "下架": "已下架"}}</span>';


    $scope.columnDefs = [
    	{field: 'name', displayName: '系列', cellTemplate: '<span>{{row.entity.name?row.entity.name:"不存在系列中"}}</span>'},
        {field: 'title', displayName: '标题', cellTemplate: '<span style="margin-left:10px">{{row.entity.title}}</span>'},
        {field: 'serial_number', displayName: '序号',cellTemplate: '<div>{{row.entity.serial_number}}</div>'},
        {field: 'create_date', displayName: '日期',cellTemplate: '<span>{{row.entity.create_date |date:"yyyy-MM-dd hh:mm"}}</span>'},
        {field: '', displayName: '操作', cellTemplate: operation}
    ];
    $scope.gridOptions = {
        data: 'myData',
        columnDefs: $scope.columnDefs,
        enablePaging: true,
        showFooter: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        multiSelect: false,
        rowHeight:80,
        i18n: 'zh_cn'
    };
    window.ngGrid.i18n['zh_cn'] = yilos_i18n.resource;

    $scope.getTopicList(1, 20);

    //搜索按钮
    $scope.searchBtn = function () {
    	$scope.pagingOptions.currentPage = 1;
    	$scope.topicData.type = 1;
    	$scope.getSearchList(1,20);
    }

    //查看所有按钮
    $scope.getAllData = function () {
    	$scope.pagingOptions.currentPage = 1;
    	$scope.topicData.type = 0;
    	$scope.getTopicList(1, 20);
    }

    //查看所有被下架的主题
    $scope.getnopushData = function () {
        $scope.pagingOptions.currentPage = 1;
        $scope.topicData.type = 2;
        $scope.getnopublishList(1, 20);
        
    }
    //删除按钮
    $scope.deleteTopic = function (data) {
    	$scope.topicData.delete = data;
    }

    //删除确认按钮
    $scope.deleteOkBtn = function () {
    	var url = '/cms/index/deleteTopicTrue';
    	$http.post(url,{topic_id: $scope.topicData.delete.topic_id}).success(function (data) {
    		$(".deletData").modal('hide');
    		$scope.pagingOptions.currentPage = 1;
    		$scope.topicData.type = true;
    		$scope.getTopicList(1, 20);
    	});
    }

    //上下架按钮
    $scope.togglepublish = function (data) {
        $scope.topicData.publish.topic_id = data.topic_id;
        
        if (data.is_publish == 1) {
            $scope.topicData.publish.is_publish = 0;
            $scope.topicData.publish.tips = "确定下架————"+ data.title;
        }else{
            $scope.topicData.publish.is_publish = 1;
            $scope.topicData.publish.tips = "取消下架————"+ data.title;
        }
    }

    //上下架确定按钮
    $scope.publishOkBtn = function () {
        var url = '/cms/index/togglepublish';
        $http.post(url,{topic_id: $scope.topicData.publish.topic_id, is_publish: $scope.topicData.publish.is_publish}).success(function (data) {
            $(".publishData").modal('hide');
            $scope.pagingOptions.currentPage = 1;
            $scope.topicData.type = true;
            $scope.getTopicList(1, 20);
        });
    }

}]);