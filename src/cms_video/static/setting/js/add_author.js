app.controller('addAuthorCtrl', ['$rootScope', '$scope', '$http','$location','$state','$stateParams', function ($rootScope, $scope, $http,$location,$state,$stateParams) {


    $scope.searchData = {
        key: '',
        type:false          //true 代表搜索用户，false所有用户  
    }

    $scope.filterOptions = {
        filterText: "",
        useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [20, 50, 100],
        pageSize: 20,
        currentPage: 1
    };

    $scope.setPagingData = function(data){
        $scope.myData = data;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    //获取所有用户信息
    $scope.getPagedDataAsync = function (pageSize, page) {
        setTimeout(function () {
            var url = '/cms/user/getUserInfo?perPage=' + pageSize + "&page=" + page;
            $http.get(url).success(function (data) {
                $scope.setPagingData(data.result.result,page,pageSize);
                $scope.totalServerItems = data.result.count[0].count;
            });
        }, 100);
    };

    //获取搜索到的用户信息
    $scope.getsearchData = function (key, pageSize, page) {
        setTimeout(function () {
            var url = '/cms/user/searchUser?key='+key+'&perPage=' + pageSize + '&page=' + page;
            $http.get(url).success(function (data) {
                $scope.setPagingData(data.result.pageData,page,pageSize);
                $scope.totalServerItems = data.result.count;
            });
        }, 100);
    }

    //搜索按钮
    $scope.searchBtn = function () {
        $scope.pagingOptions.currentPage = 1;
        $scope.searchData.type           = true;

        $scope.getsearchData($scope.searchData.key, 20, 1);
    }

    //查看全部按钮
    $scope.getAllUser = function () {
        $scope.pagingOptions.currentPage = 1;
        $scope.searchData.type           = false;

        $scope.getPagedDataAsync(20, 1);
    }

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && (newVal.currentPage !== oldVal.currentPage || newVal.pageSize !== oldVal.pageSize)) {
            if($scope.searchData.type){
    
                $scope.getsearchData($scope.searchData.key, $scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
                return;
            }

            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            if($scope.searchData.type){
    
                $scope.getsearchData($scope.searchData.key, $scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
                return;
            }

            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        }
    }, true);

    var operation = '<div >&nbsp;&nbsp;&nbsp;<span class="label bg-success" data-toggle="modal" data-target=".editUserInfo" ng-click="openEditUserInfo(row.entity)">编辑</span>';
    operation += ' <span class="label bg-info" data-toggle="modal" data-target=".reset-password" ng-click="openResetPassword(row.entity)">重置密码</span>';
    operation += ' <span class="label bg-danger" data-toggle="modal" data-target=".delete-user" ng-click="deleteUser(row.entity)">{{row.entity.black_id ? "已拉黑": "拉黑"}}</span>';
    operation += ' <span class="label bg-info" data-toggle="modal" ng-click="openUserInfor(row.entity)">查看</span></div>';

    $scope.columnDefs = [
        {field: 'username', displayName: '用户名', cellTemplate: '<span style="margin-left:10px">{{row.entity.username}}</span>'},
        {field: 'nickname', displayName: '昵称', cellTemplate: '<span>{{row.entity.nickname?row.entity.nickname:"未填写"}}</span>'},
        {field: 'gender', displayName: '性别',cellTemplate: '<div>{{row.entity.gender>=1?(row.entity.gender==1?"女":"男"):"未填写"}}</div>'},
        {field: 'type', displayName: '身份',cellTemplate: '<div>{{row.entity.type}}</div>'},
        {field: 'coin', displayName: '咖币',cellTemplate: '<span>{{row.entity.coin}}</span>'},
        {field: 'exp', displayName: '经验值',cellTemplate: '<span>{{row.entity.exp}}</span>'},
        {field: '', displayName: '操作', cellTemplate: operation}
    ];
    $scope.gridOptions = {
        data: 'myData',
        columnDefs: $scope.columnDefs,
        enablePaging: true,
        showFooter: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        multiSelect: false,
        i18n: 'zh_cn'
    };
    window.ngGrid.i18n['zh_cn'] = yilos_i18n.resource;

    $scope.getPagedDataAsync(20, 1);

    //$scope.delUser = function(user){
    //    $scope.user = user;
    //};
    //$scope.commitdelUser = function(){
    //   $http.post()
    //};
    $scope.commitAddUser = function(){
        if($scope.mobilePhone && $scope.mobilePhone!="" && $scope.selectedJob && $scope.selectedJob!=""){
            var username = $scope.mobilePhone;
            var nickname = $scope.newNickname || ("手机用户"+$scope.mobilePhone.substr(-4));
            var password = $scope.password || "000000";
            var user_type = $scope.selectedJob;
            $http.post("/cms/user/addUser", {username: username,nickname: nickname,password: password,type: user_type}).success(function(data){
                if(data.result == "该手机号已存在"){
                    alert("该手机号已存在");
                }else{
                    $scope.getPagedDataAsync(20, 1);
                    $(".add-user").modal('hide');
                }
            });
        }else{
            alert("手机号和身份都不能为空");
        }
    }

    $scope.openEditUserInfo = function(userInfo){
        $scope.editUser    = userInfo;
        $scope.oldNickname = userInfo.nickname;
        $scope.oldexp      = userInfo.exp;
        $scope.oldcoin     = userInfo.coin;
        switch(userInfo.type){
            case "美甲店主":
                $scope.editUser.typeNum = 1;
                break;
            case "美甲师":
                $scope.editUser.typeNum = 2;
                break;
            case "美甲从业者":
                $scope.editUser.typeNum = 3;
                break;
            case "美甲消费者":
                $scope.editUser.typeNum = 4;
                break;
            case "美甲老师":
                $scope.editUser.typeNum = 5;
                break;
            case "其他":
                $scope.editUser.typeNum = 6;
                break;
            default :
                $scope.editUser.typeNum = "";
        }
        $scope.oldTypeNum = $scope.editUser.typeNum;
    };
    $scope.commitEditUser = function(){

        if(($scope.oldTypeNum != $scope.editUser.typeNum) || ($scope.oldNickname != $scope.editUser.nickname) || ($scope.oldexp != $scope.editUser.exp) || ($scope.oldcoin != $scope.editUser.coin)){
            $http.post("/cms/user/editUser", {id: $scope.editUser.id, nickname:$scope.editUser.nickname, type: $scope.editUser.typeNum, exp: $scope.editUser.exp, coin: $scope.editUser.coin}).success(function(data){
                $scope.getPagedDataAsync(20, 1);
                $(".editUserInfo").modal('hide');
                $scope.pagingOptions.currentPage = 1;
                $scope.getPagedDataAsync(20, $scope.pagingOptions.currentPage);
            });
        }else{
            $(".editUserInfo").modal('hide');
        }
    };

    $scope.openResetPassword = function(userInfo){
        $scope.resetUser = userInfo;
    };
    $scope.resetPassword = function(){
        if($scope.newPassowrd){
            $http.post("/cms/user/resetPassword", {id:$scope.resetUser.id, password:$scope.newPassowrd}).success(function(data){
                alert("密码重置成功！");
                $(".reset-password").modal('hide');
            });
        }else{
            $(".reset-password").modal('hide');
        }
    };

    $scope.deleteUser = function(data){
        $scope.deleteUserData = data;
        if($scope.deleteUserData.black_id){
            $scope.deleteTips = '取消拉黑用户'
            return;
        }
        $scope.deleteTips = '拉黑用户'
    }

    $scope.deleteUserOk = function(){
        if($scope.deleteUserData){
            if($scope.deleteUserData.black_id){
                $http.post("/cms/user/cancelUserBlack", {black_id:$scope.deleteUserData.black_id}).success(function(data){
                    $(".delete-user").modal('hide');
                    $scope.pagingOptions.currentPage = 1;
                    $scope.getPagedDataAsync(20, $scope.pagingOptions.currentPage);
                });
                return;
            }
            $http.post("/cms/user/deleteUser", {id:$scope.deleteUserData.id}).success(function(data){
                $(".delete-user").modal('hide');
                $scope.pagingOptions.currentPage = 1;
                $scope.getPagedDataAsync(20, $scope.pagingOptions.currentPage);
            });
        }else{
            $(".delete-user").modal('hide');
        }
    }

    $scope.openUserInfor = function (data) {
        $state.go('app.user_infor', {
            account_id: data.id,
            nickname: data.nickname,
            type: data.type
        });
    }

}]);
