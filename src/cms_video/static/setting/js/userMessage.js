app.controller('userMessage', ['$rootScope', '$scope', '$http','$location', function ($rootScope, $scope, $http,$location) {

    $scope.filterOptions = {
        filterText: "",
        useExternalFilter: true
    };
    $scope.topics = [];
    $scope.pushuser = {
        content:'',
        targets:''
    };

    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [10, 20, 50],
        pageSize: 10,
        currentPage: 1
    };
    $scope.setPagingData = function(data){
        $scope.myData = data;
    };
    $scope.getPagedDataAsync = function (perPage, page) {
        var url = "/cms/msg/getMessage?page=" + page + "&perPage=" + perPage;
        $http.get(url).success(function (data) {
            $scope.setPagingData(data.result);
        });
    };
    $scope.getPageDataCountAsync = function () {
        var url = "/cms/msg/getMessgeCount";
        $http.get(url).success(function(data){
            if(data.code == 0){
                $scope.totalServerItems = data.result.count;
            }
        });
    };
    $scope.deleteMessage = function(){
        var url = "/cms/msg/deleteMessage/" + $scope.message.id;
        $http.post(url).success(function(data){
            $(".delete-message").modal('hide');
            if(data.code != 0){
                alert("删除失败，请联系管理员");
                return;
            }
            $scope.myData = _.filter($scope.myData, function(item){
                if(item.id == $scope.message.id){
                    return false;
                }
                return true;
            });
            $scope.totalServerItems--;
        });
    };
    $scope.addMessage = function(){
        var url = "/cms/msg/addMessage";
        if($scope.formData){
            $scope.formData.topicId = $scope.formData.topicId || null;
            if(!$scope.formData.publish_date_format){
                alert("请选择发布时间。");
                return;
            }
            $scope.formData.publish_date = transdateDate($scope.formData.publish_date_format);
            delete $scope.formData.publish_date_format;
            $http.post(url, $scope.formData).success(function(data){
                $(".add-message").modal('hide');
                if(data.code != 0){
                    alert("增加失败，请联系管理员");
                    return;
                }
                if($scope.formData.topicId && $scope.formData.topicId != ""){
                    var ids = _.pluck($scope.topics, 'id');
                    var titles = _.pluck($scope.topics, 'title');
                    $scope.formData.topic_name = titles[_.indexOf(ids, $scope.formData.topicId)];
                }
                $scope.myData.push($scope.formData);
                delete $scope.formData;
                $scope.totalServerItems++;
            });
        }else{
            alert("请标题和内容。");
            return;
        }
    };

    $scope.openCheckMessage = function(data){
        $scope.message = data;
        $scope.message.publish_date_format = getLocalTime(data.publish_date);
    };

    $scope.openDeleteMessage = function(data){
        $scope.message = data;
        $scope.message.publish_date_format = getLocalTime(data.publish_date);
    };

    $scope.openAddMessage = function(){
        var url = '/cms/msg/getLatestTopic';
        $http.get(url).success(function(data){
            $scope.topics = data.result.topics;
        });
    };

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage || newVal.pageSize !== oldVal.pageSize) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        }
    }, true);

    var operation = '<div><span class="label bg-success" data-toggle="modal" data-target=".check-message" ng-click="openCheckMessage(row.entity)">查看详细</span>';
    operation += ' <span class="label bg-danger" data-toggle="modal" data-target=".delete-message" ng-click="openDeleteMessage(row.entity)">删除</span></div>';

    $scope.columnDefs = [
        {field: 'title', displayName: '标题', width: '200px'},
        {field: 'content', displayName: '内容', width: '500px'},
        {field: 'topic_name', displayName: '主题'},
        {field: 'post_name', displayName: '帖子'},
        {field: 'publish_date', displayName: '发布时间',cellTemplate:"<div>{{row.entity.publish_date | date:'yyyy-MM-dd HH:mm:ss'}}</div>"},
        {field: '', displayName: '操作', cellTemplate: operation}
    ];

    $scope.gridOptions = {
        data: 'myData',
        columnDefs: $scope.columnDefs,
        enablePaging: true,
        showFooter: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        multiSelect: false,
        i18n: 'zh_cn'
    };

    window.ngGrid.i18n['zh_cn'] = yilos_i18n.resource;

    //初始操作
    function init(){
        //获取数据总数
        $scope.getPageDataCountAsync();
        //获取当页数据
        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    }

    init();

    //添加点对点消息
    $scope.addUserMessage = function () {
        $scope.pushuser = {
            content:'',
            targets:''
        }
    }

    $scope.addUserMessageOk = function () {
        var url = "/cms/message/single";

        if(!$scope.pushuser.content){
            alert("请将信息填写完整");
            return;
        }

        $http.post(url, $scope.pushuser).success(function(data){
            $(".add-user").modal('hide');
        });   
    }
    
}]);

function getLocalTime(timeStr) {
    var d = new Date(timeStr);

    return d.getFullYear()+"年"+((d.getMonth()+1)<10?"0"+(d.getMonth()+1):(d.getMonth()+1))+"月"+(d.getDate()<10?"0"+d.getDate():d.getDate())+
        "日 "+(d.getHours()<10?"0"+d.getHours():d.getHours())+":"+(d.getMinutes()<10?"0"+d.getMinutes():d.getMinutes())+":"+(d.getSeconds()<10?"0"+d.getSeconds():d.getSeconds());
}

function transdateDate(time){
    var date = new Date();
    date.setFullYear(time.substring(0,4));
    date.setMonth(time.substring(5,7)-1);
    date.setDate(time.substring(8,10));
    date.setHours(time.substring(12,14));
    date.setMinutes(time.substring(15,17));
    date.setSeconds(time.substring(18,20));
    return Date.parse(date);
}