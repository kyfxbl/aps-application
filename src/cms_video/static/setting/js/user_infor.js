app.controller('user_infor', ['$rootScope', '$scope', '$http','$location','$stateParams', '$state', function ($rootScope, $scope, $http,$location,$stateParams,$state) {

	$scope.userData = {
		show:{
			type:$stateParams.type,							//标示作者身份
			tab:1,								//1、2、3控制显示tab页
			nickname: $stateParams.nickname,
			show:false,							//是否显示作品tab标签	
			pic_url:'',
            postimgs:''			
		}
	};

	//判断身份是否是“美甲老师”,并处理身份
	if($scope.userData.show.type == '美甲老师' || $scope.userData.show.type == 5){
		$scope.userData.show.type = '美甲老师';
		$scope.userData.show.show = true;
	}else{
		$scope.userData.show.type == '普通用户';
	}

	//返回用户列表
	$scope.backUserList = function () {
		$state.go('app.add_author', {

        });
	}

	//tap页切换
	$scope.selectTab = function (tab) {
		$scope.userData.show.tab = tab;
		if(tab == 1){
			$scope.getUserPost(1,20);
		}else if(tab == 2){
			$scope.getUserHomeWork(1,20);
		}else{
			$scope.getUserTopic(1,20);
		}
	}

	//表格控件初始化数据
	$scope.filterOptions = {
        filterText: "",
        useExternalFilter: true  
    };
    //表格控件初始化数据
    $scope.totalServerItems = 0;
    //表格控件初始化数据
    $scope.pagingOptions = {
        pageSizes: [20, 50, 100],
        pageSize: 20,
        currentPage: 1
    };

    //创建表单
    $scope.setPagingData = function(data,column){
		$scope.myData     = data;
		$scope.columnDefs = column;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };

    //监听表单
    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && (newVal.currentPage !== oldVal.currentPage || newVal.pageSize !== oldVal.pageSize)) {
        	if($scope.userData.show.tab == 1){
        		$scope.getUserPost($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize);
        	}else if($scope.userData.show.tab == 2){
        		$scope.getUserHomeWork($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize);
        	}else{
        		$scope.getUserTopic($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize);
        	}
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            if($scope.userData.show.tab == 1){
        		$scope.getUserPost($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize);
        	}else if($scope.userData.show.tab == 2){
        		$scope.getUserHomeWork($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize);
        	}else{
        		$scope.getUserTopic($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize);
        	}
        }
    }, true);

    //帖子表格模型
    $scope.columnPost = [
        {field: 'title', displayName: '标题', cellTemplate: '<span style="margin-left:10px">{{row.entity.title}}</span>'},
        {field: 'content', displayName: '内容', cellTemplate: '<span>{{row.entity.content?row.entity.content:"没有内容"}}</span>'},
        {field: '', displayName: '图片', cellTemplate: '<span class="label bg-info" data-toggle="modal" data-target=".showpostImg" ng-click="showpostImg(row.entity)">查看图片</span>'},
        {field: 'create_date', displayName: '日期',cellTemplate: '<span>{{row.entity.create_date |date:"yyyy-MM-dd hh:mm"}}</span>'},
        {field: 'community_name', displayName: '圈子',cellTemplate: '<span>{{row.entity.community_name}}</span>'}
    ];

    //交作业表格模型
    $scope.columnHomeWork = [
        {field: 'topic_title', displayName: '主题', cellTemplate: '<span style="margin-left:10px">{{row.entity.topic_title}}</span>'},
        {field: 'content', displayName: '内容', cellTemplate: '<span>{{row.entity.content?row.entity.content:"没有内容"}}</span>'},
        {field: '', displayName: '图片',cellTemplate: '<span class="label bg-info" data-toggle="modal" data-target=".showworkImg" ng-click="showworkImg(row.entity.content_pic)">查看图片</span>'},
        {field: 'score', displayName: '分数',cellTemplate: '<div>{{row.entity.score}}</div>'},
        {field: 'create_date', displayName: '日期',cellTemplate: '<span>{{row.entity.create_date |date:"yyyy-MM-dd hh:mm"}}</span>'}
    ];

    //老师作品表格模型
    $scope.columnTopic = [
        {field: 'topic_title', displayName: '主题', cellTemplate: '<span style="margin-left:10px">{{row.entity.topic_title}}</span>'},
        {field: 'pic_url', displayName: '海报', cellTemplate: '<img style="height:80px;" src="{{row.entity.pic_url}}" alt="">'},
        {field: 'create_date', displayName: '日期',cellTemplate: '<span>{{row.entity.create_date |date:"yyyy-MM-dd hh:mm"}}</span>'}
    ];

    $scope.gridOptions = {
        data: 'myData',
        columnDefs: 'columnDefs',
        enablePaging: true,
        showFooter: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        multiSelect: false,
        rowHeight:80,
        i18n: 'zh_cn'
    };

    window.ngGrid.i18n['zh_cn'] = yilos_i18n.resource;

    //获取用户的帖子信息
    $scope.getUserPost = function (page, prePage) {    	
    	setTimeout(function () {
            var url = '/cms/user/getPosts?account_id='+$stateParams.account_id +'&perPage='+ prePage + '&page=' + page;
            $http.get(url).success(function (data) {
                $scope.setPagingData(data.result.pageData, $scope.columnPost);
                $scope.totalServerItems = data.result.count;
                
            });
        }, 100);
    }
    $scope.getUserPost(1,20);

    //获取帖子中的图片
    $scope.getPostImg = function (post_id, callback) {
        var url = '/cms/community_post/postImg?post_id='+post_id;
        $http.get(url).success(function (data) { 
            callback(data);
        });
    }

    //帖子点击查看图片按钮
    $scope.showpostImg = function (data) {
        $scope.getPostImg(data.post_id, function (data) {
            $scope.userData.show.postimgs = data.result;
        });
    }

    //获取用户交作业的信息
    $scope.getUserHomeWork = function (page, prePage) {
    	setTimeout(function () {
            var url = '/cms/user/getHomeWork?account_id='+$stateParams.account_id +'&perPage='+ prePage + '&page=' + page;
            $http.get(url).success(function (data) {
                $scope.setPagingData(data.result.pageData, $scope.columnHomeWork);
                $scope.totalServerItems = data.result.count;
                 
            });
        }, 100);
    }

    //查看交作业图片
    $scope.showworkImg = function (pic_url) {
        $scope.userData.show.pic_url = pic_url;
    }

    //获取用户作品的信息
    $scope.getUserTopic = function (page, prePage) {
    	setTimeout(function () {
            var url = '/cms/user/getTeacherData?account_id='+$stateParams.account_id +'&perPage='+ prePage + '&page=' + page;
            $http.get(url).success(function (data) {
                $scope.setPagingData(data.result.pageData, $scope.columnTopic);
                $scope.totalServerItems = data.result.count;
                 
            });
        }, 100);
    }

    //帖子跳转到编辑页面
    $scope.postShow = function (post_id) {
    	// body...
    }
}]);