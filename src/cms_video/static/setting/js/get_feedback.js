app.controller('feedBackCtrl', ['$rootScope', '$scope', '$http','$location','$stateParams', function ($rootScope, $scope, $http,$location,$stateParams) {

    $scope.delFeedBack = function(id){
        $http.get('/cms/feedback/delFeedBackMsg?id=' + id).success(function(data){
            if(data.code == 0){
                $scope.myData = _.filter($scope.myData, function(item){
                    return item.id != id;
                });
                $scope.totalServerItems--;
            }else{
                alert("删除失败，请联系管理员");
            }
        });
    };

    $scope.filterOptions = {
        filterText: "",
        useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [10, 20, 50],
        pageSize: 20,
        currentPage: 1
    };

    $scope.setPagingData = function(data){
        $scope.myData = data;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.getPagedDataAsync = function (pageSize, page) {
        setTimeout(function () {
            var url = '/cms/feedback/getFeedBackMsg?perPage=' + pageSize + "&page=" + page;
            $http.get(url).success(function (data) {
                $scope.setPagingData(data.result.result,page,pageSize);
                $scope.totalServerItems = data.result.count[0].count;
            });
        }, 100);
    };


    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && (newVal.currentPage !== oldVal.currentPage || newVal.pageSize !== oldVal.pageSize)) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    var operation = '<div class="label bg-danger" style="width:50px;margin-left: 20px" ng-click="delFeedBack(row.entity.id)" >删除</div>';

    $scope.columnDefs = [
        {field: 'username', displayName: '用户名', cellTemplate: '<span style="margin-left:10px">{{row.entity.username?row.entity.username:"游客"}}</span>'},
        {field: 'nickname', displayName: '昵称', cellTemplate: '<span>{{row.entity.nickname?row.entity.nickname:"游客"}}</span>'},
        {field: 'content', displayName: '反馈内容',cellTemplate: '<div>{{row.entity.content}}</div>'},
        {field: 'create_date', displayName: '反馈时间',cellTemplate: '<span>{{row.entity.create_date |date:"yyyy-MM-dd hh:mm"}}</span>'},
        {field: '', displayName:'操作', cellTemplate:operation}
    ];
    $scope.gridOptions = {
        data: 'myData',
        columnDefs: $scope.columnDefs,
        enablePaging: true,
        showFooter: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        rowHeight:100,
        multiSelect: false,
        i18n: 'zh_cn'
    };
    window.ngGrid.i18n['zh_cn'] = yilos_i18n.resource;

    $scope.getPagedDataAsync(15, 1);

}]);
