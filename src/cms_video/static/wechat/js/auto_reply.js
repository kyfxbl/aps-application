app.controller('autoReplyCtrl', ['$rootScope', '$scope', '$http','$location','$stateParams', function ($rootScope, $scope, $http,$location,$stateParams) {


    $scope.filterOptions = {
        filterText: "",
        useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [20, 50, 100],
        pageSize: 20,
        currentPage: 1
    };

    $scope.setPagingData = function(data){
        $scope.myData = data;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.getPagedDataAsync = function (pageSize, page) {
        setTimeout(function () {
            var url = '/cms/weixinAutoReply/queryAllAutoReply?perPage=' + pageSize + "&page=" + page;
            $http.get(url).success(function (data) {
                $scope.setPagingData(data.result.datas,page,pageSize);
                $scope.totalServerItems = data.result.count;
            });
        }, 100);
    };


    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && (newVal.currentPage !== oldVal.currentPage || newVal.pageSize !== oldVal.pageSize)) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    var operation = '<div >&nbsp;&nbsp;&nbsp;<span class="label bg-success" data-toggle="modal" data-target=".edit-reply" ng-click="openEditReply(row.entity)">编辑</span>';
    operation += ' <span class="label bg-danger" data-toggle="modal" data-target=".delete-reply" ng-click="openDeleteReply(row.entity)">删除</span></div>';

    $scope.columnDefs = [
        {field: 'keyword', displayName: '关键词', cellTemplate: '<span style="margin-left:10px">{{row.entity.keyword}}</span>'},
        {field: 'content', displayName: '内容', cellTemplate: '<span>{{row.entity.content}}</span>'},
        {field: 'content_url', displayName: '链接',cellTemplate: '<div style="white-space:nowrap;">{{row.entity.content_url}}</div>'},
        {field: '', displayName: '操作', cellTemplate: operation}


    ];
    $scope.gridOptions = {
        data: 'myData',
        columnDefs: $scope.columnDefs,
        enablePaging: true,
        showFooter: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        multiSelect: false,
        i18n: 'zh_cn'
    };
    window.ngGrid.i18n['zh_cn'] = yilos_i18n.resource;

    $scope.getPagedDataAsync(20, 1);


    $scope.commitAddReply = function(){
        var keyword = $scope.keyword;
        var content = $scope.content;
        var content_url = $scope.content_url || "";
        if(keyword && keyword!="" && content && content!=""){
            $http.post("/cms/weixinAutoReply/addAutoReply", {keyword: keyword,content: content,content_url: content_url}).success(function(data){
                if(data.code == 1){
                    alert(data.message);
                }else{
                    $scope.getPagedDataAsync(20, 1);
                    $(".add-reply").modal('hide');
                }
            });
        }else{
            alert("关键词或内容不能为空");
        }
    };

    $scope.openEditReply= function(data){
        $scope.editReplyData = data;
    };
    $scope.commitEditReply = function(){
        var options = {
            id: $scope.editReplyData.id,
            keyword: $scope.editReplyData.keyword,
            content: $scope.editReplyData.content,
            content_url: $scope.editReplyData.content_url
        };
        $http.post("/cms/weixinAutoReply/updateAutoReply", options).success(function(data){
            $scope.getPagedDataAsync(20, 1);
            $(".edit-reply").modal('hide');
        });
    };



    $scope.openDeleteReply = function(data){
        $scope.deleteReplyData = data;
    };

    $scope.deleteUser = function(){
        if($scope.deleteReplyData){
            $http.post("/cms/weixinAutoReply/delAutoReply", {id: $scope.deleteReplyData.id}).success(function(data){
                $scope.getPagedDataAsync(20, 1);
                $(".delete-reply").modal('hide');
            });
        }else{
            $(".delete-reply").modal('hide');
        }
    };

}]);

